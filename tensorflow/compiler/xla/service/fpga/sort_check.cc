#include <algorithm>
#include <iostream>
#include <vector>
#include "absl/types/span.h"
int main() {
  std::vector<const int> v = {2, 3, 1, 0};
  absl::Span<int> op(v);

  std::sort(op.begin(), op.end());
  // for(auto it = op.begin(); it != op.end(); ++it)
  for (int i = 0; i < 4; i++) std::cout << op[i] << " ";

  return 0;
}
