#include <bits/stdc++.h>
#include <utility>
#include "riffa_stream.h"
namespace xla{
  namespace fpga{
struct Tile {
  int x;  // origin
  int y;
  int tx;  // size
  int ty;
};

bool compare(const std::pair<int, Tile> i, const std::pair<int, Tile> j) ;
void print(Tile t) ;

int calc_output_dim(int inp_size, int K, int S) ;

int calc_input_dim(int outp_size, int K, int S) ;

Tile calc_input_tile(Tile outTile, int Kx, int Ky, int Sx, int Sy) ;

std::vector<Tile> calc_input_tiles(std::vector<Tile> outTiles, int Kx, int Ky,
                                   int Sx, int Sy) ;

Tile calc_output_tile(Tile inTile, int Kx, int Ky, int Sx, int Sy) ;

int findRoot(int x) ;

void findTilesSS(std::vector<Tile> &outp, Tile inp, convParamsType conv_params,
                 maxpoolParamsType maxpool_params, int num_PEs) ;

void findTilesSL(std::vector<Tile> &outp, Tile inp, convParamsType conv_params,
                 maxpoolParamsType maxpool_params, int num_PEs) ;

void findTilesLS(std::vector<Tile> &outp, Tile inp, convParamsType conv_params,
                 maxpoolParamsType maxpool_params, int num_PEs) ;

void findTilesLL(std::vector<Tile> &outp, Tile inp, convParamsType conv_params,
                 maxpoolParamsType maxpool_params, int num_PEs) ;

std::vector<Tile> sortTiles(std::vector<Tile> maxpoolTiles, int Kx, int Ky,
                            int Sx, int Sy) ;

std::vector<Tile> findTiles(Tile inp, convParamsType conv_params,
                            maxpoolParamsType maxpool_params, int num_PEs) ;
}
}
