#include "executeGraphPass.h"
#include "graph_builder.h"
#define __NUM_PE_PER_CORE 16
#include "fpga_ops.h"

namespace xla {
namespace fpga {
executeGraphPass::executeGraphPass(fGraph_tf *graph_ptr) : graphVisitor(*graph_ptr) {
  LOG(INFO)<<"DEBUG";
  if (graph_ptr == nullptr) {
    LOG(INFO) << "ERROR: graph is empty";
    exit(1);
  }
  graph_tf = graph_ptr;
  LOG(INFO)<<"EXIT DEBUG";
}

void executeGraphPass::run(void *result_buffer, int result_buffer_size) {
  LOG(INFO) << "Starting Execution";
  if(result_buffer_size==0) return;
  visitPostOrder();
  LOG(INFO) << "Done Execution";
  if (graph.root_node->result == nullptr) {
    LOG(INFO) << "ERROR: no result found in root node after execution";
    exit(1);
  }
  if (result_buffer_size != graph.root_node->get_result_size_in_bytes()) {
    LOG(INFO) << "ERROR: size of output does not match. result_buffer_size = "
              << result_buffer_size << "root_node size"
              << graph.root_node->get_result_size_in_bytes();
    exit(1);
  }
  LOG(INFO) << "Copying results from root node "<<graph.root_node->getNodeType()<<" id = "<<graph.root_node->id;
  memcpy(result_buffer, graph.root_node->result,
         graph.root_node->get_result_size_in_bytes());
  LOG(INFO) << "Copied  "<<graph.root_node->get_result_size_in_bytes()<<" bytes";
}

void executeGraphPass::assignParameters(
    absl::Span<const ShapedBuffer *const> &arguments,
    std::unique_ptr<const BufferAssignment> &assignment) {
  for (int i = 0; i < assignment->Allocations().size(); ++i) {
    auto &allocation = assignment->GetAllocation(i);

    // LOG(INFO) << allocation.ToString();

    if (allocation.is_entry_computation_parameter()) {
      auto it =
          graph_tf->parameter_to_node_map.find(allocation.parameter_number());
      if (it != graph_tf->parameter_to_node_map.end()) {
        fParameter *node = dynamic_cast<fParameter *>(it->second);
        if (node == nullptr ||
            node->parameter_number != allocation.parameter_number()) {
          LOG(INFO) << "ERROR: parameter_to_node_map is wrong!";
        }
        node->result = arguments[allocation.parameter_number()]
                           ->buffer(allocation.param_shape_index())
                           .opaque();
      }
    }
    continue;
  }
}

float *executeGraphPass::padding(const float *input,
                                 const std::vector<int> &inp_dims,
                                 int padding_top, int padding_bot,
                                 int padding_left, int padding_right) {
  int x, y, flag = 0;
  std::vector<int> output_dim = inp_dims;

  output_dim[1] += (padding_top + padding_bot);
  output_dim[2] += (padding_left + padding_right);
  float *output =
      new float[output_dim[0] * output_dim[1] * output_dim[2] * output_dim[3]];

  for (int i = 0; i < output_dim[0]; i++) {
    for (int j = 0, x = 0; j < output_dim[1]; j++) {
      for (int k = 0, y = 0; k < output_dim[2]; k++) {
        for (int l = 0; l < output_dim[3]; l++) {
          if ((j < padding_top) || (j >= (output_dim[1] - padding_bot))) {
            *(output + (i * output_dim[1] * output_dim[2] * output_dim[3]) +
              (j * output_dim[2] * output_dim[3]) + (k * output_dim[3]) + l) =
                0;
          } else if ((k < padding_left) ||
                     (k >= (output_dim[2] - padding_right))) {
            *(output + (i * output_dim[1] * output_dim[2] * output_dim[3]) +
              (j * output_dim[2] * output_dim[3]) + (k * output_dim[3]) + l) =
                0;
          } else {
            *(output + (i * output_dim[1] * output_dim[2] * output_dim[3]) +
              (j * output_dim[2] * output_dim[3]) + (k * output_dim[3]) + l) =
                *(input + (i * inp_dims[1] * inp_dims[2] * inp_dims[3]) +
                  (x * inp_dims[2] * inp_dims[3]) + (y * inp_dims[3]) + l);
            flag = 1;
          }
        }
        if (flag == 1) {
          y++;
          if (y >= inp_dims[2]) {
            y = 0;
            x++;
          }
          if (x >= inp_dims[1]) x = 0;
          flag = 0;
        }
      }
    }
  }
  return output;
}

void executeGraphPass::myconv2d(float *result, convParams params,
                                const float *input, const float *kernel) {
  convParamsType conv_params;
  maxpoolParamsType maxpool_params;

  conv_params.Kx = params.Kx;
  conv_params.Ky = params.Ky;
  conv_params.Sx = params.Sx;
  conv_params.Sy = params.Sy;
  conv_params.Ix = params.Ix;
  conv_params.Iy = params.Iy;
  conv_params.Iz = params.Iz;
  conv_params.Oz = params.Oz;

  // no maxpooling (K=S=1)
  maxpool_params.reset = 0;
  maxpool_params.cflush = 0;
  maxpool_params.Ix =
      calc_output_dim(conv_params.Ix, conv_params.Kx, conv_params.Sx);
  maxpool_params.Iy =
      calc_output_dim(conv_params.Iy, conv_params.Ky, conv_params.Sy);
  maxpool_params.Iz = conv_params.Oz;
  maxpool_params.Kx = 1;
  maxpool_params.Ky = 1;
  maxpool_params.Sx = 1;
  maxpool_params.Sy = 1;
  maxpool_params.Ox =
      calc_output_dim(maxpool_params.Ix, maxpool_params.Kx, maxpool_params.Sx);
  maxpool_params.Oy =
      calc_output_dim(maxpool_params.Iy, maxpool_params.Ky, maxpool_params.Sy);
  maxpool_params.relu = false;

  fpga_tensor<const float> inpTensor(1, conv_params.Iy, conv_params.Ix,
                                     conv_params.Iz, input);
  fpga_tensor<const float> wtTensor(conv_params.Ky, conv_params.Kx,
                                    conv_params.Iz, conv_params.Oz, kernel);
  fpga_tensor<const float> biasTensor =
      make_fpga_tensor<const float, float>(1, 1, 1, conv_params.Oz, float(0));
  fpga_tensor<float> cpu_outTensor(1, maxpool_params.Oy, maxpool_params.Ox,
                                   maxpool_params.Iz);
  fpga_tensor<float> fpga_outTensor(1, maxpool_params.Oy, maxpool_params.Ox,
                                    maxpool_params.Iz, result);

  fpga_conv(fpga_outTensor, conv_params, maxpool_params, inpTensor, biasTensor,
            wtTensor,__NUM_PE_PER_CORE);
}

void executeGraphPass::visitConvolution(fConvolution *node) {
  fNode *input = node->operands[0];
  fNode *weight = node->operands[1];
  if(input==nullptr){
    LOG(INFO)<<"ERROR: input fnode to conv is nullptr";
    exit(1);
  }

  LOG(INFO)<<node->getNodeType()<<" id="<<node->id;
  LOG(INFO)<<"input buffer size = "<<input->get_result_size_in_bytes();
  int size = node->get_result_size_in_bytes();
  float *result = (float *)new char[size];
  std::vector<int> inp_dims = {node->params.batch, node->params.Iy,
                               node->params.Ix, node->params.Iz};
  if(input->result==nullptr){
    LOG(INFO)<<"ifmap nullptr in conv";
    exit(1);
  }
  if(weight->result==nullptr){
    LOG(INFO)<<"weight nullptr in conv";
    exit(1);
  }
  std::unique_ptr<float> padded_input(
      padding((const float *)input->result, inp_dims, node->padding.top,
              node->padding.bottom, node->padding.left, node->padding.right));
  myconv2d(result, node->params, padded_input.get(),
           (const float *)weight->result);
  node->result = (void *)result;
}
void executeGraphPass::visitConstant(fConstant *node) {
  /* NOP : result already as ptr to const*/
}
void executeGraphPass::visitAdd(fAdd *node) {
  int size = node->get_result_size_in_bytes();
  float *result = (float *)new char[size];
  const float *opA = (const float *)node->operands[0]->result;
  const float *opB = (const float *)node->operands[1]->result;
  int N = node->result_dims[0];
  int H = node->result_dims[1];
  int W = node->result_dims[2];
  int C = node->result_dims[3];
  for (int n = 0; n < N; n++) {
    for (int h = 0; h < H; h++) {
      for (int w = 0; w < W; w++) {
        for (int c = 0; c < C; c++) {
          int linear_idx = n * H * W * C + h * W * C + w * C + c;
          result[linear_idx] = opA[linear_idx] + opB[linear_idx];
        }
      }
    }
  }
  node->result = (void *)result;
}
void executeGraphPass::visitMaximum(fMaximum *node) {
  int size = node->get_result_size_in_bytes();
  float *result = (float *)new char[size];
  const float *opA = (const float *)node->operands[0]->result;
  const float *opB = (const float *)node->operands[1]->result;
  int N = node->result_dims[0];
  int H = node->result_dims[1];
  int W = node->result_dims[2];
  int C = node->result_dims[3];
  for (int n = 0; n < N; n++) {
    for (int h = 0; h < H; h++) {
      for (int w = 0; w < W; w++) {
        for (int c = 0; c < C; c++) {
          int linear_idx = n * H * W * C + h * W * C + w * C + c;
          result[linear_idx] = (opA[linear_idx] > opB[linear_idx])
                                   ? opA[linear_idx]
                                   : opB[linear_idx];
        }
      }
    }
  }
  node->result = (void *)result;
}
void executeGraphPass::visitSubtract(fSubtract *node) {
  int size = node->get_result_size_in_bytes();
  float *result = (float *)new char[size];
  const float *opA = (const float *)node->operands[0]->result;
  const float *opB = (const float *)node->operands[1]->result;
  int N = node->result_dims[0];
  int H = node->result_dims[1];
  int W = node->result_dims[2];
  int C = node->result_dims[3];
  for (int n = 0; n < N; n++) {
    for (int h = 0; h < H; h++) {
      for (int w = 0; w < W; w++) {
        for (int c = 0; c < C; c++) {
          int linear_idx = n * H * W * C + h * W * C + w * C + c;
          result[linear_idx] = opA[linear_idx] - opB[linear_idx];
        }
      }
    }
  }
  node->result = (void *)result;
}

void executeGraphPass::visitBroadcast(fBroadcast *node) {
  // result dims are asserted to be 4 by graph_builder.h
  int N = node->result_dims[0];
  int H = node->result_dims[1];
  int W = node->result_dims[2];
  int C = node->result_dims[3];
  std::vector<int> &inp_dims = node->operands[0]->result_dims;
  std::vector<int> &dim_sel = node->params.broadcast_dims;

  float *result = (float *)new char[node->get_result_size_in_bytes()];
  const float *operand = (const float *)node->operands[0]->result;
  // For 2D to 4D broadcast dim_sel can have [4,4,3,1]
  // first two 4's ensure dim_sel indexes to value zero in out_iters
  // next 3,1 indicates W,C dims.
  // This means we get operand[0+0+W*inp_dims[3]+C]
  for (int n = 0; n < N; n++) {
    for (int h = 0; h < H; h++) {
      for (int w = 0; w < W; w++) {
        for (int c = 0; c < C; c++) {
          int out_iters[5] = {n, h, w, c, 0};
          result[n * (H * W * C) + h * (W * C) + w * (C) + c] =
              operand[out_iters[dim_sel[0]] *
                          (inp_dims[1] * inp_dims[2] * inp_dims[3]) +
                      out_iters[dim_sel[1]] * (inp_dims[2] * inp_dims[3]) +
                      out_iters[dim_sel[2]] * (inp_dims[3]) +
                      out_iters[dim_sel[3]]];
        }
      }
    }
  }
  node->result = result;
}
void executeGraphPass::visitReduceWindow(fReduceWindow *node) {
  LOG(INFO)<<"executing ReduceWindow";
  std::vector<int> inp_dims = node->operands[0]->result_dims;
  // FIXME: Actually it should be padded with -INF not zero
  // We can still use it bcz maxpooling happens after relu
  float *padded_input = padding(
      (const float *)node->operands[0]->result, inp_dims, node->padding.top,
      node->padding.bottom, node->padding.left, node->padding.right);

  std::vector<int> padded_dims = std::move(inp_dims);
  padded_dims[1] += node->padding.top + node->padding.bottom;
  padded_dims[2] += node->padding.left + node->padding.right;

  int N = node->result_dims[0];
  int H = node->result_dims[1];
  int W = node->result_dims[2];
  int C = node->result_dims[3];
  int Sx = node->params.Sx;
  int Sy = node->params.Sy;
  int Kx = node->params.Kx;
  int Ky = node->params.Ky;
  float *result = (float *)new char[node->get_result_size_in_bytes()];
  float *result_start_ptr = result;
  for (int n = 0; n < N; n++) {
    for (int h = 0; h < H; h += Sy) {
      for (int w = 0; w < W; w += Sx) {
        for (int c = 0; c < C; c++) {
          float max = 0.0;
          // FIXME: In TF maximum is actually -INF.
          // We can use zero bcz there is a relu before every maxpool node in
          // vgg-net
          for (int kx = 0; kx < Kx; kx++) {
            for (int ky = 0; ky < Ky; ky++) {
              max =
                  std::fmax(max, padded_input[n * H * W * C + (h + ky) * W * C +
                                              (w + kx) * C + c]);
            }
          }
          *result = max;
          result++;
        }
      }
    }
  }
  node->result = result_start_ptr;
}
void executeGraphPass::visitReshape(fReshape *node) {
  node->result = node->operands[0]->result;
}
void executeGraphPass::visitTuple(fTuple *node) {
  // FIXME: We assume tuple has only one element. Add a compile time check for
  // this
  if(node->operands.size()==0) {
    LOG(INFO)<<"tuple with zero result size found";
    return;//Initially some graphs come which have a tuple only
  }
  node->result = node->operands[0]->result;
}
void executeGraphPass::visitGetTupleElement(fGetTupleElement *node) {
  // FIXME: We assume tuple has only one element. Add a compile time check for
  // this
  if(node->operands.size()==0){ 
    LOG(ERROR)<<"fGetTupleElement has no operand!";
  }
  node->result = node->operands[0]->result;
}
void executeGraphPass::visitFusedConv(fFusedConv *node) {
  // UNIMPLEMENTED
  LOG(INFO) << "ERROR: Unimplemented!";
  exit(1);
}
void executeGraphPass::visitParameter(fParameter *node) {
  // assignParameters should have been called by now.
  if (node->result == nullptr) {
    LOG(INFO) << "ERROR: No assigned parameters found. "
                 "executeGraphPass::assignParameters must be called before "
                 "executing the parameter node";
  }
}
}
}
