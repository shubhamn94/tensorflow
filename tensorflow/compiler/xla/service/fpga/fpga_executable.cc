/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include "tensorflow/compiler/xla/service/fpga/fpga_executable.h"
#include "tensorflow/compiler/xla/service/executable.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <set>
#include <string>
#include <utility>
#include <vector>

#include "absl/memory/memory.h"
#include "absl/strings/str_cat.h"
#include "absl/strings/str_format.h"
#include "absl/strings/str_join.h"
#include "tensorflow/compiler/xla/service/buffer_assignment.h"
#include "tensorflow/compiler/xla/service/fpga/fpga_executor.h"
#include "tensorflow/compiler/xla/service/fpga/fpgraph.h"
#include "tensorflow/compiler/xla/service/hlo_computation.h"
#include "tensorflow/compiler/xla/service/hlo_execution_profile.h"
#include "tensorflow/compiler/xla/service/hlo_instruction.h"
#include "tensorflow/compiler/xla/service/hlo_module.h"
#include "tensorflow/compiler/xla/service/logical_buffer.h"
#include "tensorflow/compiler/xla/service/shaped_buffer.h"
#include "tensorflow/compiler/xla/shape_util.h"
#include "tensorflow/compiler/xla/status_macros.h"
#include "tensorflow/compiler/xla/types.h"
#include "tensorflow/compiler/xla/util.h"
#include "tensorflow/compiler/xla/xla_data.pb.h"
#include "tensorflow/core/lib/core/errors.h"
#include "tensorflow/core/lib/core/status.h"
#include "tensorflow/core/platform/env.h"
#include "tensorflow/core/platform/mem.h"
#include "tensorflow/core/platform/mutex.h"
#include "tensorflow/core/platform/stream_executor_no_cuda.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/stream_executor/host/host_stream.h"
namespace xla {
namespace fpga {

//using tensorflow::bfloat16;

FpgaExecutable::FpgaExecutable(
    std::unique_ptr<const BufferAssignment> assignment,
    std::unique_ptr<Graph> graph, 
    fGraph_tf* fgraph_ptr, 
    std::unique_ptr<const HloModule> hlo_module)
    : Executable(std::move(hlo_module), /*hlo_profile_printer=*/nullptr,
                 /*hlo_profile_index_map=*/nullptr),
      assignment_(std::move(assignment)),
      fgraph(fgraph_ptr), 
      graphExecutor(fgraph_ptr),
      graph_(std::move(graph)){
      }

FpgaExecutable::~FpgaExecutable() {}
StatusOr<std::pair<std::vector<se::DeviceMemoryBase>,
                   std::vector<OwningDeviceMemory>>>
FpgaExecutable::GetBufferPointer(
    DeviceMemoryAllocator* memory_allocator, int device_ordinal,
    absl::Span<const ShapedBuffer* const> arguments) {
  std::vector<se::DeviceMemoryBase> unowning_buffers(
      assignment_->Allocations().size());
  std::vector<OwningDeviceMemory> owning_buffers(
      assignment_->Allocations().size());

  // LOG(INFO) << "Allocating "<< assignment_->Allocations().size()
  //        <<" allocations for hlo module "<< module().name();

  for (BufferAllocation::Index i = 0; i < assignment_->Allocations().size();
       ++i) {
    auto& allocation = assignment_->GetAllocation(i);

    // LOG(INFO) << allocation.ToString();

    if (allocation.is_entry_computation_parameter()) {
      auto data_buffer = arguments[allocation.parameter_number()]->buffer(
          allocation.param_shape_index());
      // LOG(INFO) << "Allocation #" << i <<" is a parameter";
      /*se::DeviceMemoryBase buff(nullptr,allocation.size(),false);
        buff =
        arguments[allocation.parameter_number()]->buffer(allocation.param_shape_index());*/

      auto it =
          graph_->parameter_to_node_map.find(allocation.parameter_number());
      if (it != graph_->parameter_to_node_map.end()) {
        Node* vertex = it->second;
        if (vertex->tag == 4) {
          if (vertex->node_type_.p.param_number ==
              allocation.parameter_number()) {
            void* buff = new char[data_buffer.size()];
            vertex->node_type_.p.set_host_kernel_ptr(buff);
            memcpy(vertex->node_type_.p.get_host_kernel_ptr(),
                   data_buffer.opaque(), data_buffer.size());
            vertex->node_type_.p.set_param_size(data_buffer.size());
          }
        }
      }
      continue;
    }

    if (allocation.is_constant()) {
      // LOG(INFO) << "Allocation #" << i <<" is a constant";
      continue;
    }

    if (allocation.is_thread_local()) {
      // LOG(INFO) << "buffer #" << i <<" is thread-local";
      continue;
    }

    int64 buffer_size = allocation.size();
    if (!owning_buffers[i].is_null()) {
      // LOG(INFO) << "buffer #" <<i
      //<<" is in the preallocated result ShapedBuffer";
    }

    else {
      TF_ASSIGN_OR_RETURN(owning_buffers[i], memory_allocator->Allocate(
                                                 device_ordinal, buffer_size));
      unowning_buffers[i] = owning_buffers[i].AsDeviceMemoryBase();

      // LOG(INFO) << "buffer #" << i << " allocated " << buffer_size << " bytes
      // ["
      //<< owning_buffers[i].opaque() << "]";
    }
    // Since the output buffer and all the temporary buffers were written into
    // by the JITed code, msan has no way of knowing their memory was
    // initialized. Mark them initialized so that msan doesn't flag loads from
    // these buffers.
    TF_ANNOTATE_MEMORY_IS_INITIALIZED(owning_buffers[i].opaque(), buffer_size);
  }

  // if(graph_->kernel_transfer_flag == 1) {
  //    LOG(INFO) << "Kernels have been already transferred. Sending Input.. ";
  //    if(graph_->input_node != nullptr) {
  //        LOG(INFO) << "Input node Parameter is:
  //        "<<graph_->input_node->node_type_.p.param_number;
  //        const float* pt = static_cast<const
  //        float*>(graph_->input_node->node_type_.p.get_host_kernel_ptr());
  //        LOG(INFO) << "Input values are ";
  //        for(int i = 0; i < 50; i++) {
  //            LOG(INFO) << *pt;
  //            pt++;
  //        }
  //        //send input node
  //    }
  //}

  // if(graph_->kernel_transfer_flag == 0) {
  //    //send data send(constants, parameters)
  //    graph_->kernel_transfer_flag = 1;
  //    LOG(INFO) << "Kernels have been transferred successfully";
  //}

  TF_ASSIGN_OR_RETURN(const BufferAllocation::Slice result_slice,
                      assignment_->GetUniqueTopLevelOutputSlice());
  // LOG(INFO) << "result index: " << result_slice.index();

  return {{std::move(unowning_buffers), std::move(owning_buffers)}};
}

StatusOr<ScopedShapedBuffer> FpgaExecutable::ExecuteOnStream(
    const ServiceExecutableRunOptions* run_options,
    absl::Span<const ShapedBuffer* const> arguments,
    HloExecutionProfile* hlo_execution_profile) {
  se::Stream* stream = run_options->stream();
  se::StreamExecutor* executor = stream->parent();
  const se::Platform* platform = executor->platform();

  if (GetRootPointsToSet().IsAmbiguous()) {
    return Unimplemented("Points-to set of the root instruction is ambiguous");
  }

  auto* host_stream = dynamic_cast<se::host::HostStream*>(
      run_options->stream()->implementation());
  DeviceMemoryAllocator* memory_allocator = run_options->allocator();
  std::vector<OwningDeviceMemory> owning_buffers;
  std::vector<se::DeviceMemoryBase> unowning_buffers;
  TF_ASSIGN_OR_RETURN(
      std::tie(unowning_buffers, owning_buffers),
      GetBufferPointer(memory_allocator, stream->parent()->device_ordinal(),
                       arguments));

  TF_ASSIGN_OR_RETURN(
      ScopedShapedBuffer result,
      CreateResultShapedBuffer(run_options, absl::MakeSpan(owning_buffers)));

  const HloComputation* computation = module().entry_computation();
  if (computation->num_parameters() != arguments.size()) {
    return tensorflow::errors::Internal(
        "Mismatch between argument count and graph parameter count.");
  }
  if (graph_->num_parameters() != arguments.size()) {
    return tensorflow::errors::Internal(
        "Mismatch between argument count and graph parameter count. ");
  }
  
  printGraphPass graphPrinter(*fgraph.get());
  graphPrinter.printDotfile("fgraph_dumps/fgraph");
  // At this point, `unowning_buffers` contains unowning pointers to all of our
  // buffers, and `buffers` contains owning pointers to the non-live-out
  // buffers.  Enqueue a task which keeps alive the non-live-out buffers.
  //
  // Logically we want this lambda to capture `buffers` by move, ultimately our
  // functor needs to be wrapped in an std::function, and that requires its
  // functor to be copyable.  Thus we perpitrate the hack of capturing buffers
  // "by shared pointer".
  //
  // We also need to change the types of some of the variables we capture:
  // run_options needs to change from a pointer to a value type, and arguments
  // needs to change from a Span into a vector.  We use a struct instead
  // of a lambda to make this explicit.
  
  graphExecutor.assignParameters(arguments,assignment_);
  struct AsyncRunTask {
    FpgaExecutable* executable;
    ServiceExecutableRunOptions run_options;
    std::vector<se::DeviceMemoryBase> unowning_buffers;
    std::shared_ptr<std::vector<OwningDeviceMemory>> buffers;
    HloExecutionProfile* hlo_execution_profile;

    void operator()() {
      // Failing a CHECK here is not great, but I don't see an obvious way to
      // return a failed Status asynchronously.
      TF_CHECK_OK(executable->ExecuteComputeFunction(
          &run_options.run_options(), unowning_buffers, hlo_execution_profile));
    }
  };

  host_stream->EnqueueTask(
      AsyncRunTask{this, *run_options, std::move(unowning_buffers),
                   std::make_shared<std::vector<OwningDeviceMemory>>(
                       std::move(owning_buffers)),
                   hlo_execution_profile});

  //std::cout << result.ToString();
  // std::cout<<result.buffer(Idx);
  return std::move(result);
}

StatusOr<ScopedShapedBuffer> FpgaExecutable::CreateResultShapedBuffer(
    const ServiceExecutableRunOptions* run_options,
    absl::Span<OwningDeviceMemory> buffers) {
  se::Stream* stream = run_options->stream();
  ScopedShapedBuffer result_buffer(
      /*on_host_shape=*/result_shape(),
      /*on_device_shape=*/result_shape(), run_options->allocator(),
      stream->parent()->device_ordinal());

  // Move OwningDeviceMemory values which contain the array(s) of the result
  // into the respective location in ScopedShapedBuffer which is returned to the
  // caller.
  TF_RETURN_IF_ERROR(result_buffer.buffers().ForEachMutableElementWithStatus(
      [&](const ShapeIndex& index, se::DeviceMemoryBase* device_memory) {
        const auto& sources = this->GetRootPointsToSet().element(index);
        // The points to set is unambiguous so the set should be a
        // singleton.
        CHECK_EQ(1, sources.size());
        const LogicalBuffer* buffer_source = sources[0];
        HloInstruction* src = buffer_source->instruction();
        string str = buffer_source->ToString();
        std::cout << "Instruction is: " << str << "\n";
        // Idx = buffer_source->index()
        // The source for this result buffer can be a nested buffer such as
        // a tuple element. The source instruction should have a
        // non-parameter buffer assigned.
        TF_ASSIGN_OR_RETURN(
            const BufferAllocation::Slice slice,
            this->assignment_->GetUniqueSlice(src, buffer_source->index()));
        CHECK(!slice.allocation()->is_entry_computation_parameter());

        const BufferAllocation::Index buffer_index = slice.index();
        OwningDeviceMemory& buffer = buffers[buffer_index];
        CHECK(!buffer.is_null() || buffer.size() == 0);
        *device_memory = buffer.Forget();
        return Status::OK();
      }));

  return std::move(result_buffer);
}

Status FpgaExecutable::ExecuteComputeFunction(
    const ExecutableRunOptions* run_options,
    absl::Span<const se::DeviceMemoryBase> buffers,
    HloExecutionProfile* hlo_execution_profile) {
  // The calling convention for JITed functions is:
  //
  //  void function(void* result, const void* run_options, void** args_array,
  //                void** buffer_table)
  //
  // result: Points at the result.
  // run_options: the ExecutableRunOptions object.
  // args_array: null
  // buffer_table: An array of pointers, containing pointers to temporary
  //   buffers required by the executable adn pointers to entry computation
  //   parameters.
  //

  uint64 start_micros = tensorflow::Env::Default()->NowMicros();

  size_t profile_counters_size =
      hlo_execution_profile ? hlo_execution_profile->profile_counters().size()
                            : 0;
  int64* profile_counters =
      hlo_execution_profile
          ? hlo_execution_profile->mutable_profile_counters()->data()
          : nullptr;

  // Call the computation function following the calling convention.
  std::vector<void*> buffer_pointers;
  for (auto& buffer : buffers) {
    buffer_pointers.push_back(const_cast<void*>(buffer.opaque()));
  }
  TF_ASSIGN_OR_RETURN(const BufferAllocation::Slice result_slice,
                      assignment_->GetUniqueTopLevelOutputSlice());
  void* result_buffer = buffer_pointers[result_slice.index()];
  int64 result_buffer_size = result_slice.size();
  if (1) {  // VLOG_IS_ON(3)) {
    // LOG(INFO) << "Executing compute function:";
    // LOG(INFO) << absl::StrFormat(
    //    "  func(void* result, void* params[null], void* buffer_table[%u], "
    //    "uint64 profile_counters[%u])",
    //    buffer_pointers.size(), profile_counters_size);
    // LOG(INFO) << absl::StrFormat("    result = %p", result_buffer);
    auto ptr_printer = [](string* out, const void* p) {
      absl::StrAppend(out, absl::StrFormat("%p", p));
    };
    // LOG(INFO) << "    params = nullptr";
    // LOG(INFO) << absl::StrFormat(
    //    "    buffer_table = [%s]",
    //    absl::StrJoin(buffer_pointers, ", ", ptr_printer));
    // LOG(INFO) << absl::StrFormat("    profile_counters = %p",
    // profile_counters);
  }

  // LOG(INFO) << "Compute function is about to execute";

  tensorflow::mutex_lock lock(graph_evaluator_lock_);

  // LOG(INFO) << "Result buffer size is: "<<result_buffer_size;

  // LOG(INFO) <<" Node getting executed is: "<<graph_.get()->root->tag;
  // LOG(INFO) << "Tuple operands are: "<<graph_.get()->root->operands.size();

  // Execute Function to execute the Graph and compute results
  //execute_function.Execute_Graph(
  //    result_buffer, run_options, graph_.get(),fgraph.get(), profile_counters,
  //    result_buffer_size);  // buffer_pointers.data(),
  this->graphExecutor.run(result_buffer,result_buffer_size);
  // Updating the global Epoch for current execution
  if (graph_.get()->global_epoch == 0)
    graph_.get()->global_epoch = 1;
  else
    graph_.get()->global_epoch = 0;

  // LOG(INFO) << "Compute function over FPGA has executed successfully";

  uint64 end_micros = tensorflow::Env::Default()->NowMicros();

  {
    tensorflow::mutex_lock lock(mutex_);
    const double nanoseconds = (end_micros - start_micros) * 1000.0;
    execution_profile_.set_compute_time_ns(std::max(nanoseconds, 1.0));
    // If hlo profiling was disabled then the cycle count is left empty.
    if (hlo_execution_profile) {
      execution_profile_.set_compute_cycle_count(
          hlo_execution_profile->total_cycles_executed(
              *module().entry_computation()));
    }
  }

  std::cout << result_slice.ToString();
  return Status::OK();
}

StatusOr<ScopedShapedBuffer> FpgaExecutable::ExecuteAsyncOnStream(
    const ServiceExecutableRunOptions* run_options,
    absl::Span<const ShapedBuffer* const> arguments) {
  return tensorflow::errors::Unimplemented(
      "ExecuteAsyncOnStream is not yet supported on Fpga.");
}

/*static*/ int64 FpgaExecutable::ShapeSizeBytes(const Shape& shape) {
  if (ShapeUtil::IsOpaque(shape)) {
    return sizeof(void*);
  }
  return ShapeUtil::ByteSizeOf(shape, sizeof(void*));
}

const PointsToSet& FpgaExecutable::GetRootPointsToSet() const {
  return assignment_->points_to_analysis().GetPointsToSet(
      module().entry_computation()->root_instruction());
}

}  // namespace fpga
}  // namespace xla
