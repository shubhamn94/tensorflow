#include "fGraph.h"
namespace xla {
namespace fpga {
class verifyGraphPass : public graphVisitor {
  FILE* fp;
  bool isEq(std::vector<int>& left, std::vector<int>& right) {
    if (left.size() != right.size()) {
      return false;
    } else {
      for (int i = 0; i < left.size(); i++) {
        if (left[i] != right[i]) {
          return false;
        }
      }
    }
    return true;
  }
  void assertBinOpOperandShapesCorrect(fNode* node) {
    for (auto operand : node->operands) {
      bool size_eq = isEq(operand->result_dims, node->result_dims);
      if (!size_eq) {
        LOG(INFO) << "ERROR: Maximum node expects operand to have same dims as "
                     "result. Found operand "
                  << operand->getNodeType();
      }
    }
  }
  void assertTypeIsSupported(fNode* node) {
    if (node->result_type != PrimitiveType::F32 &&
        !dynamic_cast<fTuple*>(node)) {
      // Tuples hold tuple of f32. Hence we dont check tuple node
      LOG(INFO)
          << "ERROR: In node " << node->getNodeType()
          << " : We don't support data types other than float32 for this node."
          << "Found " << node->result_type;
      exit(1);
    }
  }

  void assert4DTensor(fNode* node) {
    if (node->result_layout.size() != 4) {
      LOG(INFO) << "ERROR:In node " << node->getNodeType()
                << " : Only 4 dimensional tensors are supported";
      exit(1);
    }
  }
  void assertCLayout(fNode* node) {
    int n = node->result_layout.size();
    for (auto v : node->result_layout) {
      n--;
      if (v != n) {
        LOG(INFO) << "ERROR: In node " << node->getNodeType()
                  << " : Only c-style layout (3210) is supported";
        exit(1);
      }
    }
  }

 public:
  verifyGraphPass(fGraph& graph) : graphVisitor(graph) {}
  void run() { visitPostOrder(); }
  void pre_visitFNode(fNode* node) { assertTypeIsSupported(node); }
  void visitConvolution(fConvolution* node) {
    assert4DTensor(node);
    assertCLayout(node);
  }
  void visitReduceWindow(fReduceWindow* node) {
    assert4DTensor(node);
    assertCLayout(node);
  }
  void visitMaximum(fMaximum* node) {
    assertCLayout(node);
    assert4DTensor(node);
    assertBinOpOperandShapesCorrect(node);
  }

  void visitAdd(fAdd* node) {
    assertCLayout(node);
    assert4DTensor(node);
    assertBinOpOperandShapesCorrect(node);
  }
  void visitSubtract(fAdd* node) {
    assertCLayout(node);
    assert4DTensor(node);
    assertBinOpOperandShapesCorrect(node);
  }
  void visitBroadcast(fBroadcast* node){
    assertCLayout(node);
    assert4DTensor(node);
  }
};
}
}
