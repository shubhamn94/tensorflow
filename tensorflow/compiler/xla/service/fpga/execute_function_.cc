/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include "tensorflow/compiler/xla/service/fpga/execute_function.h"

#include <iostream>
#include <vector>
#include "absl/types/span.h"
#include "tensorflow/compiler/xla/service/fpga/fpgraph.h"
#include "tensorflow/compiler/xla/status.h"
#include "tensorflow/compiler/xla/types.h"
#include "tensorflow/compiler/xla/xla_data.pb.h"
#include "tensorflow/core/platform/logging.h"

namespace xla {
namespace fpga {

Execute::Execute() {}

~Execute::Execute() {}

void Execute::Execute_Graph(void* result,
                            const ExecutableRunOptions* run_options,
                            const Graph* graph, int64* profile_counters,
                            int64 result_size) {
  const void* result_handle = TraverseAndExecute(graph);
  memcpy(result, result_handle, result_size);
}

void Execute::Execute_Handle(Node* vertex) {
  switch (vertex->tag) {
    case 0:
      if (vertex->child != nullptr)
        result_ptr = Execute_Handle(vertex->child->vertex);
      break;
    case 1:
      Execute_Convolution(vertex);
      break;
    case 2:
      Execute_Dot(vertex);
      break;
    case 3:
      Execute_Broadcast(vertex);
      break;
    case 4:
      Execute_Parameter(vertex);
      break;
    case 5:
      Execute_Constant(vertex);
      break;
    case 6:
      Execute_Add(vertex);
      break;
    case 7:
      Execute_Subtract(vertex);
      break;
    case 8:
      Execute_Reduce(vertex);
      break;
    case 9:
      Execute_Divide(vertex);
      break;
    case 10:
      Execute_ReduceWindow(vertex);
      break;
    case 11:
      Execute_Maximum(vertex);
      break;
    case 12:
      Execute_Reshape(vertex);
      break;
    case 13:
      Execute_Convert(vertex);
      break;
    case 14:
      Execute_Exponential(vertex);
      break;
    case 15:
      Execute_Tuple(vertex);
      break;
    case 16:
      Execute_GetTupleElement(vertex) break;
    Default:
      LOG(INFO) << "Execution Handle for tag " << vertex->tag << " not present";
      break;
  }
}

void* Execute::TraverseAndExecute(const Graph* graph) {
  Node* root_ = graph->root;
  // FillupStack(root_);

  // while(nodestack.empty()) {
  // Node* vertex = nodestack.pop();
  if (vertex->IsVisited == graph->global_epoch) Execute_Handle(root_);
  //}

  return root_->result_ptr;
}

/*void Execute::FillupStack(Node* node) {

    if(node->operand_index == node->operands.size())
        return;
    vertex_T = node->operands[node->operand_index]->vertex;
    node->operand_index++;
    nodestack.push(node);

    while(vertex_T->operands.empty()) {
        if(vertex_T->operand_index == vertex_T->operands.size())
            return;
        vertex_T = vertex_T->operands[vertex_T->operand_index]->vertex;
        vertex_T->operand_index++;
        nodestack.push(vertex_T);
    }

}*/

void Execute_Convolution(Node* convolution) {
  if (convolution->IsVisited != graph->global_epoch) return;

  if (convolution->operand_index < convolution->operands.size()) {
    // FillupStack(convolution);
    Execute_Handle(convolution->operands[operand_index]->vertex);
    convolution->operand_index++;
    // Execute_Handle(nodestack.pop());
  }

  lhs = convolution->operands[0]->vertex;
  rhs = convolution->operands[1]->vertex;

  if (convolution->IsVisited == 0)
    convolution->IsVisited = 1;
  else
    convolution->IsVisited = 0;

  // Resetting operand_index for next execution
  convolution->operand_index = 0;
  return;
}

void Execute_Parameter(Node* parameter) {
  if (convolution->IsVisited != graph->global_epoch) return;

  if (convolution->operand_index < convolution->operands.size()) {
    // FillupStack(convolution);
    Execute_Handle(convolution->operands[operand_index]->vertex);
    convolution->operand_index++;
    // Execute_Handle(nodestack.pop());
  }

  if (convolution->IsVisited == 0)
    convolution->IsVisited = 1;
  else
    convolution->IsVisited = 0;

  // Resetting operand_index for next execution
  convolution->operand_index = 0;
  return;
}
