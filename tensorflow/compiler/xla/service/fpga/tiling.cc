#include <bits/stdc++.h>
#include <utility>
#include "riffa_stream.h"
namespace xla{
  namespace fpga{
struct Tile {
  int x;  // origin
  int y;
  int tx;  // size
  int ty;
};

bool compare(const std::pair<int, Tile> i, const std::pair<int, Tile> j) {
  return i.first > j.first;
}
void print(Tile t) {
  std::cout << std::endl;
  std::cout << "t.x  = " << t.x << std::endl;
  std::cout << "t.y  = " << t.y << std::endl;
  std::cout << "t.tx = " << t.tx << std::endl;
  std::cout << "t.ty = " << t.ty << std::endl;
  std::cout << std::endl;
}

int calc_output_dim(int inp_size, int K, int S) {
  //    std::cout <<std::endl;
  //    std::cout<<"calc_output_dim: S="<<S<<std::endl;
  //    std::cout<<"calc_output_dim: K="<<K<<std::endl;
  //    std::cout<<"calc_output_dim: inp_size="<<inp_size<<std::endl;
  int outp_size = (inp_size - K) / S + 1;
  //    std::cout<<"calc_output_dim: outp_size="<<outp_size<<std::endl;
  return outp_size;
}

int calc_input_dim(int outp_size, int K, int S) {
  // std::cout <<std::endl;
  // std::cout<<"calc_input_dim: S="<<S<<std::endl;
  // std::cout<<"calc_input_dim: K="<<K<<std::endl;
  // std::cout<<"calc_input_dim: outp_size="<<outp_size<<std::endl;
  auto inp_size = (outp_size - 1) * S + K;
  // std::cout<<"calc_input_dim: inp_size="<<inp_size<<std::endl;
  return inp_size;
}

Tile calc_input_tile(Tile outTile, int Kx, int Ky, int Sx, int Sy) {
  Tile inTile;
  inTile.x = outTile.x * Sx;
  inTile.y = outTile.y * Sy;
  inTile.tx = calc_input_dim(outTile.tx, Kx, Sx);
  inTile.ty = calc_input_dim(outTile.ty, Ky, Sy);
  return inTile;
}

std::vector<Tile> calc_input_tiles(std::vector<Tile> outTiles, int Kx, int Ky,
                                   int Sx, int Sy) {
  // std::cout<< "cac_inp_tiles\n";
  std::vector<Tile> inTiles;
  for (auto outTile : outTiles) {
    inTiles.push_back(calc_input_tile(outTile, Kx, Ky, Sx, Sy));
  }
  return inTiles;
}

Tile calc_output_tile(Tile inTile, int Kx, int Ky, int Sx, int Sy) {
  Tile outTile;
  myassert(inTile.x % Sx == 0, "inTile left boundary not aligned to Sx grid\n");
  myassert(inTile.y % Sy == 0, "inTile top boundary not aligned to Sy grid\n");

  outTile.x = inTile.x / Sx;
  outTile.y = inTile.y / Sy;
  outTile.tx = calc_output_dim(inTile.tx, Kx, Sx);
  outTile.ty = calc_output_dim(inTile.ty, Ky, Sy);
  return outTile;
}

int findRoot(int x) {
  myassert(x > 0, "x can not be <=0");
  int root = 0;
  for (int i = 1; i < x; i++) {
    if (i * i > x) {
      return root;
    }
    root = i;
  }
  myassert(root * root == x, "could not find square root");
  return root;
}

void findTilesSS(std::vector<Tile> &outp, Tile inp, convParamsType conv_params,
                 maxpoolParamsType maxpool_params, int num_PEs) {
  // std::cout<<"findTilesSS\n";
  // Find projection of square tiling window in maxpooled output
  int rootX =
      calc_output_dim(findRoot(num_PEs), maxpool_params.Kx, maxpool_params.Sx);
  int rootY =
      calc_output_dim(findRoot(num_PEs), maxpool_params.Ky, maxpool_params.Sy);
  std::cout << "rootX=" << rootX << std::endl;
  std::cout << "rootY=" << rootY << std::endl;
  myassert(rootX >= inp.tx && rootY >= inp.ty, "loc0");
  Tile out;
  out.tx = inp.tx;
  out.ty = inp.ty;
  out.x = inp.x;
  out.y = inp.y;
  outp.push_back(out);
}

void findTilesSL(std::vector<Tile> &outp, Tile inp, convParamsType conv_params,
                 maxpoolParamsType maxpool_params, int num_PEs) {
  // std::cout<<"findTilesSL\n";
  // Find projection of square tiling window in maxpooled output
  int rootX =
      calc_output_dim(findRoot(num_PEs), maxpool_params.Kx, maxpool_params.Sx);
  myassert(inp.tx <= rootX, "loc1");
  int remaining_ty = inp.ty;
  int x = inp.x;
  int y = inp.y;
  while (remaining_ty) {
    Tile out;
    out.x = x;
    out.y = y;
    out.tx = inp.tx;
    bool pending = false;
    for (int ty = 1; ty <= remaining_ty; ty++) {
      int num_required_PEs =
          calc_input_dim(ty, maxpool_params.Ky, maxpool_params.Sy) *
          calc_input_dim(out.tx, maxpool_params.Kx, maxpool_params.Sx);
      if (num_required_PEs > num_PEs) {
        pending = true;
        break;
      } else
        out.ty = ty;
    }
    outp.push_back(out);
    if (pending) {
      remaining_ty -= out.ty;
      y += out.ty;
    } else
      break;
  }
}

void findTilesLS(std::vector<Tile> &outp, Tile inp, convParamsType conv_params,
                 maxpoolParamsType maxpool_params, int num_PEs) {
  // std::cout<<"findTilesLS\n";
  // Find projection of square tiling window in maxpooled output
  int rootY =
      calc_output_dim(findRoot(num_PEs), maxpool_params.Ky, maxpool_params.Sy);
  myassert(inp.ty <= rootY, "loc2");
  int remaining_tx = inp.tx;
  int x = inp.x;
  int y = inp.y;
  while (true) {
    Tile out;
    out.x = x;
    out.y = y;
    out.ty = inp.ty;
    bool pending = false;
    for (int tx = 1; tx <= remaining_tx; tx++) {
      int num_required_PEs =
          calc_input_dim(tx, maxpool_params.Kx, maxpool_params.Sx) *
          calc_input_dim(out.ty, maxpool_params.Ky, maxpool_params.Sy);
      if (num_required_PEs > num_PEs) {
        pending = true;
        break;
      } else
        out.tx = tx;
    }
    outp.push_back(out);
    if (pending) {
      remaining_tx -= out.tx;
      x += out.tx;
    } else
      break;
  }
}

void findTilesLL(std::vector<Tile> &outp, Tile inp, convParamsType conv_params,
                 maxpoolParamsType maxpool_params, int num_PEs) {
  /*
   * Tiling strategy
   |-----|-----|---|
   |     |     |SL |
   | SS  | SS  | v |
   |     |     | s |
   |-----|-----| t |
   |     |     | r |
   | SS  | SS  | i |
   |     |     | p |
   |---------------|
   |   LS hstrip   |
   |---------------|
   *
   * Tiling done in maxpool output
   * inp and outp are tiles of maxpool output
   */

  // Find projection of square tiling window in maxpooled output
  int rootX =
      calc_output_dim(findRoot(num_PEs), maxpool_params.Kx, maxpool_params.Sx);
  int rootY =
      calc_output_dim(findRoot(num_PEs), maxpool_params.Ky, maxpool_params.Sy);
  // std::cout << "rootX="<<rootX<<std::endl;
  // std::cout << "rootY="<<rootY<<std::endl;

  myassert(rootX < inp.tx && rootY < inp.ty, "loc3");
  int x;
  int y;

  for (y = 0; y < (inp.ty - rootY + 1); y += rootY) {
    for (x = 0; x < (inp.tx - rootX + 1); x += rootX) {
      Tile t;
      t.x = x + inp.x;
      t.y = y + inp.y;
      t.tx = rootX;
      t.ty = rootY;
      outp.push_back(t);
    }
  }

  if (x < inp.tx) {
    Tile t;
    t.x = x + inp.x;
    t.y = inp.y;
    t.tx = inp.tx - x;
    t.ty = y;
    findTilesSL(outp, t, conv_params, maxpool_params, num_PEs);  // vstrip
  }

  if (y < inp.ty) {
    Tile t;
    t.x = inp.x;
    t.y = y + inp.y;
    t.ty = inp.ty - y;
    t.tx = inp.tx;
    findTilesLS(outp, t, conv_params, maxpool_params, num_PEs);  // hstrip
  }
}

std::vector<Tile> sortTiles(std::vector<Tile> maxpoolTiles, int Kx, int Ky,
                            int Sx, int Sy) {
  std::vector<std::pair<int, Tile>> tilesWithSize;
  std::vector<Tile> sortedMaxpoolTiles;
  for (auto tile : maxpoolTiles) {
    Tile convTile = calc_input_tile(tile, Kx, Ky, Sx, Sy);
    int size = convTile.tx * convTile.ty;
    tilesWithSize.push_back(std::make_pair(size, tile));
  }
  std::sort(tilesWithSize.begin(), tilesWithSize.end(), compare);
  int prev_size = tilesWithSize[0].first;
  for (auto tileWithSize : tilesWithSize) {
    Tile tile = tileWithSize.second;
    if (!(prev_size >= tile.tx * tile.ty)) {
      std::cout << "sorting error\n";
      assert(false);
    }
    sortedMaxpoolTiles.push_back(tile);
  }
  return sortedMaxpoolTiles;
}

std::vector<Tile> findTiles(Tile inp, convParamsType conv_params,
                            maxpoolParamsType maxpool_params, int num_PEs) {
  std::vector<Tile> outp;
  // Find projection of square tiling window in maxpooled output
  int rootX =
      calc_output_dim(findRoot(num_PEs), maxpool_params.Kx, maxpool_params.Sx);
  int rootY =
      calc_output_dim(findRoot(num_PEs), maxpool_params.Ky, maxpool_params.Sy);

  if (inp.tx <= rootX && inp.ty <= rootY) {
    findTilesSS(outp, inp, conv_params, maxpool_params, num_PEs);
  } else if (inp.tx <= rootX && inp.ty > rootY) {
    findTilesSL(outp, inp, conv_params, maxpool_params, num_PEs);
  } else if (inp.tx > rootX && inp.ty <= rootY) {
    findTilesLS(outp, inp, conv_params, maxpool_params, num_PEs);
  } else {
    findTilesLL(outp, inp, conv_params, maxpool_params, num_PEs);
  }
  return sortTiles(outp, maxpool_params.Kx, maxpool_params.Ky,
                   maxpool_params.Sx, maxpool_params.Sy);
}
}
}
