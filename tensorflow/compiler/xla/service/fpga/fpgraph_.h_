/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/


#ifndef TENSORFLOW_COMPILER_XLA_SERVICE_FPGA_FPGRAPH_H_
#define TENSORFLOW_COMPILER_XLA_SERVICE_FPGA_FPGRAPH_H_

#include <iostream>
#include <vector>
#include "tensorflow/compiler/xla/xla_data.pb.h"
#include "absl/types/span.h"
#include "absl/container/inlined_vector.h"
#include "tensorflow/compiler/xla/types.h"
#include "tensorflow/compiler/xla/status.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/compiler/xla/service/hlo_module.h"

namespace xla {
namespace fpga {

 class Graph {
     public:
         class Node* root = new class Node;
         std::vector<int> queue;
         bool search_queue(int);
         int64 param_count = 0;
         int64 num_parameters();
         std::vector<Node*> input_nodes;
 };

 class Edge {
     public:
         class Node* vertex;
         Edge* next;
 };

 class Instr_Shape {
     public:
         absl::Span<const int64> dimensions;
         absl::Span<const int64> layout;
         PrimitiveType element_type;
 };

 class Operand_Shape {
     public:
         absl::Span<const int64> dimensions;
         absl::Span<const int64> layout;
         PrimitiveType element_type;
 };
 
 class Root {
     public: 
         Instr_Shape *shape;
         Operand_Shape *operand_shape;

         Root() {
             shape = new Instr_Shape;
             operand_shape = new Operand_Shape;
         }
 };

 class convolution {
     public:
         //input tensor
         int64 input_batch, input_rows, input_cols, input_channels;
     
         // kernel tensor
         int64 kernel_rows, kernel_cols, kernel_channels, kernel_filters;
     
         //output tensor
         int64 output_rows, output_cols; 
 
         //window info
         int64 row_stride, col_stride;

         //padding info
         int64 padding_top, padding_bottom, padding_left, padding_right;
      
         //LHS-RHS Dilations
         int64 lhs_row_dilation, lhs_col_dilation, rhs_row_dilation, rhs_col_dilation;
     
         //Shape of convolution inclassion and it's operands
         Instr_Shape *conv_shape;
         Operand_Shape *input_shape;
         Operand_Shape *kernel_shape;

         convolution() {
             conv_shape = new Instr_Shape;
             input_shape = new Operand_Shape;
             kernel_shape = new Operand_Shape;
         }

         ~convolution() {
             delete conv_shape;
             delete input_shape;
             delete kernel_shape;
         }
 
         //Opaque pointer to the assigned buffer for input and kernel Parameters within CPU
         void *input_ptr;
         void *kernel_ptr;
  };

 class Tuple {
     public:
         std::vector<Operand_Shape> operand_shape;
 };

 class get_tuple_element {
     public:
         int64 index;
         Operand_Shape *element_shape;

         get_tuple_element() {
             element_shape = new Operand_Shape;
         }

         ~get_tuple_element() {
             delete element_shape;
         }
 };

 class Broadcast {
     public:
         Instr_Shape *broadcast_shape;
         Operand_Shape *operand_shape;

         Broadcast() {
             broadcast_shape = new Instr_Shape;
             operand_shape = new Operand_Shape;
         }

         ~Broadcast() {
             delete broadcast_shape;
             delete operand_shape;
          }

 };

 class Reshape {
   public:
       Instr_Shape *reshape_shape;
       Reshape() {
           reshape_shape = new Reshape;
       }

       ~Reshape() {
           delete reshape_shape;
       }
 };

 class constant {
   public:
       const void *ptr;
       PrimitiveType pointer_type;		
       absl::Span<const int64> dimensions;
       absl::Span<const int64> minor_to_major;
       int64 element_count;  
       unsigned long int size; //bytes
 };
 
 class dot {
   public:
       absl::Span<const int64> lhs_contr_dim;
       absl::Span<const int64> rhs_contr_dim;
       absl::Span<const int64> lhs_batch_dim;
       absl::Span<const int64> rhs_batch_dim;
       
       //Shape of convolution inclassion and it's operands 
       Instr_Shape *dot_shape;
       Operand_Shape *lhs_shape;
       Operand_Shape *rhs_shape;

       dot() {
           dot_shape = new Instr_shape;
           lhs_shape = new Operand_Shape;
           rhs_shape = new Operand_Shape;
       }

       dot() {
           delete dot_shape;
           delete lhs_shape;
           delete rhs_shape;
       }
 };

 class ElementwiseBinary {
     public:
        Instr_Shape *bin_shape;
        Operand_Shape *lhs_shape;
        Operand_Shape *rhs_shape;

        ElementwiswBinary() {
            bin_shape = new Instr_Shape;
            lhs_shape = new Operand_Shape;
            rhs_shape = new rhs_shape;
        }

        ~ElementwiseBinary() {
            delete bin_shape;
            delete lhs_shape;
            delete rhs_shape;
        }

 };

enum tags{ _root_, kconv, _dot_, _broadcast_, _param_, kcons, add, subtract, reduce, divide, reduce_window_, maximum, _reshape_, convert, exponential, _tuple_, get_tuple_ele, _unimplemented_};
typedef enum tags tags;
 
 class Reduce {
    public:
        int64 input_count;
        absl::Span<const int64> dimensions_to_reduce;
        tags subcomputation_opcode;
        
        std::vector<void const*> init_value_ptr;
        std::vector<PrimitiveType> init_value_type;
        
        std::vector<Operand_Shape> input_shape;
        std::vector<Operand_Shape> init_shape;
        Instr_Shape *reduce_shape;

        Reduce() {
            reduce_shape = new Instr_Shape;
        }

        ~Reduce() { 
            delete reduce_shape;
         }
 };


 class ReduceWindow {
    public:
        Instr_Shape *reducewindow_shape;
        Operand_Shape *lhs_shape, *rhs_shape;
        
        tags subcomputation_opcode; 
        
        std::vector<int> size;
        std::vector<int> stride;
        std::vector<int> padding_low;
        std::vector<int> padding_high;

        ReduceWindow() {
            reducewindow_shape = new Instr_Shape;
            lhs_shape = new Operand_Shape;
            rhs_shape = new Operand_Shape;
        }

        ~ReduceWindow() {
            delete reducewindow_shape;
            delete lhs_shape;
            delete rhs_shape;
        }
 };

 class parameter {
     public:
         void *param_ptr;
         int64 buffer_size;
         int64 param_number;
         Instr_Shape *param_shape;

         parameter() {
             param_shape = new Instr_Shape;
             buffer_size = 0;
             param_number = 0;
         }

         ~parameter() {
             delete param_shape;
         }

 };

 class Default {
     public:
        Instr_Shape *unimplemented_shape;
        std::vector<Operand_Shape> operand_shape;
        int64 operand_count;
        string opcode;

        Default() {
            unimplemented_shape = new Instr_Shape;
            operand_count = 0;
        }

        ~Default() {
            delete unimplemented_shape;
         }
 };

 class Stack {
     public:
         Edge* edge = new Edge; 
         Stack* link = new Stack;

         Stack() {
             edge = nullptr;
             link = nullptr;
         }

         ~Stack() {
             delete edge;
             delete link;
          }
 };

union node_types {
       ~node_types() {}
       Root *root_node;
       convolution *c;
       dot *d;
       parameter *p;
       constant *cons;
       ElementwiseBinary *bin;
       Broadcast *broadcast;
       Reshape *reshape;
       Tuple *tuple_t;
       get_tuple_element *get_tuple_elem;
       Reduce *reduce;
       ReduceWindow *reducewindow;
       Default *unimplemented;
       node_types() {}
       ~node_types() {}
           
};
typedef union node_types node_types;

 class Node {
     public:
        Node() {
            uniqueId = 0;     
            node_type_ = new node_types;        
        }
        ~Node() {
            delete node_type_;        
        }

        int uniqueId;
        tags tag;
        node_types node_type_;
        Edge* child = nullptr;
 };


class FPGraph  {
 
 public: 
 FPGraph() { LOG(INFO)<< "\nCreated Graph object!!";  }
 ~FPGraph();
 
 Graph* get_graph() { return graph; }
 
 Stack* top = nullptr;

 virtual Status CreateGraph(const HloModule&) = 0;
 
 void AddGraphVertex(int, Node*, Graph*);

 std::vector<int> delete_graph_components(Graph*);
 
 Node* DFSGraphTraverse(int, Graph*); 
 
 void DFSGraphTraverse(Graph*);

 void DisplayGraph();

 void AddGraphEdge(Node*, Node*); 

 void push(class Edge*);
 Edge* pop(void);
 void stack_clear();

 private:
 
 Graph* graph = new Graph; 

 Edge* AddGraphEdge(int, Graph*);
 void AddGraphEdge(int, int, Graph*);
};

}  //namespace fpga
}  //namespace xla

#endif //TENSORFLOW_COMPILER_XLA_SERVICE_FPGA_FPGRAPH_H_
