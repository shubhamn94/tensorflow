/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include "tensorflow/compiler/xla/service/fpga/fpga_compiler.h"

#include <string>
#include <utility>

#include "absl/memory/memory.h"
#include "tensorflow/compiler/xla/service/computation_placer.h"
#include "tensorflow/compiler/xla/service/fpga/fpga_executable.h"
#include "tensorflow/compiler/xla/service/fpga/fpgraph.h"
#include "tensorflow/compiler/xla/service/fpga/ir_emitter.h"
#include "tensorflow/compiler/xla/service/hlo_memory_scheduler.h"
#include "tensorflow/compiler/xla/service/hlo_pass_fix.h"
#include "tensorflow/compiler/xla/service/hlo_pass_pipeline.h"
#include "tensorflow/compiler/xla/service/layout_assignment.h"
#include "tensorflow/compiler/xla/status_macros.h"
#include "tensorflow/core/lib/core/errors.h"
#include "tensorflow/core/platform/types.h"

#include "graph_builder.h"
#include "verifyGraphPass.h"

namespace xla {
namespace fpga {

Status FpgaCompiler::RunHloOptimization(HloModule* hlo_module) {
  HloPassPipeline pipeline("Fpga");

  pipeline.AddPass<LayoutAssignment>(
      hlo_module->mutable_entry_computation_layout(),
      LayoutAssignment::InstructionCanChangeLayout);
  return pipeline.Run(hlo_module).status();
}

StatusOr<std::unique_ptr<HloModule>> FpgaCompiler::RunHloPasses(
    std::unique_ptr<HloModule> hlo_module, se::StreamExecutor* /*stream_exec*/,
    DeviceMemoryAllocator* /*device_allocator*/) {
  VLOG(1) << "Run hlo passes on graph " << hlo_module->name();
  TF_RETURN_IF_ERROR(RunHloOptimization(hlo_module.get()));

  return std::move(hlo_module);
}
namespace {
constexpr int64 kMemoryAlignment = 16;
auto memory_alignment = [](LogicalBuffer::Color) { return kMemoryAlignment; };
}

StatusOr<std::unique_ptr<Executable>> FpgaCompiler::RunBackend(
    std::unique_ptr<HloModule> hlo_module, se::StreamExecutor* stream_exec,
    DeviceMemoryAllocator* /*device_allocator*/) {
  const string timer_message =
      "Compiling [" + hlo_module->name() + "] for FPGA";
  XLA_SCOPED_LOGGING_TIMER(timer_message);
  TF_RET_CHECK(stream_exec != nullptr);

  VLOG(1) << "Run backend " << hlo_module->name();

  // Typically you would visit the HLO graph, building up a compiled equivalent
  // In this case we are using an HloEvaluator at execution time, so we don't
  // need to compile anything

  // Select an order for emitting the HLO instructions for each
  // computation. Using this sequence enables tighter buffer liveness analysis
  // and reduced memory usage (as compared to using DependencyHloOrdering).
  TF_ASSIGN_OR_RETURN(
      HloSchedule schedule,
      ScheduleModule(*(hlo_module.get()), BufferSizeBytesFunction(),
                     DFSMemoryScheduler));

  // Run buffer allocation on the HLO graph.
  TF_ASSIGN_OR_RETURN(
      std::unique_ptr<BufferAssignment> assignment,
      BufferAssigner::Run(hlo_module.get(),
                          absl::make_unique<SequentialHloOrdering>(schedule),
                          BufferSizeBytesFunction(), memory_alignment,
                          /*allow_input_output_aliasing=*/false,
                          /*allocate_buffers_for_constants=*/true));
  // BufferAssignment::ToString() includes a header, so no need for us to
  // print one ourselves.
  //XLA_VLOG_LINES(2, assignment->ToString());
  // Calling IR Emitter for Intermediate Graph creation though HLO Module.
  IrEmitter ir_emitter(*(hlo_module.get())) /*, *(assignment.get()))*/;
  auto graph = absl::make_unique<Graph>(*(ir_emitter.GetGraph()));
  
  //kingshuk
  //FIXME: Check there is only one computation per module
  std::unique_ptr<fGraph_tf>fgraph(new fGraph_tf(hlo_module.get()->entry_computation())); 
  verifyGraphPass graphVerifier(*fgraph.get());
  graphVerifier.run();
  if(fgraph.get()==nullptr){
    LOG(INFO)<<"ERROR: fgraph is empty";
    exit(1);
  }
  //
  // Creating FPGA Executable.
  std::unique_ptr<Executable> fpga_executable;
  fpga_executable.reset(new FpgaExecutable(
      std::move(assignment), std::move(graph), fgraph.release(), std::move(hlo_module)));

  VLOG(1) << "Compilation finished";

  return std::move(fpga_executable);
}

StatusOr<std::vector<std::unique_ptr<Executable>>> FpgaCompiler::Compile(
    std::vector<std::unique_ptr<HloModule>> /*hlo_modules*/,
    std::vector<std::vector<se::StreamExecutor*>> /*stream_execs*/,
    DeviceMemoryAllocator* /*device_allocator*/) {
  return tensorflow::errors::Unimplemented(
      "Compilation of multiple HLO modules is not supported on Fpga.");
}

StatusOr<std::vector<std::unique_ptr<AotCompilationResult>>>
FpgaCompiler::CompileAheadOfTime(
    std::vector<std::unique_ptr<HloModule>> hlo_modules,
    const AotCompilationOptions& aot_options) {
  return tensorflow::errors::InvalidArgument(
      "AOT compilation not supported on Fpga");
}

se::Platform::Id FpgaCompiler::PlatformId() const {
  return se::fpga::kXlaFpgaPlatformId;
}

HloCostAnalysis::ShapeSizeFunction FpgaCompiler::ShapeSizeBytesFunction()
    const {
  return FpgaExecutable::ShapeSizeBytes;
}

static bool InitModule() {
  xla::Compiler::RegisterCompilerFactory(se::fpga::kXlaFpgaPlatformId, []() {
    return absl::make_unique<xla::fpga::FpgaCompiler>();
  });
  xla::ComputationPlacer::RegisterComputationPlacer(
      se::fpga::kXlaFpgaPlatformId,
      []() { return absl::make_unique<xla::ComputationPlacer>(); });
  return true;
}

static bool module_initialized = InitModule();

}  // namespace fpga
}  // namespace xla
