/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef TENSORFLOW_COMPILER_XLA_SERVICE_FPGA_KERNEL_H_
#define TENSORFLOW_COMPILER_XLA_SERVICE_FPGA_KERNEL_H_

#include <iostream>
#include "tensorflow/compiler/xla/status.h"
#include "tensorflow/compiler/xla/types.h"
#include "tensorflow/core/platform/logging.h"

namespace xla {
namespace fpga {

class Kernel {
 public:
  Kernel();
  //~Kernel();

  void* get_host_kernel_ptr();

  void* get_fpga_kernel_ptr();

  uint64 get_kernel_byte_size();

  void set_host_kernel_ptr(void*);

  void* Allocate(uint64);
  void Deallocate(void*);

 private:
  uint64 kernel_byte_size;

  // void pointer which will point to newly allocated on host memory location
  void* host_kernel_ptr;

  // void pointer which will point to memory allocated on FPGA memory location
  void* fpga_kernel_ptr;
};

}  // namespace fpga
}  // namespace xla

#endif  // TENSORFLOW_COMPILER_XLA_SERVICE_FPGA_KERNEL_H_
