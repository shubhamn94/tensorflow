/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef TENSORFLOW_COMPILER_XLA_SERVICE_FPGA_EXECUTE_FUNCTION_H_
#define TENSORFLOW_COMPILER_XLA_SERVICE_FPGA_EXECUTE_FUNCTION_H_

#include <iostream>
#include <vector>
#include "absl/types/span.h"
#include "tensorflow/compiler/xla/executable_run_options.h"
#include "tensorflow/compiler/xla/service/fpga/fpgraph.h"
#include "tensorflow/compiler/xla/status.h"
#include "tensorflow/compiler/xla/types.h"
#include "tensorflow/compiler/xla/xla_data.pb.h"
#include "tensorflow/core/platform/logging.h"
#include "fGraph.h"
namespace xla {
namespace fpga {

class Execute {
 public:
  Execute();  // const ExecutableRunOptions* /*run_options*/,
  // std::unique_ptr<Graph> graph/*Graph to be executed*/, int64*
  // /*profile_counters*/, int64 /*result size*/);
  ~Execute();
  void Execute_Convolution(Node*);
  void Execute_Parameter(Node*);
  void Execute_Broadcast(Node*);
  void Execute_Constant(Node*);
  void Execute_Add(Node*);
  void Execute_Subtract(Node*);
  void Execute_Divide(Node*);
  void Execute_ReduceWindow(Node*);
  void Execute_Maximum(Node*);
  void Execute_Reshape(Node*);
  void Execute_Tuple(Node*);
  void Execute_GetTupleElement(Node*);
  void* get_result_handle();
  void Execute_Graph(void* /*result_buffer*/,
                     const ExecutableRunOptions* /*run_options*/,
                     const Graph* /*Graph to be executed*/,
                     const fGraph*, 
                     int64* /*profile_counters*/, int64 /*result size*/);

 private:
  // std::stack<Node*> nodestack; //Will contain graph execution order

  // Recursively Traverse Graph and Execute it
  void* TraverseAndExecute();

  // Handle for execution of each node in the graph
  void Execute_Handle(Node*);

  // Creating graph for comparing global epoch in each node
  const Graph* graph_ = nullptr;  // std::unique_ptr<Graph> graph_;

  // Result Handle
  void* result_handle = nullptr;

  TF_DISALLOW_COPY_AND_ASSIGN(Execute);
};

}  // namespace fpga
}  // namespace xla

#endif  // end namespace
