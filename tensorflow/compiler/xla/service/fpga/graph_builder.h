#ifndef GRAPH_BUILDER_H
#define GRAPH_BUILDER_H
#include "fGraph.h"
#include "tensorflow/compiler/xla/service/hlo_computation.h"
#include "tensorflow/compiler/xla/service/hlo_instruction.h"

namespace xla {
namespace fpga {
// void HandleConvolution(HloInstruction*);
// void HandleParameter(HloInstruction*);
// void HandleConstant(HloInstruction*);
// void HandleAdd(HloInstruction*);
// void HandleMaximum(HloInstruction*);
// void HandleSubtract(HloInstruction*);
// void HandleReshape(HloInstruction*);
// void HandleBroadcast(HloInstruction*);
// void HandleReduceWindow(HloInstruction*);
// void HandleTuple(HloInstruction*);
// void HandleGetTupleElement(HloInstruction*);

class fGraph_tf : public fGraph {
 public:
  void dummy() {}
  fGraph_tf(const HloComputation* computation) {
    for (auto* instruction : computation->instructions()) {
      switch (instruction->opcode()) {
        case HloOpcode::kConvolution: {
          HandleConvolution(instruction);
          break;
        }
        case HloOpcode::kParameter: {
          HandleParameter(instruction);
          break;
        }
        case HloOpcode::kConstant: {
          HandleConstant(instruction);
          break;
        }
        case HloOpcode::kAdd: {
          HandleAdd(instruction);
          break;
        }
        case HloOpcode::kMaximum: {
          HandleMaximum(instruction);
          break;
        }
        case HloOpcode::kSubtract: {
          HandleSubtract(instruction);
          break;
        }
        case HloOpcode::kReshape: {
          HandleReshape(instruction);
          break;
        }
        case HloOpcode::kBroadcast: {
          HandleBroadcast(instruction);
          break;
        }
        case HloOpcode::kReduceWindow: {
          HandleReduceWindow(instruction);
          break;
        }
        case HloOpcode::kTuple: {
          HandleTuple(instruction);
          break;
        }
        case HloOpcode::kGetTupleElement: {
          HandleGetTupleElement(instruction);
          break;
        }
        default: {
          LOG(INFO) << "ERROR: FPGA found unimplemented instruction "
                    << instruction->name();
          exit(1);
        }
      }
    }
    root_node =
        instruction_to_node_map[computation->root_instruction()->unique_id()];
    addEdges(computation);
  }

  std::map<int, fNode*> parameter_to_node_map;

 private:
  std::map<int, fNode*> instruction_to_node_map;

  void addEdges(const HloComputation* computation) {
    for (auto dst_inst : computation->instructions()) {
      auto node = instruction_to_node_map[dst_inst->unique_id()];
      for (auto src_inst : dst_inst->operands()) {
        auto operand = instruction_to_node_map[src_inst->unique_id()];
        node->setOperand(operand);
      }
    }
  }
  void intoVec(std::vector<int>& out, absl::Span<const int64> tf_vec) {
    for (auto elem : tf_vec) {
      out.push_back(elem);
    }
  }
  void intoVec(std::vector<int>& out, std::vector<int64> tf_vec) {
    for (auto elem : tf_vec) {
      out.push_back(elem);
    }
  }
  void HandleGeneric(fNode* node, HloInstruction* inst) {
    // Dimension and layout of Binary instruction
    intoVec(node->result_dims, AsInt64Slice(inst->shape().dimensions()));

    intoVec(node->result_layout,
            AsInt64Slice(inst->shape().layout().minor_to_major()));
    if (node->result_dims.size() != node->result_layout.size()) {
      LOG(INFO) << "ERROR: num dims not matching num layout";
      exit(1);
    }
    if (node->result_dims.size() > 4) {
      LOG(INFO) << "ERROR: We don't support more than 4D tensors (including "
                   "batch dim)";
      exit(1);
    }
    for (int i = node->result_layout.size(); i < 4;
         i++) {  // normalize to 4D tensor
      node->result_dims.insert(node->result_dims.begin(), 1);
      node->result_layout.insert(node->result_layout.begin(), i);
    }
    node->result_type = inst->shape().element_type();

    instruction_to_node_map[inst->unique_id()] = node;
    node->result = nullptr;
  }
  void HandleConvolution(HloInstruction* inst) {
    auto node = addNode<fConvolution>();
    // Shape of operands

    // Dimensions of operands

    auto lhs = inst->operand(0);
    const Shape& convolution_shape = inst->shape();
    bool one_dim_convolution = lhs->shape().dimensions_size() == 3;
    if (one_dim_convolution) {
      LOG(INFO) << "ERROR: Unexpected one dim convolution";
    }

    const ConvolutionDimensionNumbers& dnums =
        inst->convolution_dimension_numbers();

    // Input tensor
    const Shape& input_shape = inst->operand(0)->shape();

    node->params.batch = input_shape.dimensions(dnums.input_batch_dimension());
    node->params.Ix =
        one_dim_convolution
            ? 1
            : input_shape.dimensions(dnums.input_spatial_dimensions(1));
    node->params.Iy = input_shape.dimensions(dnums.input_spatial_dimensions(0));
    node->params.Iz = input_shape.dimensions(dnums.input_feature_dimension());

    // Kernel tensor
    const Shape& kernel_shape = inst->operand(1)->shape();
    node->params.Ky =
        kernel_shape.dimensions(dnums.kernel_spatial_dimensions(0));
    node->params.Kx =
        one_dim_convolution
            ? 1
            : kernel_shape.dimensions(dnums.kernel_spatial_dimensions(1));
    node->params.Oz =
        kernel_shape.dimensions(dnums.kernel_output_feature_dimension());

    // Output tensor
    node->params.Oy =
        convolution_shape.dimensions(dnums.output_spatial_dimensions(0));
    node->params.Ox =
        one_dim_convolution
            ? 1
            : convolution_shape.dimensions(dnums.output_spatial_dimensions(1));

    // Extract the window stride for the convolution
    const Window& window = inst->window();
    node->params.Sy = window.dimensions(0).stride();
    node->params.Sx = one_dim_convolution ? 1 : window.dimensions(1).stride();

    node->padding.top = window.dimensions(0).padding_low();
    node->padding.bottom = window.dimensions(0).padding_high();
    node->padding.left =
        one_dim_convolution ? 0 : window.dimensions(1).padding_low();
    node->padding.right =
        one_dim_convolution ? 0 : window.dimensions(1).padding_high();

    // FIXME: Check below 4 to ensure dilation is not there
    // window.dimensions(0).base_dilation();
    // one_dim_convolution ? 1 : window.dimensions(1).base_dilation();
    // window.dimensions(0).window_dilation();
    // one_dim_convolution ? 1 : window.dimensions(1).window_dilation();

    HandleGeneric(node, inst);
  }

  void HandleParameter(HloInstruction* inst) {
    auto node = addNode<fParameter>();
    HandleGeneric(node, inst);
    node->parameter_number = inst->parameter_number();
    parameter_to_node_map[inst->parameter_number()] = node;
  }

  void HandleAdd(HloInstruction* inst) {
    auto node = addNode<fAdd>();
    HandleGeneric(node, inst);
  }
  void HandleSubtract(HloInstruction* inst) {
    auto node = addNode<fSubtract>();
    HandleGeneric(node, inst);
  }
  void HandleMaximum(HloInstruction* inst) {
    auto node = addNode<fMaximum>();
    HandleGeneric(node, inst);
  }
  void HandleBroadcast(HloInstruction* inst) {
    auto node = addNode<fBroadcast>();
    if (!node->result_dims.size() == 4) {
      LOG(INFO) << "Only Broadcasting to 4D tensors is supported";
    }
    HandleGeneric(node, inst);
    intoVec(node->params.broadcast_dims, inst->dimensions());
    for (int i = node->params.broadcast_dims.size(); i < 4;
         i++) {  // normalize to 4D tensor
      node->params.broadcast_dims.insert(node->params.broadcast_dims.begin(),
                                         4);
      // 4 used by executeGraphPass.h to index to zero value
    }
  }
  void HandleReshape(HloInstruction* inst) {
    auto node = addNode<fReshape>();
    HandleGeneric(node, inst);
  }
  void HandleTuple(HloInstruction* inst) {
    auto node = addNode<fTuple>();
    HandleGeneric(node, inst);
    if (node->operands.size() == 0) {
      node->result_type = PrimitiveType::PRIMITIVE_TYPE_INVALID;
    } else {
      node->result_type =
          node->operands[0]->result_type;  // for tuple take from prev operand
      node->result_dims = node->operands[0]->result_dims;
    }
  }
  void HandleGetTupleElement(HloInstruction* inst) {
    auto node = addNode<fGetTupleElement>();
    HandleGeneric(node, inst);
  }
  void HandleConstant(HloInstruction* inst) {
    auto node = addNode<fConstant>();
    HandleGeneric(node, inst);
    node->result = inst->literal().untyped_data();
  }
  void HandleReduceWindow(HloInstruction* inst) {
    auto node = addNode<fReduceWindow>();
    HandleGeneric(node, inst);
    int num_dims = inst->window().dimensions().size();
    if (num_dims != 4) {
      LOG(INFO) << "ERROR: ReduceWindow input should be 4D";
    }
    // Assuming NHWC layout
    node->params.Ky = inst->window().dimensions()[1].size();
    node->params.Kx = inst->window().dimensions()[2].size();
    node->params.Sy = inst->window().dimensions()[1].stride();
    node->params.Sx = inst->window().dimensions()[2].stride();
    node->padding.top =
        inst->window()
            .dimensions()[1]
            .padding_high();  // FIXME: padding_high and low should be reversed
    node->padding.bottom = inst->window().dimensions()[1].padding_low();
    node->padding.left = inst->window().dimensions()[2].padding_low();
    node->padding.right = inst->window().dimensions()[2].padding_high();
  }
};
}
}
#endif
