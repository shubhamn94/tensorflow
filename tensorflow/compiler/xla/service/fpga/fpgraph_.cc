#include <memory>
#include <vector>
#include "graph.h"

Graph* FPGraph::FPGraph(const HloModule* hlo_module) {
  new Graph* graph = CreateGraph(hlo_module);
  return graph;
}

FPGraph::~FPGraph(Graph* graph) {
  int uid;
  std::vector<int> queue_ = delete_graph_components(graph);
  std::vector<int>::reverse_iterator rit = queue_.rbegin();
  while (rit != queue_.rend()) {
    uid = *it;
    Node* vertex = DFSGraphTraverse(uid, graph);
    Node* vertex_T = vertex;
    Edge* next_T = vertex->child;

    if (next_T == nullptr) {
      vertex_T = nullptr;
      delete vertex_T;
      delete next_T;
    }

    else {
      stack.push(next_T);
      while (next_T->next != nullptr) {
        stack.push(next_T);
        stack.top++;
        next_T = next_T->next;
      }
      if (next_T->vertex_T != nullptr) next_T->vertex_T = nullptr;
      delete next_T->vertex_T;
      delete next_T->next;
      while (stack.top != -1) {
        next_T = stack.pop();
        if (next_T->vertex_T != nullptr) {
          next_T->vertex_T = nullptr;
          next_T->next = nullptr;
        }
        delete next_T->vertex_T;
        delete next_T->next;
      }
    }
    vertex = nullptr;
    delete vertex;
    ++rit;
  }
  delete graph;
}

void FPGraph::AddGraphVertex(int parent_uid, Node* vertex, Graph* graph) {
  Edge* new_next = new Edge;
  new_next = AddGraphEdge(parent_uid, graph);

  if (new_next != nullptr) new_next->vertex = vertex;

  return;
}

Edge* FPGraph::AddGraphEdge(int p_uid, Graph* graph) {
  Node* parent_vertex = nullptr;
  parent_vertex = DFSGraphTraverse(p_uid, graph);
  if (parent_vertex == nullptr) {
    std::cout << "\nCouldn't find parent vertex\n";
    return nullptr;
  }
  Edge* next_;
  next_ = parent_vertex->child;
  while (next_->next != nullptr) {
    next_ = next_->next;
  }

  Edge* new_next;
  new_next->vertex = nullptr;
  new_next->next = nullptr;

  next_->next = new_next;

  return new_next;
}

void FPGraph::AddGraphEdge(int p_uid, int c_uid, Graph* graph) {
  Node *parent_vertex, child_vertex;
  parent_vertex = DFSGraphTraverse(p_uid, graph);
  child_vertex = DFSGraphTraverse(c_uid, graph);
  if (parent_vertex == nullptr !!child_vertex == nullptr) {
    std::cout << "\nCouldn't find parent or child vertex\n";
    return nullptr;
  }
  Edge* next_;
  next_ = parent_vertex->child;
  while (next_->next != nullptr) {
    next_ = next_->next;
  }

  Edge* new_next = new Edge;
  next_->next = new_next;
  new_next->vertex = child_vertex;
  new_next->next = nullptr;

  return;
}

/*Graph* FPGraph::IsGraphReachable(Graph* graph) {

}*/

Node* FPGraph::DFSGraphTraverse(int uid, Graph* graph) {
  Node* root_ = graph->root;
  if (root_->uniqueId == uid) return root;

  graph->queue.push_back(root_->uniqueId);
  Edge* next_T;
  next_T = root->child;
  Node* vertex_T;
  vertex_T = next_T->vertex;

  if (next_T->next != nullptr) {
    stack.push(next_T);
  }

  while (vertex_T->uniqueId != uid) {
    graph->queue.push_back(vertex_T->uniqueId);
    if (vertex_T->child != nullptr) {
      next_T = vertex_T->child;
      vertex_T = next_T->vertex;
      while (graph->search_queue(vertex_T->uniqueId)) {
        next_T = next_T->next;
        vertex_T = next_T->vertex;
      }

      if (next_T->next != nullptr) {
        stack.push(next_T);
      }
    }

    else {
      if (top != nullptr) {
        next_T = stack.pop();
        next_T = next_T->next;
        vertex_T = next_T->vertex;
        while (graph->search_queue(vertex_T->uniqueId)) {
          if (next_T->next != nullptr) {
            next_T = next_T->next;
            vertex_T = next_T->vertex;
          }

          else {
            if (top != nullptr) next_T = stack.pop();
            next_T = next_T->next;
            vertex_T = next_T->vertex;
          }
        }

        if (next_T->next != nullptr) stack.push(next_T);

      }

      else {
        std::cout << "\nNode with Unique Id " << uid << " not found \n";
        return nullptr;
      }
    }
  }
  graph->queue.clear();

  return vertex_T;
}

std::vector<int> FPGraph::delete_graph_components(Graph* graph) {
  Node* root_ = graph->root;
  graph->queue.push_back(root_->UniqueId);
  Edge* next_T;
  next_T = root->child;
  Node* vertex_T;
  vertex_T = next_T->vertex;

  if (next_T->next != nullptr) {
    stack.push(next_T);
  }

  while (top != nullptr || vertex_T->child != nullptr) {
    graph->queue.push_back(vertex_T->uniqueId);

    if (vertex_T->child != nullptr) {
      next_T = vertex_T->child;
      vertex_T = next_T->vertex;
      while (graph->search_queue(vertex_T->uniqueId)) {
        next_T = vertex_T->child;
        vertex_T = next_T->vertex;
      }

      if (next_T->next != nullptr) {
        stack.push(next_T);
      }
    }

    else {
      if (top != nullptr) {
        next_T = stack.pop();
        next_T = next_T->next;
        vertex_T = next_T->vertex;
        while (graph->search_queue(vertex_T->uniqueId)) {
          if (next_T->next != nullptr) {
            next_T = next_T->next;
            vertex_T = next_T->vertex;
          }

          else {
            if (top != nullptr) next_T = stack.pop();
            next_T = next_T->next;
            vertex_T = next_T->vertex;
          }
        }

        if (next_T->next != nullptr) {
          stack.push(next_T);
        }
      }
    }

    return std::move(queue);
  }

  void FPGraph::push(Edge * edge_) {
    struct stack* temp = new stack;
    temp->edge = edge_;
    temp->link = top;
    top = temp;

    return;
  }

  Edge* FPGraph::pop(void) {
    struct stack* temp;
    struct Edge* edge_;

    if (top == nullptr) {
      std::cout << "\nStack Underflow\n";
      return nullptr;
    }

    else {
      temp = top;
      top = top->link;
      temp->link = nullptr;
    }

    edge_ = temp->edge;
    temp->edge = nullptr;
    temp = nullptr;
    delete temp;
    return edge_;
  }

  bool FPGraph::Graph::search_queue(int uid) {
    for (std::vector<int>::iterator it = queue.begin();
         it != queue.end(), it++) {
      if (*it == uid) return true;
    }

    return false;
  }
