#include "graph_builder.h"
#include "tensorflow/compiler/xla/service/shaped_buffer.h"
#include "tensorflow/compiler/xla/service/buffer_assignment.h"

namespace xla {
  namespace fpga {

    class executeGraphPass : public graphVisitor {
      FILE *fp;
      fGraph_tf *graph_tf;
      float *padding(const float *input, const std::vector<int> &inp_dims,
          int padding_top, int padding_bot, int padding_left,
          int padding_right);

      void myconv2d(float *result, convParams params, const float *input,
          const float *kernel);

      public:
      executeGraphPass(fGraph_tf *);

      void run(void *result_buffer, int result_buffer_size);

      void assignParameters(absl::Span<const ShapedBuffer *const> &arguments,
          std::unique_ptr<const BufferAssignment> &assignment);
      void visitFusedConv(fFusedConv *)            ;
      void visitConvolution(fConvolution *)        ;
      void visitParameter(fParameter *)            ;
      void visitConstant(fConstant *)              ;
      void visitAdd(fAdd *)                        ;
      void visitMaximum(fMaximum *)                ;
      void visitSubtract(fSubtract *)              ;
      void visitReshape(fReshape *)                ;
      void visitBroadcast(fBroadcast *)            ;
      void visitReduceWindow(fReduceWindow *)      ;
      void visitTuple(fTuple *)                    ;
      void visitGetTupleElement(fGetTupleElement *);

    };
  }
}
