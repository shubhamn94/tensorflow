/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include "tensorflow/compiler/xla/service/fpga/ir_emitter.h"
#include "tensorflow/compiler/xla/service/fpga/fpgraph.h"

#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <algorithm>
#include <iterator>
#include <limits>
#include <memory>
#include <utility>
#include <vector>

#include "absl/container/flat_hash_map.h"
#include "absl/container/flat_hash_set.h"
#include "absl/strings/str_cat.h"
#include "absl/strings/str_format.h"
#include "absl/types/span.h"
#include "tensorflow/compiler/xla/layout_util.h"
#include "tensorflow/compiler/xla/map_util.h"
#include "tensorflow/compiler/xla/service/buffer_assignment.h"
#include "tensorflow/compiler/xla/service/elemental_ir_emitter.h"
#include "tensorflow/compiler/xla/service/hlo_casting_utils.h"
#include "tensorflow/compiler/xla/service/hlo_computation.h"
#include "tensorflow/compiler/xla/service/hlo_instruction.h"
#include "tensorflow/compiler/xla/service/hlo_instructions.h"
#include "tensorflow/compiler/xla/service/hlo_module.h"
#include "tensorflow/compiler/xla/service/hlo_opcode.h"
#include "tensorflow/compiler/xla/service/shape_inference.h"
#include "tensorflow/compiler/xla/service/tuple_points_to_analysis.h"
#include "tensorflow/compiler/xla/shape_util.h"
#include "tensorflow/compiler/xla/status_macros.h"
#include "tensorflow/compiler/xla/types.h"
#include "tensorflow/compiler/xla/util.h"
#include "tensorflow/compiler/xla/window_util.h"
#include "tensorflow/core/lib/core/bits.h"
#include "tensorflow/core/lib/core/errors.h"
#include "tensorflow/core/lib/math/math_util.h"
#include "tensorflow/core/platform/logging.h"
#include "sstream"
namespace xla {

namespace {
namespace gtl = tensorflow::gtl;
}  // namespace

namespace fpga {
void dump_graph(const HloComputation* computation) {
  static int n=0;
  std::ostringstream filename;
  filename<<"hlo_graphs/hlo_graph_dump"<<n<<".dot";
  FILE* fp = fopen(filename.str().c_str(), "w");
  fprintf(fp, "digraph G{\n");
  for (auto* instruction : computation->instructions()) {
    std::string hlo_opcode = HloOpcodeString(instruction->opcode());
    std::string inst_name = std::to_string(instruction->unique_id());
    fprintf(fp, "%s[label=\"%s(id=%d\"]; \n", inst_name.c_str(), hlo_opcode.c_str(),n);
  }
  for (auto* instruction : computation->instructions()) {
    std::string hlo_opcode = HloOpcodeString(instruction->opcode());
    std::string inst_name = std::to_string(instruction->unique_id());
    auto operandVec = instruction->operands();
    for (auto operand_inst : operandVec) {
      std::string operand_name = std::to_string(operand_inst->unique_id());
      fprintf(fp, "%s -> %s ;\n", operand_name.c_str(), inst_name.c_str());
    }
  }
  fprintf(fp, "}");
  fclose(fp);
  n++;
}

IrEmitter::IrEmitter(
    const HloModule& hlo_module) /*, const BufferAssignment& assignment)*/
    : ShapeVerifier(false, false) {
  // fp_log = fopen("ir_emitter_dump.log","a");
  CreateGraph(hlo_module);
}

IrEmitter::~IrEmitter() {
  // fprintf(fp_log,"IrEmitter destructor called!! ");
  // fclose(fp_log);
}

void IrEmitter::create_node_to_xla_op_mapping(
    const HloComputation* computation) {
  for (auto* instruction : computation->instructions()) {
    switch (instruction->opcode()) {
      case HloOpcode::kConvolution: {
        HandleConvolution(instruction);
        break;
      }
      case HloOpcode::kParameter: {
        HandleParameter(instruction);
        graph_->param_count++;
        break;
      }
      case HloOpcode::kConstant: {
        HandleConstant(instruction);
        break;
      }
      case HloOpcode::kAdd: {
        HandleElementwiseBinary(instruction);
        break;
      }
      case HloOpcode::kDivide: {
        HandleElementwiseBinary(instruction);
        break;
      }
      case HloOpcode::kMaximum: {
        HandleElementwiseBinary(instruction);
        break;
      }
      case HloOpcode::kSubtract: {
        HandleElementwiseBinary(instruction);
        break;
      }
      case HloOpcode::kReshape: {
        HandleReshape(instruction);
        break;
      }
      case HloOpcode::kBroadcast: {
        HandleBroadcast(instruction);
        break;
      }
      case HloOpcode::kReduceWindow: {
        HandleReduceWindow(instruction);
        break;
      }
      case HloOpcode::kTuple: {
        HandleTuple(instruction);
        break;
      }
      case HloOpcode::kGetTupleElement: {
        HandleGetTupleElement(instruction);
        break;
      }
      default: {
        LOG(INFO) << "ERROR: FPGA found unimplemented instruction "
                  << instruction->name();
        exit(1);
      }
    }
  }
}

void IrEmitter::addEdges_new(const HloComputation* computation) {
  // populate dictionary hlo_op -> node;
  for (auto* dst_inst : computation->instructions()) {
    for (auto src_inst : dst_inst->operands()) {
      auto parent_vertex =
          instruction_to_node_map[dst_inst->unique_id()];  // src is child(graph
                                                           // is upside down)
      auto child_vertex = instruction_to_node_map[src_inst->unique_id()];
      Edge* edge =
          AddGraphEdge(parent_vertex, child_vertex,
                       0);  // FIXME: the last field(tag) is useless. remove it.
      parent_vertex->operands.push_back(edge);
    }
  }
}

Status IrEmitter::CreateGraph(const HloModule& hlo_module) {
  graph_ = get_graph();
  std::vector<const HloComputation*> computations_;

  // Push the Entry computation first.
  computations_.push_back(hlo_module.entry_computation());

  for (auto* computation : hlo_module.MakeComputationPostOrder()) {
    if (computation == hlo_module.entry_computation()) continue;
    computations_.push_back(computation);
  }
  LOG(INFO) <<"Number of computations in HloModule" << hlo_module.name() <<"is:"<< computations_.size();

  auto* computation = hlo_module.entry_computation();
  // fprintf(fp_log,"List of instructions to be executed by FPGA\n");
  // for (auto* instruction : computation->instructions()) {
  //    fprintf(fp_log,"\t=> Found hlo opcode
  //    %s\n",HloOpcodeString(instruction->opcode()).c_str());
  //}
  // fflush(fp_log);
  dump_graph(computation);
  // create node to xla op mapping
  create_node_to_xla_op_mapping(computation);

  auto it = instruction_to_node_map.find(
      computation->root_instruction()->unique_id());
  if (it != instruction_to_node_map.end()) {
    graph_->root = it->second;
  } else {
    HloInstruction* root_instruction = computation->root_instruction();
    LOG(INFO)
        << "CRITICAL WARNING: Instruction Handle for root instruction named: "
        << root_instruction->name() << " not present ";
    graph_->root->uniqueId = root_instruction->unique_id();
    graph_->root->tag = _root_;
    graph_->root->node_type_.root_node.shape.dimensions =
        AsInt64Slice(root_instruction->shape().dimensions());
    graph_->root->node_type_.root_node.shape.layout =
        AsInt64Slice(root_instruction->shape().layout().minor_to_major());

    if (root_instruction->operand_count() != 1)
      LOG(INFO) << "CRITICAL WARNING: Root Instruction has more than 1 "
                   "operand!! Adding shape of only operand0 to the graph";

    graph_->root->node_type_.root_node.operand_shape.dimensions =
        AsInt64Slice(root_instruction->operand(0)->shape().dimensions());
    graph_->root->node_type_.root_node.operand_shape.layout = AsInt64Slice(
        root_instruction->operand(0)->shape().layout().minor_to_major());
    instruction_to_node_map[root_instruction->unique_id()] = graph_->root;
  }

  // addEdges_old(computation);
  addEdges_new(computation);

  // LOG(INFO) <<"Running a check where graph is created successfully and fully
  // connected";
  // for(auto* instruction_:computation->instructions()) {
  //    if(instruction_->unique_id() == 162 || instruction_->unique_id() == 305)
  //        continue;
  //    LOG(INFO)<< "Finding vertex for instruction:"<<
  //    instruction_->unique_id();
  //    Node* Vertex = DFSGraphTraverse(instruction_->unique_id(), get_graph());
  //    if(Vertex != nullptr)
  //        LOG(INFO) << "Vertex Found!! It's tag is: "<< Vertex->tag;
  //    else
  //        LOG(INFO) <<"Instruction with unique_id
  //        "<<instruction_->unique_id()<< " not found in created graph";
  //}

  // fprintf(fp_log,"Exiting irEmitter::Create graph\n");
  return Status::OK();
}

Status IrEmitter::ElementTypesSameAndSupported(
    const HloInstruction& instruction,
    absl::Span<const HloInstruction* const> operands,
    absl::Span<const PrimitiveType> supported_types) {
  for (auto operand : operands) {
    TF_RET_CHECK(
        ShapeUtil::SameElementType(operands[0]->shape(), operand->shape()));
  }
  TF_RET_CHECK(!operands.empty());
  PrimitiveType primitive_type = operands[0]->shape().element_type();
  if (std::find(supported_types.begin(), supported_types.end(),
                primitive_type) == supported_types.end()) {
    return Unimplemented("unsupported operand type %s in op %s",
                         PrimitiveType_Name(primitive_type),
                         HloOpcodeString(instruction.opcode()));
  }
  return Status::OK();
}

Status IrEmitter::HandleConvolution(HloInstruction* convolution) {
  Node* vertex = new Node;
  vertex->uniqueId = convolution->unique_id();
  vertex->tag = kconv;
  auto lhs = convolution->operand(0);
  auto rhs = convolution->operand(1);
  TF_RETURN_IF_ERROR(ElementTypesSameAndSupported(
      /*instruction=*/*convolution, /*operands=*/{lhs, rhs},
      /*supported_types=*/{F16, F32, C64}));

  // Shape of operands

  // Dimensions of operands
  vertex->node_type_.c.lhs_shape.dimensions =
      AsInt64Slice(lhs->shape().dimensions());
  vertex->node_type_.c.rhs_shape.dimensions =
      AsInt64Slice(rhs->shape().dimensions());

  // Layout of operands
  vertex->node_type_.c.lhs_shape.layout =
      AsInt64Slice(lhs->shape().layout().minor_to_major());
  vertex->node_type_.c.rhs_shape.layout =
      AsInt64Slice(rhs->shape().layout().minor_to_major());

  // PrimitiveType of Input and Kernel
  vertex->node_type_.c.lhs_shape.element_type = lhs->shape().element_type();
  vertex->node_type_.c.rhs_shape.element_type = rhs->shape().element_type();

  // PrimitiveType of Convolution
  vertex->node_type_.c.conv_shape.element_type =
      convolution->shape().element_type();

  const Shape& convolution_shape = convolution->shape();
  // The input, kernel and output agree with respect to layout.
  if (LayoutUtil::IsMonotonicWithDim0Major(lhs->shape().layout()) &&
      LayoutUtil::IsMonotonicWithDim0Major(rhs->shape().layout()) &&
      LayoutUtil::IsMonotonicWithDim0Major(convolution_shape.layout())) {
    bool one_dim_convolution = lhs->shape().dimensions_size() == 3;
    // void* lhs_address = GetEmittedValueFor(lhs);
    // void* rhs_address = GetEmittedValueFor(rhs);
    // TF_RETURN_IF_ERROR(EmitTargetAddressForOp(convolution));

    // Dimension and Layout of Convolution
    vertex->node_type_.c.conv_shape.dimensions =
        AsInt64Slice(convolution->shape().dimensions());
    vertex->node_type_.c.conv_shape.layout =
        AsInt64Slice(convolution->shape().layout().minor_to_major());

    const ConvolutionDimensionNumbers& dnums =
        convolution->convolution_dimension_numbers();

    // Input tensor
    const Shape& input_shape = convolution->operand(0)->shape();

    vertex->node_type_.c.input_batch =
        input_shape.dimensions(dnums.input_batch_dimension());
    vertex->node_type_.c.input_rows =
        input_shape.dimensions(dnums.input_spatial_dimensions(0));
    vertex->node_type_.c.input_cols =
        one_dim_convolution
            ? 1
            : input_shape.dimensions(dnums.input_spatial_dimensions(1));
    vertex->node_type_.c.input_channels =
        input_shape.dimensions(dnums.input_feature_dimension());

    // Kernel tensor
    const Shape& kernel_shape = convolution->operand(1)->shape();
    vertex->node_type_.c.kernel_rows =
        kernel_shape.dimensions(dnums.kernel_spatial_dimensions(0));
    vertex->node_type_.c.kernel_cols =
        one_dim_convolution
            ? 1
            : kernel_shape.dimensions(dnums.kernel_spatial_dimensions(1));
    vertex->node_type_.c.kernel_channels =
        kernel_shape.dimensions(dnums.kernel_input_feature_dimension());
    vertex->node_type_.c.kernel_filters =
        kernel_shape.dimensions(dnums.kernel_output_feature_dimension());

    // Output tensor
    const Shape& convolution_shape = convolution->shape();
    vertex->node_type_.c.output_rows =
        convolution_shape.dimensions(dnums.output_spatial_dimensions(0));
    vertex->node_type_.c.output_cols =
        one_dim_convolution
            ? 1
            : convolution_shape.dimensions(dnums.output_spatial_dimensions(1));

    // Extract the window stride for the convolution
    const Window& window = convolution->window();
    vertex->node_type_.c.row_stride = window.dimensions(0).stride();
    vertex->node_type_.c.col_stride =
        one_dim_convolution ? 1 : window.dimensions(1).stride();

    vertex->node_type_.c.padding_top = window.dimensions(0).padding_low();
    vertex->node_type_.c.padding_bottom = window.dimensions(0).padding_high();
    vertex->node_type_.c.padding_left =
        one_dim_convolution ? 0 : window.dimensions(1).padding_low();
    vertex->node_type_.c.padding_right =
        one_dim_convolution ? 0 : window.dimensions(1).padding_high();

    vertex->node_type_.c.lhs_row_dilation =
        window.dimensions(0).base_dilation();
    vertex->node_type_.c.lhs_col_dilation =
        one_dim_convolution ? 1 : window.dimensions(1).base_dilation();
    vertex->node_type_.c.rhs_row_dilation =
        window.dimensions(0).window_dilation();
    vertex->node_type_.c.rhs_col_dilation =
        one_dim_convolution ? 1 : window.dimensions(1).window_dilation();

    instruction_to_node_map[convolution->unique_id()] = vertex;
  }

  return Status::OK();
}

Status IrEmitter::HandleConstant(HloInstruction* constant) {
  Node* vertex = new Node;
  vertex->uniqueId = constant->unique_id();
  vertex->tag = kcons;

  const Literal& literal_ = constant->literal();
  vertex->node_type_.cons.pointer_type = literal_.shape().element_type();
  vertex->node_type_.cons.ptr = literal_.untyped_data();
  vertex->node_type_.cons.size = literal_.size_bytes();
  /*switch (literal_.shape().element_type()) {
    case PRED:
    absl::Span<const bool> data_vector = literal_.data<bool>();
    memcpy(vertex->node_type_->cons->data_vector.data(), data_vector.data(),
    data_vector.size());
    break;
    case S8:
    vertex->node_type_.cons.ptr = literal_.untyped_data();
    vertex->node_type_.cons.pointer_type = literal_.shape().element_type();

    break;
    case U8:
    absl::Span<const uint8> data_vector = literal_.data<uint8>();
    break;
    case S16:
    absl::Span<const int16> data_vector = literal_.data<int16>();
    break;
    case U16:
    absl::Span<const uint16> data_vector = literal_.data<uint16>();
    break;
    case S32:
    absl::Span<const int32> data_vector = literal_.data<int32>();
    break;
    case U32:
    absl::Span<const uint32> data_vector = literal_.data<uint32>();
    break;
    case S64:
    absl::Span<const int64> data_vector = literal_.data<int64>();
    break;
    case U64:
    absl::Span<const uint64> data_vector = literal_.data<uint64>();
    break;
    case F32:
    absl::Span<const float> data_vector = literal_.data<float>();
    break;
    case F64:
    absl::Span<const double> data_vector = literal_.data<double>();
    break;
    case C64:
    absl::Span<const complex64> data_vector = literal_.data<complex64>();
    break;
    case F16:
    absl::Span<const half> data_vector = literal_.data<half>();
    break;
    case BF16:
    absl::Span<const bfloat16> data_vector = literal_.data<bfloat16>();
    break;
    default:
    LOG(FATAL) << "Element type not valid for array: "
    << PrimitiveType_Name(subshape().element_type());
    }*/

  vertex->node_type_.cons.element_count =
      ShapeUtil::ElementsInRecursive(literal_.shape());

  // Dimension and Layout of literal
  vertex->node_type_.cons.dimensions =
      AsInt64Slice(literal_.shape().dimensions());
  vertex->node_type_.cons.minor_to_major =
      AsInt64Slice(literal_.shape().layout().minor_to_major());

  instruction_to_node_map[constant->unique_id()] = vertex;
  Graph* graph_ = GetGraph();
  graph_->constant_to_node_map[constant->unique_id()] = vertex;

  return Status::OK();
}

Status IrEmitter::HandleParameter(HloInstruction* param) {
  Node* vertex = new Node;
  vertex->uniqueId = param->unique_id();
  vertex->tag = _param_;
  vertex->node_type_.p.param_number = param->parameter_number();

  // Shape of the parameter
  vertex->node_type_.p.param_shape.dimensions =
      AsInt64Slice(param->shape().dimensions());
  vertex->node_type_.p.param_shape.layout =
      AsInt64Slice(param->shape().layout().minor_to_major());

  // Hard coding input recognition in backend. This is temporary solution for
  // VGGNET and won't work for a new Neural net.
  // uint32 input_shape[] = {1, 224, 224, 3};
  // uint32 flag = 0;
  // LOG(INFO) << "Input shape is: ";
  // for(int i = 0; i < 4; i++) {
  //    if(input_shape[i] != param->shape().dimensions(i))
  //        flag = 1;
  //}

  // if(flag == 0)
  //    graph_->input_node = vertex;

  instruction_to_node_map[param->unique_id()] = vertex;
  Graph* graph_ = GetGraph();
  graph_->parameter_to_node_map[param->parameter_number()] = vertex;

  return Status::OK();
}

Status IrEmitter::HandleDot(HloInstruction* dot) {
  Node* vertex = new Node;
  vertex->uniqueId = dot->unique_id();
  vertex->tag = _dot_;
  auto lhs = dot->operand(0);
  auto rhs = dot->operand(1);
  TF_RETURN_IF_ERROR(ElementTypesSameAndSupported(
      /*instruction=*/*dot, /*operands=*/{lhs, rhs},
      /*supported_types=*/{F16, F32, C64}));

  // Shape of operand

  // Dimensions of operands
  vertex->node_type_.d.lhs_shape.dimensions =
      AsInt64Slice(lhs->shape().dimensions());
  vertex->node_type_.d.rhs_shape.dimensions =
      AsInt64Slice(rhs->shape().dimensions());

  // Layout of operands
  vertex->node_type_.d.lhs_shape.layout =
      AsInt64Slice(lhs->shape().layout().minor_to_major());
  vertex->node_type_.d.rhs_shape.layout =
      AsInt64Slice(rhs->shape().layout().minor_to_major());

  // Dimensions of Dot instruction

  vertex->node_type_.d.lhs_contr_dim =
      AsInt64Slice(dot->dot_dimension_numbers().lhs_contracting_dimensions());
  vertex->node_type_.d.rhs_contr_dim =
      AsInt64Slice(dot->dot_dimension_numbers().rhs_contracting_dimensions());
  vertex->node_type_.d.lhs_batch_dim =
      AsInt64Slice(dot->dot_dimension_numbers().lhs_batch_dimensions());
  vertex->node_type_.d.rhs_batch_dim =
      AsInt64Slice(dot->dot_dimension_numbers().rhs_batch_dimensions());

  CHECK_EQ(dot->dot_dimension_numbers().lhs_batch_dimensions_size(),
           dot->dot_dimension_numbers().rhs_batch_dimensions_size());

  // Dimension and Layout of Dot instruction
  vertex->node_type_.d.dot_shape.dimensions =
      AsInt64Slice(dot->shape().dimensions());
  vertex->node_type_.d.dot_shape.layout =
      AsInt64Slice(dot->shape().layout().minor_to_major());

  instruction_to_node_map[dot->unique_id()] = vertex;

  return Status::OK();
}

Status IrEmitter::HandleElementwiseBinary(HloInstruction* hlo) {
  Node* vertex = new Node;
  vertex->uniqueId = hlo->unique_id();

  switch (hlo->opcode()) {
    case HloOpcode::kMaximum:
      vertex->tag = _maximum_;
      break;
    case HloOpcode::kAdd:
      vertex->tag = add;
      break;
    case HloOpcode::kSubtract:
      vertex->tag = subtract;
      break;
    case HloOpcode::kDivide:
      vertex->tag = divide;
      break;
    default:
      LOG(ERROR) << "HloOpcode not defined!!";
  }

  auto lhs = hlo->operand(0);
  auto rhs = hlo->operand(1);

  // Dimension of operands
  vertex->node_type_.bin.lhs_shape.dimensions =
      AsInt64Slice(lhs->shape().dimensions());
  vertex->node_type_.bin.rhs_shape.dimensions =
      AsInt64Slice(rhs->shape().dimensions());

  // Layout of operands
  vertex->node_type_.bin.lhs_shape.layout =
      AsInt64Slice(lhs->shape().layout().minor_to_major());
  vertex->node_type_.bin.rhs_shape.layout =
      AsInt64Slice(rhs->shape().layout().minor_to_major());

  // Dimension and layout of Binary instruction
  vertex->node_type_.bin.bin_shape.dimensions =
      AsInt64Slice(hlo->shape().dimensions());
  vertex->node_type_.bin.bin_shape.layout =
      AsInt64Slice(hlo->shape().layout().minor_to_major());

  const Shape& out_shape = hlo->shape();
  for (HloInstruction* operand : hlo->operands()) {
    const Shape& operand_shape = operand->shape();
    if (!ShapeUtil::CompatibleIgnoringElementType(operand_shape, out_shape)) {
      return FailedPrecondition(
          "Implicit broadcast is not allowed in HLO."
          "Found different shapes for instruction %s.\n"
          "output: %s\noperand: %s\n",
          HloOpcodeString(hlo->opcode()), ShapeUtil::HumanString(out_shape),
          ShapeUtil::HumanString(operand_shape));
    }
  }

  instruction_to_node_map[hlo->unique_id()] = vertex;

  return Status::OK();
}

Status IrEmitter::HandleAdd(HloInstruction* add) {
  return HandleElementwiseBinary(add);
}

Status IrEmitter::HandleSubtract(HloInstruction* subtract) {
  return HandleElementwiseBinary(subtract);
}

Status IrEmitter::HandleDivide(HloInstruction* divide) {
  return HandleElementwiseBinary(divide);
}

Status IrEmitter::HandleMaximum(HloInstruction* maximum) {
  return HandleElementwiseBinary(maximum);
}

Status IrEmitter::HandleBroadcast(HloInstruction* broadcast) {
  Node* vertex = new Node;
  vertex->uniqueId = broadcast->unique_id();
  vertex->tag = _broadcast_;

  // Shape to be broadcasted to
  vertex->node_type_.broadcast.broadcast_shape.dimensions =
      AsInt64Slice(broadcast->shape().dimensions());
  vertex->node_type_.broadcast.broadcast_shape.layout =
      AsInt64Slice(broadcast->shape().layout().minor_to_major());

  // Current shape of the operand
  vertex->node_type_.broadcast.operand_shape.dimensions =
      AsInt64Slice(broadcast->operand(0)->shape().dimensions());
  vertex->node_type_.broadcast.operand_shape.layout =
      AsInt64Slice(broadcast->operand(0)->shape().layout().minor_to_major());

  const HloBroadcastInstruction* broadcast_ =
      static_cast<const HloBroadcastInstruction*>(broadcast);
  const std::vector<int64>& dimensions_ = broadcast->dimensions();
  vertex->node_type_.broadcast.dimensions = dimensions_;

  TF_RET_CHECK(broadcast->dimensions().size() ==
               ShapeUtil::Rank(broadcast->operand(0)->shape()))
      << "Broadcast HLO (" << broadcast->ToShortString()
      << ") has invalid number of dimensions: "
      << broadcast->dimensions().size()
      << " != " << ShapeUtil::Rank(broadcast->operand(0)->shape());

  instruction_to_node_map[broadcast->unique_id()] = vertex;

  return Status::OK();
}

Status IrEmitter::HandleReshape(HloInstruction* reshape) {
  Node* vertex = new Node;
  vertex->uniqueId = reshape->unique_id();
  vertex->tag = _reshape_;

  vertex->node_type_.reshape.reshape_shape.dimensions =
      AsInt64Slice(reshape->shape().dimensions());
  vertex->node_type_.reshape.reshape_shape.layout =
      AsInt64Slice(reshape->shape().layout().minor_to_major());

  TF_RET_CHECK(ShapeUtil::ElementsInRecursive(reshape->operand(0)->shape()) ==
               ShapeUtil::ElementsInRecursive(reshape->shape()));

  vertex->node_type_.reshape.operand_shape.dimensions =
      AsInt64Slice(reshape->operand(0)->shape().dimensions());
  vertex->node_type_.reshape.operand_shape.layout =
      AsInt64Slice(reshape->operand(0)->shape().layout().minor_to_major());

  instruction_to_node_map[reshape->unique_id()] = vertex;

  return Status::OK();
}

Status IrEmitter::HandleTuple(HloInstruction* Tuple) {
  Node* vertex = new Node;
  vertex->uniqueId = Tuple->unique_id();
  vertex->tag = _tuple_;

  Operand_Shape temp_shape;
  for (const auto operand_ : Tuple->operands()) {
    temp_shape.dimensions = AsInt64Slice(operand_->shape().dimensions());
    temp_shape.layout =
        AsInt64Slice(operand_->shape().layout().minor_to_major());
    temp_shape.element_type = operand_->shape().element_type();
    vertex->node_type_.tuple_t.operand_shape.push_back(temp_shape);
  }
  instruction_to_node_map[Tuple->unique_id()] = vertex;

  return Status::OK();
}

Status IrEmitter::HandleGetTupleElement(HloInstruction* get_tuple_element) {
  Node* vertex = new Node;
  vertex->uniqueId = get_tuple_element->unique_id();
  vertex->tag = get_tuple_ele;

  TF_RETURN_IF_ERROR(
      CheckShape(get_tuple_element, ShapeInference::InferGetTupleElementShape(
                                        get_tuple_element->operand(0)->shape(),
                                        get_tuple_element->tuple_index())));
  vertex->node_type_.get_tuple_elem.index = get_tuple_element->tuple_index();
  vertex->node_type_.get_tuple_elem.element_shape.dimensions =
      AsInt64Slice(get_tuple_element->shape().dimensions());
  vertex->node_type_.get_tuple_elem.element_shape.layout =
      AsInt64Slice(get_tuple_element->shape().layout().minor_to_major());
  vertex->node_type_.get_tuple_elem.element_shape.element_type =
      get_tuple_element->shape().element_type();

  instruction_to_node_map[get_tuple_element->unique_id()] = vertex;

  return Status::OK();
}

Status IrEmitter::DefaultAction(HloInstruction* hlo) {
  Node* vertex = new Node;
  vertex->uniqueId = hlo->unique_id();
  vertex->tag = _default_;

  vertex->node_type_.unimplemented.unimplemented_shape.dimensions =
      AsInt64Slice(hlo->shape().dimensions());
  vertex->node_type_.unimplemented.unimplemented_shape.layout =
      AsInt64Slice(hlo->shape().layout().minor_to_major());

  vertex->node_type_.unimplemented.operand_count = hlo->operand_count();
  Operand_Shape temp_operand;
  for (auto operand_ : hlo->operands()) {
    temp_operand.dimensions = AsInt64Slice(operand_->shape().dimensions());
    temp_operand.layout =
        AsInt64Slice(operand_->shape().layout().minor_to_major());
    temp_operand.element_type = operand_->shape().element_type();

    vertex->node_type_.unimplemented.operand_shape.push_back(temp_operand);
  }
  vertex->node_type_.unimplemented.opcode = HloOpcodeString(hlo->opcode());

  instruction_to_node_map[hlo->unique_id()] = vertex;

  return Status::OK();
}

/*Status IrEmitter::HandleConvert(HloInstruction* convert) {

  }

  Status IrEmitter::HandleTranspose(HloInstruction* transpose) {

  }

  Status IrEmitter::HandleCopy(HloInstruction* copy) {

  }*/

Status IrEmitter::HandleReduceWindow(HloInstruction* reduce_window) {
  Node* vertex = new Node;
  vertex->uniqueId = reduce_window->unique_id();
  vertex->tag = reduce_window_;

  auto lhs = reduce_window->operand(0);
  auto rhs = reduce_window->operand(1);

  // Dimension of operands
  vertex->node_type_.reducewindow.lhs_shape.dimensions =
      AsInt64Slice(lhs->shape().dimensions());
  vertex->node_type_.reducewindow.rhs_shape.dimensions =
      AsInt64Slice(rhs->shape().dimensions());

  // Layout of operands
  vertex->node_type_.reducewindow.lhs_shape.layout =
      AsInt64Slice(lhs->shape().layout().minor_to_major());
  vertex->node_type_.reducewindow.rhs_shape.layout =
      AsInt64Slice(rhs->shape().layout().minor_to_major());

  // Dimension and layout of Reduce-Window instruction
  vertex->node_type_.reducewindow.reducewindow_shape.dimensions =
      AsInt64Slice(reduce_window->shape().dimensions());
  vertex->node_type_.reducewindow.reducewindow_shape.layout =
      AsInt64Slice(reduce_window->shape().layout().minor_to_major());

  for (const auto& dimension : reduce_window->window().dimensions()) {
    int temp = static_cast<int>(dimension.size());
    vertex->node_type_.reducewindow.size.push_back(temp);
    temp = static_cast<int>(dimension.stride());
    vertex->node_type_.reducewindow.stride.push_back(temp);
    temp = static_cast<int>(dimension.padding_low());
    vertex->node_type_.reducewindow.padding_low.push_back(temp);
    temp = static_cast<int>(dimension.padding_high());
    vertex->node_type_.reducewindow.padding_high.push_back(temp);
  }

  TF_RETURN_IF_ERROR(CheckShape(
      reduce_window,
      ShapeInference::InferReduceWindowShape(
          reduce_window->operand(0)->shape(),
          reduce_window->operand(1)->shape(), reduce_window->window(),
          reduce_window->to_apply()->ComputeProgramShape())));

  HloComputation* subcomputation = reduce_window->to_apply();
  if (subcomputation != nullptr) {
    HloInstruction* instruction_ = subcomputation->root_instruction();
    switch (instruction_->opcode()) {
      case HloOpcode::kMaximum:
        vertex->node_type_.reducewindow.subcomputation_opcode = _maximum_;
        break;

      case HloOpcode::kAdd:
        vertex->node_type_.reducewindow.subcomputation_opcode = add;
        break;

      case HloOpcode::kSubtract:
        vertex->node_type_.reducewindow.subcomputation_opcode = subtract;
        break;

      default:
        LOG(ERROR) << "Opcode for Reduce Window subcomputation not supported ";
    }

    Node* sub_root_node_ = new Node;
    sub_root_node_->uniqueId = instruction_->unique_id();
    sub_root_node_->tag = _root_;
    sub_root_node_->node_type_.root_node.shape.dimensions =
        AsInt64Slice(instruction_->shape().dimensions());
    sub_root_node_->node_type_.root_node.shape.layout =
        AsInt64Slice(instruction_->shape().layout().minor_to_major());
    vertex->node_type_.reducewindow.sub_root_node = sub_root_node_;

    subcomp_reducewind_instruction_to_node_map[instruction_->unique_id()] =
        vertex->node_type_.reducewindow.sub_root_node;

    for (auto instruction : subcomputation->instructions()) {
      if (instruction->unique_id() == instruction_->unique_id()) continue;
      switch (instruction->opcode()) {
        case HloOpcode::kParameter: {
          Node* vertice = new Node;
          vertice->uniqueId = instruction->unique_id();
          vertice->tag = _param_;
          vertice->node_type_.p.param_number = instruction->parameter_number();
          vertice->node_type_.p.param_shape.dimensions =
              AsInt64Slice(instruction->shape().dimensions());
          vertice->node_type_.p.param_shape.layout =
              AsInt64Slice(instruction->shape().layout().minor_to_major());

          subcomp_reducewind_instruction_to_node_map[instruction->unique_id()] =
              vertice;
          break;
        }

        case HloOpcode::kConstant: {
          Node* vertice = new Node;
          vertice->uniqueId = instruction->unique_id();
          vertice->tag = kcons;
          const Literal& literal = instruction->literal();
          vertice->node_type_.cons.pointer_type =
              literal.shape().element_type();
          vertice->node_type_.cons.ptr = literal.untyped_data();
          vertice->node_type_.cons.size = literal.size_bytes();
          vertice->node_type_.cons.element_count =
              ShapeUtil::ElementsInRecursive(literal.shape());

          // Dimension and Layout of literal
          vertice->node_type_.cons.dimensions =
              AsInt64Slice(literal.shape().dimensions());
          vertice->node_type_.cons.minor_to_major =
              AsInt64Slice(literal.shape().layout().minor_to_major());

          subcomp_reducewind_instruction_to_node_map[instruction->unique_id()] =
              vertice;
          break;
        }

        case HloOpcode::kTuple: {
          Node* vertice = new Node;
          vertice->uniqueId = instruction->unique_id();
          vertice->tag = _tuple_;
          Operand_Shape temp_shape;

          for (const auto operand_ : instruction->operands()) {
            temp_shape.dimensions =
                AsInt64Slice(operand_->shape().dimensions());
            temp_shape.layout =
                AsInt64Slice(operand_->shape().layout().minor_to_major());
            temp_shape.element_type = operand_->shape().element_type();
            vertice->node_type_.tuple_t.operand_shape.push_back(temp_shape);
          }

          subcomp_reducewind_instruction_to_node_map[instruction->unique_id()] =
              vertice;
          break;
        }

        default: {
          LOG(INFO) << "ERROR: Unknown HloOpcode";
          exit(1);
        }
      }
    }
  }

  instruction_to_node_map[reduce_window->unique_id()] = vertex;

  return Status::OK();
}
}  // namespace fpga
}  // namespace xla
