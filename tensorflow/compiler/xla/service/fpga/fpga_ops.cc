#include "fpga_ops.h"
#include <sys/time.h>
#include <chrono>
#include <cstring>
#include <thread>

namespace xla{
  namespace fpga{
int foo(){return 1;}

int max_num_tiles;
int min_num_tiles;
int ___NUM_PE_PER_CORE = 16;
int min(int a, int b) { return (a < b) ? a : b; }
void fused_convf(send_fifo &toCore, Tile inTile,
                 const convParamsType conv_params,
                 const maxpoolParamsType maxpool_params,
                 fpga_tensor<const float> &inpTensor,
                 fpga_tensor<const float> &biasTensor,
                 fpga_tensor<const float> &wtTensor) {
  myassert(inTile.tx == conv_params.Ix, "tx!=Ix");
  myassert(inTile.ty == conv_params.Iy, "ty!=Iy");
  // std::cout<< "Input tile for fpga conv\n----------------------------\n";
  // print(inTile);

  toCore.push_cmd(conv_params, maxpool_params,
                  ___NUM_PE_PER_CORE);  // Send command

  for (int c = 0; c < conv_params.Iz; c++) {
    for (int i = 0; i < conv_params.Iy; i++) {
      for (int j = 0; j < conv_params.Ix; j++) {
        toCore.push_ifmap(inpTensor(0, i + inTile.y, j + inTile.x, c));
      }
    }

    for (int i = 0; i < conv_params.Ky; i++) {
      for (int j = 0; j < conv_params.Kx; j++) {
        for (int k = 0; k < conv_params.Oz; k++) {
          toCore.push_kernel(wtTensor(i, j, c, k));
        }
      }
    }
  }

  for (int k = 0; k < conv_params.Oz; k++) {
    toCore.push_bias(biasTensor(0, 0, 0, k));
  }

  toCore.flush_pending();  // To pad any remaining bias into 128bits and flush
}

void create_output_tensor(fpga_tensor<float> &outTensor, recv_fifo &recvQueue,
                          std::vector<Tile> &outMaxpoolTiles,
                          maxpoolParamsType maxpool_params) {
  for (auto outTile : outMaxpoolTiles) {
    for (int c = 0; c < maxpool_params.Iz; c++) {
      for (int i = 0; i < outTile.ty; i++) {
        for (int j = 0; j < outTile.tx; j++) {
          float val = recvQueue.pop(4);
          outTensor(0, i + outTile.y, j + outTile.x, c) = val;
        }
      }
    }
  }
}

std::vector<Tile> fused_conv(send_fifo &toCore,
                             const convParamsType conv_params,
                             const maxpoolParamsType maxpool_params,
                             fpga_tensor<const float> &inpTensor,
                             fpga_tensor<const float> &biasTensor,
                             fpga_tensor<const float> &wtTensor) {
  myassert(inpTensor.N == 1, "Do not support batch size>1\n");
  Tile t;
  t.x = 0;
  t.y = 0;
  t.tx = maxpool_params.Ox;
  t.ty = maxpool_params.Oy;

  std::cout << "Original Output:\n----------------\n";
  std::cout << "output.x  =" << t.x << std::endl;
  std::cout << "output.y  =" << t.y << std::endl;
  std::cout << "output.tx =" << t.tx << std::endl;
  std::cout << "output.ty =" << t.ty << std::endl;
  std::cout << std::endl;

  auto outMaxpoolTiles =
      findTiles(t, conv_params, maxpool_params, ___NUM_PE_PER_CORE);

  // std::cout << "After Tiling:\n----------------\n";
  // for(auto tile:outMaxpoolTiles){
  //    print(tile);
  //}

  auto outConvTiles =
      calc_input_tiles(outMaxpoolTiles, maxpool_params.Kx, maxpool_params.Ky,
                       maxpool_params.Sx, maxpool_params.Sy);
  auto inTiles = calc_input_tiles(outConvTiles, conv_params.Kx, conv_params.Ky,
                                  conv_params.Sx, conv_params.Sy);
  int num_tiles_processed = 0;
  for (auto inTile : inTiles) {
    num_tiles_processed++;
    if (num_tiles_processed > max_num_tiles) break;
    if (num_tiles_processed < min_num_tiles) continue;

    auto tile_conv_params = conv_params;
    auto tile_maxpool_params = maxpool_params;

    tile_conv_params.Ix = inTile.tx;
    tile_conv_params.Iy = inTile.ty;
    tile_maxpool_params.Ix = calc_output_dim(
        tile_conv_params.Ix, tile_conv_params.Kx, tile_conv_params.Sx);
    tile_maxpool_params.Iy = calc_output_dim(
        tile_conv_params.Iy, tile_conv_params.Ky, tile_conv_params.Sy);
    tile_maxpool_params.Ox = calc_output_dim(
        tile_maxpool_params.Ix, tile_maxpool_params.Kx, tile_maxpool_params.Sx);
    tile_maxpool_params.Oy = calc_output_dim(
        tile_maxpool_params.Iy, tile_maxpool_params.Ky, tile_maxpool_params.Sy);

    fused_convf(toCore, inTile, tile_conv_params, tile_maxpool_params,
                inpTensor, biasTensor, wtTensor);
  }
  return outMaxpoolTiles;
}

int calc_num_outputs(std::vector<Tile> outMaxpoolTiles,
                     maxpoolParamsType maxpool_params) {
  int num_outputs = 0;
  int num_tiles_processed = 0;
  for (auto tile : outMaxpoolTiles) {
    num_tiles_processed++;
    if (num_tiles_processed > max_num_tiles) break;
    if (num_tiles_processed < min_num_tiles) continue;
    num_outputs += tile.tx * tile.ty * maxpool_params.Iz;
    // std::cout << "tile.tx=            " << tile.tx           << std::endl ;
    // std::cout << "tile.ty           = " << tile.ty           << std::endl ;
    // std::cout << "maxpool_params.Iz = " << maxpool_params.Iz << std::endl ;
    // std::cout << "num_outputs       = " <<num_outputs        << std::endl ;
  }
  return num_outputs;
}

void fpga_conv(fpga_tensor<float> &outTensor, const convParamsType conv_params,
               const maxpoolParamsType maxpool_params,
               fpga_tensor<const float> &inpTensor,
               fpga_tensor<const float> &biasTensor,
               fpga_tensor<const float> &wtTensor, int _NUM_PE_PER_CORE) {
  ___NUM_PE_PER_CORE = _NUM_PE_PER_CORE;
  std::cout << "\n\n..........................................................."
               "....................\n";
  std::cout << "........................DEBUG "
               "MESSAGES.........................................\n";
  std::cout << "..............................................................."
               "................\n\n";
  riffa_stream fpga;
  send_fifo sendQueue;

  sendQueue.push_reset();

  max_num_tiles = 10000;
  min_num_tiles = 0;
  std::vector<Tile> outMaxpoolTiles = fused_conv(
      sendQueue, conv_params, maxpool_params, inpTensor, biasTensor, wtTensor);
  int output_size = calc_num_outputs(outMaxpoolTiles, maxpool_params);
  if (output_size !=
      maxpool_params.Ox * maxpool_params.Oy * maxpool_params.Iz) {
    std::cout << "sum of tile output sizes=" << output_size;
    std::cout << "expected output sizes="
              << maxpool_params.Ox * maxpool_params.Oy * maxpool_params.Iz;
    // myassert(false,"output_size mismatch\n");
  }
  std::cout << "Number of Maxpool Tiles = " << outMaxpoolTiles.size()
            << std::endl;
  // for(auto maxpoolTile:outMaxpoolTiles){
  //    print(maxpoolTile);
  //}
  // std::cout<<"\n";

  std::cout << "output_size = " << output_size << std::endl;
  recv_fifo recvQueue(4 * output_size);
  auto f = [&fpga, &recvQueue]() mutable {
    fpga >> recvQueue;
    // std::cout <<"recvQueue[0] =
    // "<<*(float*)&recvQueue.recvVector[0]<<std::endl;
  };
  std::thread tReader(f);
  struct timeval _start, _stop;
  gettimeofday(&_start, NULL);
  std::cout << "Start execution\n";
  auto start = std::chrono::steady_clock::now();
  fpga << sendQueue;
  tReader.join();
  auto end = std::chrono::steady_clock::now();
  gettimeofday(&_stop, NULL);
  printf("Took %lu microseconds\n", _stop.tv_usec - _start.tv_usec);
  // recvQueue.print();

  std::cout << "\n\n..........................................................."
               "....................\n";
  std::cout << "..............................................................."
               "................\n\n";
  // std::cout << "Time taken =
  // "<<std::chrono::duration_cast<std::chrono::microseconds>(end -
  // start).count() << " µs" << std::endl;
  std::cout << "Time taken = "
            << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                     start)
                   .count()
            << " ms" << std::endl;
  printf("First 4 elems of recvQueue=(%f,%f,%f,%f)\n",
         *(float *)&recvQueue.recvVector[0], *(float *)&recvQueue.recvVector[4],
         *(float *)&recvQueue.recvVector[8],
         *(float *)&recvQueue.recvVector[12]);
  printf(
      "Second 4 elems of recvQueue=(%f,%f,%f,%f)\n",
      *(float *)&recvQueue.recvVector[16], *(float *)&recvQueue.recvVector[20],
      *(float *)&recvQueue.recvVector[24], *(float *)&recvQueue.recvVector[28]);
  // sendQueue.binarydump("test.in");

  create_output_tensor(outTensor, recvQueue, outMaxpoolTiles, maxpool_params);
}
}
}
