#include "fpgraph.h"
#include <iostream>
#include <vector>
#include "tensorflow/compiler/xla/service/hlo_module.h"
#include "tensorflow/core/platform/logging.h"

// Graph* FPGraph::FPGraph(const HloModule* hlo_module) {

namespace xla {
namespace fpga {

FPGraph::~FPGraph() {}

void FPGraph::AddGraphVertex(int parent_uid, Node* vertex, Graph* graph) {
  Edge* new_next = new Edge;
  Edge* child_edge = new Edge;
  Node* parent_vertex;

  child_edge->vertex = vertex;
  child_edge->next = nullptr;
  parent_vertex = DFSGraphTraverse(parent_uid, graph);

  if (parent_vertex != nullptr && parent_vertex->child == nullptr)
    parent_vertex->child = child_edge;

  else {
    new_next = AddGraphEdge(parent_uid, graph);

    if (new_next != nullptr) {
      new_next->vertex = vertex;
      new_next->next = nullptr;
    }
  }

  new_next = nullptr;
  child_edge = nullptr;
  delete new_next;
  delete child_edge;
  return;
}

Edge* FPGraph::AddGraphEdge(int p_uid, Graph* graph) {
  Node* parent_vertex = nullptr;
  parent_vertex = DFSGraphTraverse(p_uid, graph);
  if (parent_vertex == nullptr) {
    std::cout << "\nCouldn't find parent vertex\n";
    std::cout.flush();
    return nullptr;
  }
  Edge* next_;
  next_ = parent_vertex->child;

  Edge* new_next = new Edge;
  new_next->vertex = nullptr;
  new_next->next = nullptr;

  if (next_ != nullptr) {
    while (next_->next != nullptr) {
      next_ = next_->next;
    }
    next_->next = new_next;
  }

  return new_next;
}

void FPGraph::AddGraphEdge(int p_uid, int c_uid, Graph* graph) {
  Node *parent_vertex, *child_vertex;
  parent_vertex = DFSGraphTraverse(p_uid, graph);
  child_vertex = DFSGraphTraverse(c_uid, graph);
  if (parent_vertex == nullptr || child_vertex == nullptr) {
    std::cout << "\nCouldn't find parent or child vertex\n";
    std::cout.flush();
    return;
  }

  Edge* new_next = new Edge;
  new_next->vertex = child_vertex;
  new_next->next = nullptr;

  if (parent_vertex->child == nullptr) {
    parent_vertex->child = new_next;
    return;
  }

  Edge* next_ = new Edge;
  next_ = parent_vertex->child;
  while (next_->next != nullptr) {
    next_ = next_->next;
  }
  next_->next = new_next;

  next_ = nullptr;
  delete next_;
  return;
}

Edge* FPGraph::AddGraphEdge(Node* p_vertex, Node* c_vertex,
                            uint32 edge_prior_tag) {
  if (p_vertex == nullptr || c_vertex == nullptr) {
    std::cout << "\nCouldn't find parent or child vertex\n";
    std::cout.flush();
    return nullptr;
  }

  Edge* new_next = new Edge;
  new_next->vertex = c_vertex;
  new_next->next = nullptr;
  new_next->priority_tag = edge_prior_tag;

  if (p_vertex->child == nullptr) {
    p_vertex->child = new_next;
    return new_next;
  }

  Edge* next_ = new Edge;
  next_ = p_vertex->child;
  while (next_->next != nullptr) {
    next_ = next_->next;
  }
  next_->next = new_next;

  next_ = nullptr;
  delete next_;
  return new_next;
}

/*Graph* FPGraph::IsGraphReachable(Graph* graph) {

  }*/

Node* FPGraph::DFSGraphTraverse(int uid, Graph* graph) {
  Node* root_ = graph->root;
  if (root_->uniqueId == uid) {
    return root_;
  }

  graph->queue.push_back(root_->uniqueId);
  Edge* next_T;
  next_T = root_->child;
  if (next_T == nullptr) return nullptr;
  Node* vertex_T;
  vertex_T = next_T->vertex;

  if (next_T->next != nullptr) {
    push(next_T);
  }
  graph->queue.push_back(vertex_T->uniqueId);

  while (vertex_T->uniqueId != uid) {
    if (vertex_T->child != nullptr) {
      next_T = vertex_T->child;
      vertex_T = next_T->vertex;
      while (graph->search_queue(vertex_T->uniqueId)) {
        next_T = next_T->next;
        if (next_T != nullptr) vertex_T = next_T->vertex;
      }

      if (next_T->next != nullptr) push(next_T);

    }

    else {
      if (top != nullptr) {
        next_T = pop();
        next_T = next_T->next;
        vertex_T = next_T->vertex;

        while (graph->search_queue(vertex_T->uniqueId)) {
          if (next_T->next != nullptr) {
            next_T = next_T->next;
            if (next_T != nullptr) vertex_T = next_T->vertex;
          } else {
            if (top != nullptr) {
              next_T = pop();
              next_T = next_T->next;
              if (next_T != nullptr) vertex_T = next_T->vertex;
            }
          }
        }

        if (next_T->next != nullptr) push(next_T);
      }

      else {
        std::cout << "\nNode with Unique Id " << uid << " not found \n";
        std::cout.flush();
        return nullptr;
      }
    }
    graph->queue.push_back(vertex_T->uniqueId);
  }
  graph->queue.clear();
  stack_clear();
  return vertex_T;
}

std::vector<int> FPGraph::delete_graph_components(Graph* graph) {
  Node* root_ = graph->root;
  graph->queue.push_back(root_->uniqueId);

  Edge* next_T;
  next_T = root_->child;
  Node* vertex_T;
  if (next_T == nullptr) return graph->queue;
  vertex_T = next_T->vertex;

  if (next_T->next != nullptr) {
    push(next_T);
  }

  graph->queue.push_back(vertex_T->uniqueId);
  while (top != nullptr || vertex_T->child != nullptr) {
    if (vertex_T->child != nullptr) {
      next_T = vertex_T->child;
      vertex_T = next_T->vertex;
      while (graph->search_queue(vertex_T->uniqueId)) {
        next_T = next_T->next;
        if (next_T != nullptr) vertex_T = next_T->vertex;
      }

      if (next_T->next != nullptr) push(next_T);

    }

    else {
      if (top != nullptr) {
        next_T = pop();
        next_T = next_T->next;
        vertex_T = next_T->vertex;
        while (graph->search_queue(vertex_T->uniqueId)) {
          if (next_T->next != nullptr) {
            next_T = next_T->next;
            if (next_T != nullptr) vertex_T = next_T->vertex;
          }

          else {
            if (top != nullptr) {
              next_T = pop();
              next_T = next_T->next;
              if (next_T != nullptr) vertex_T = next_T->vertex;
            }

            else
              break;
          }
        }

        if (next_T->next != nullptr) push(next_T);
      }
    }
    if (!graph->search_queue(vertex_T->uniqueId))
      graph->queue.push_back(vertex_T->uniqueId);
  }
  stack_clear();

  return std::move(graph->queue);
}

void FPGraph::DisplayGraph() {
  std::vector<int> queue_;
  queue_ = delete_graph_components(graph);
  std::cout << " \nDFS Queue contains: \n";
  for (std::vector<int>::iterator it = queue_.begin(); it != queue_.end(); it++)
    std::cout << *it << " ";
  std::cout << '\n';
  return;
}

void FPGraph::push(Edge* edge_) {
  struct Stack* temp = new Stack;
  temp->edge = edge_;
  temp->link = top;
  top = temp;

  return;
}

Edge* FPGraph::pop(void) {
  struct Stack* temp;
  struct Edge* edge_;

  if (top == nullptr) {
    std::cout << "\nStack Underflow\n";
    std::cout.flush();
    return nullptr;
  }

  else {
    temp = top;
    top = top->link;
    temp->link = nullptr;
  }

  edge_ = temp->edge;
  temp->edge = nullptr;
  temp = nullptr;
  delete temp;
  return edge_;
}

void FPGraph::stack_clear() {
  struct Stack* temp;

  while (top != nullptr) {
    temp = top;
    top = top->link;
    temp->edge = nullptr;
    temp->link = nullptr;
    delete temp;
  }

  return;
}

bool Graph::search_queue(int uid) {
  for (std::vector<int>::iterator it = queue.begin(); it != queue.end(); it++) {
    if (*it == uid) return true;
  }

  return false;
}

int64 Graph::num_parameters() { return param_count; }

Node* FPGraph::GetGraphVertex(const HloModule& hlo_module, Graph* graph,
                              int64 param_number) {
  const HloComputation* computation = hlo_module.entry_computation();
  for (auto instruction_ : computation->instructions()) {
    if (instruction_->opcode() == HloOpcode::kParameter) {
      Node* Vertex = DFSGraphTraverse(instruction_->unique_id(), graph);
      if (Vertex != nullptr) {
        if (Vertex->node_type_.p.param_number == param_number) return Vertex;
      }
    }
  }

  return nullptr;
}

void Parameter::set_host_kernel_ptr(void* ptr) {
  host_kernel_ptr = ptr;
  return;
}

void* Parameter::get_host_kernel_ptr() { return host_kernel_ptr; }

void* Parameter::get_fpga_kernel_ptr() { return fpga_kernel_ptr; }

uint64 Parameter::get_param_size() { return param_size; }

void Parameter::set_param_size(uint64 size) { param_size = size; }

Reduce::Reduce() {
  int64 a = 10;
  Operand_Shape op;
  init_value_ptr.push_back(&a);
  input_shape.push_back(op);
  init_shape.push_back(op);
}

}  // namespace fpga
}  // namespace xla
