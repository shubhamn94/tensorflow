#include "fGraph.h"
namespace xla {
namespace fpga {
void fNode::pre_visit(graphVisitor *visitor) {visitor->pre_visitFNode(this);} //called before visit
void fNode::post_visit(graphVisitor *visitor) {visitor->post_visitFNode(this);} //called after visit
}
}
