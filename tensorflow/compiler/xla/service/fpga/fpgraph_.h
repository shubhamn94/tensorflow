#include <iostream>
#include <stdlib>

class FPGraph : {
 public:
  FPGraph(const HloModule*);
  ~FPGraph(Graph*) override;

  virtual Graph* CreateGraph(const HloModule*) = 0;

  void AddGraphVertex(Node*, Graph*);

  std::vector<int> delete_graph_components(Graph*);

  Node* DFSGraphTraverse(int, Graph*);

  void DFSGraphTraverse(Graph*);

  struct stack {
    struct Edge* edge = nullptr;
    struct stack* link = nullptr;
  } * top;

  top = nullptr;
  void push(struct Edge*);
  struct Edge* pop(void);

  // IsGraphReachable();

 private:
  struct Graph {
    struct Node* root;
    std::vector<int> queue;
    bool search_queue(int);
  }

  struct Node {
    int uniqueId;
    enum tag = {"conv",     "dot",    "broadcast", "parameter",     "add",
                "subtract", "reduce", "divide",    "reduce-window", "maximum"};
    bool visited = false;
    union node_types {
      struct convolution;
      struct dot;
    }

    struct Edge* child;
  }

  struct Edge {
    struct Node* vertex;
    struct Edge* next;
  }

  struct convolution {
    int *lhs, *rhs void *datalayout;

  }

  Edge*
  AddGraphEdge(int, Graph*);
  void AddGraphEdge(int, int, Graph*);
};
