#ifndef RIFFA_STREAM_H
#define RIFFA_STREAM_H
#include "riffa.h"
namespace xla{
namespace fpga{

struct maxpoolParamsType {
  int reset;
  int cflush;
  int Ix;
  int Iy;
  int Iz;
  int Kx;
  int Ky;
  int Sx;
  int Sy;
  int Ox;
  int Oy;
  bool relu;
};

struct convParamsType {
  int Kx;
  int Ky;
  int Sx;
  int Sy;
  int Ix;
  int Iy;
  int Iz;
  int Oz;
};

void print_stacktrace(FILE* out = stderr,
                                    unsigned int max_frames = 63) ;

void myassert(bool condition, const char msg[] = "") ;

/************************************************************************************/
/**********************LEVEL 1 (LOWEST
 * LEVEL)****************************************/
/************************************************************************************/
class send_fifo;
class recv_fifo;
class riffa_stream {
 private:
  fpga_t* fpga;
  void make_send_cmd(unsigned int* sendBuffer, int numWords) ;

  void make_recv_cmd(unsigned int* sendBuffer, int numWords) ;

  int send(unsigned int* sendBuffer, int numWords) ;

  int recv(unsigned int* recvBuffer, int numWords) ;

 public:
  riffa_stream() ;

  ~riffa_stream() ;
  friend riffa_stream& operator<<(riffa_stream& fpga, send_fifo& sBuffer);
  friend riffa_stream& operator>>(riffa_stream& fpga, recv_fifo& rBuffer);
};

/************************************************************************************/
/************************************LEVEL
 * 2*****************************************/
/************************************************************************************/
#define CMD 0
#define IFMAP 1
#define KERNEL 2
#define TXCMD 3
#define BIAS 4

struct cmdType {
  bool reset;
  bool cflush;
  int Kx;              // 7
  int Ky;              // 7
  int Sx;              // 7
  int Sy;              // 7
  int Ix;              // 13
  int Iy;              // 13
  int Iz;              // 13
  int Oz;              // 13
  int num_active_PEs;  // 13
};

void serialize_conv_cmd(convParamsType conv_params, unsigned int* buffer,
                        int rst, int cflush, int num_active_PEs) ;

void serialize_maxpool_cmd(maxpoolParamsType command, unsigned int* buffer,
                           int rst, int cflush) ;

class send_fifo {
 public:
  std::vector<unsigned int> sendVector;
  std::vector<float> pendingIfmap;
  std::vector<float> pendingKernel;
  std::vector<float> pendingBias;
  void verify_params(convParamsType& conv_params,
                     maxpoolParamsType& maxpool_params, int num_active_PEs) ;

  void push_ifmap128() ;

  void push_kernel128() ;

  void push_bias128() ;

 public:
  send_fifo() ;

  void push_reset() ;

  void push_cmd(convParamsType conv_params, maxpoolParamsType maxpool_params,int _NUM_PE_PER_CORE) ;

  void flush_pending() ;

  void push_ifmap(float dataF) ;

  void push_kernel(float dataF) ;

  void push_bias(float dataF) ;

  void push(unsigned int value) ;

  int size() ;

  void print() ;
  void binarydump(const char filename[]) ;

  friend riffa_stream& operator<<(riffa_stream& fpga, send_fifo& sBuffer);
};

class recv_fifo {
 public:
  int loc;
  int nelems;
  int _size;

 public:
  std::vector<unsigned int> recvVector;
  recv_fifo(int sz) ;

  float pop(int step = 1) ;

  int size() ;

  void resize(int sz) ;
  void shrink(int sz) ;
  void print() ;

  friend riffa_stream& operator>>(riffa_stream& fpga, recv_fifo& rBuffer);
};

//riffa_stream& operator<<(riffa_stream& fpga, send_fifo& sBuffer) ;

//riffa_stream& operator>>(riffa_stream& fpga, recv_fifo& rBuffer) ;
}
}
#endif
