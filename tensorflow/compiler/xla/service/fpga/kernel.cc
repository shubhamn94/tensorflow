/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include "tensorflow/compiler/xla/service/fpga/kernel.h"

#include <iostream>
#include "tensorflow/compiler/xla/status.h"
#include "tensorflow/compiler/xla/types.h"
#include "tensorflow/core/platform/logging.h"

namespace xla {
namespace fpga {

Kernel::Kernel() {
  host_kernel_ptr = nullptr;
  fpga_kernel_ptr = nullptr;

  kernel_byte_size = 0;  //(kernel_size_bytes)/sizeof(float); //Kernel weights
                         //and biases will always be float type
}

void* Kernel::Allocate(uint64 size) {
  kernel_byte_size = size;
  return new char[size];
}

void Kernel::Deallocate(void* mem) { delete[] static_cast<char*>(mem); }

/*Kernel::~Kernel() {
    LOG(INFO) << "Kernel destructor is being called!! ";
    Deallocate(host_kernel_ptr);
}*/

void Kernel::set_host_kernel_ptr(void* ptr) {
  host_kernel_ptr = ptr;
  return;
}

void* Kernel::get_host_kernel_ptr() { return host_kernel_ptr; }

void* Kernel::get_fpga_kernel_ptr() { return fpga_kernel_ptr; }

uint64 Kernel::get_kernel_byte_size() { return kernel_byte_size; }

}  // namespace fpga
}  // namespace xla
