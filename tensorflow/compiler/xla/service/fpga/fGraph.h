#ifndef FGRAPH_H
#define FGRAPH_H
#include "tensorflow/compiler/xla/xla_data.pb.h"
#include "tensorflow/core/platform/logging.h"
namespace xla {
namespace fpga {
class graphVisitor;
class fFusedConv;
class fConvolution;
class fParameter;
class fConstant;
class fAdd;
class fMaximum;
class fSubtract;
class fReshape;
class fBroadcast;
class fReduceWindow;
class fTuple;
class fGetTupleElement;

/*******************************************************************/
struct Padding {  // origin is top-left. i.e. top padding is vertical dim near
                  // origin.
  int top;
  int bottom;
  int left;
  int right;
};

class fNode {
 public:
  fNode() { id = -1; }
  int id;
  std::vector<fNode *> children;
  std::vector<fNode *> operands;
  std::vector<int> result_dims;
  std::vector<int> result_layout;
  PrimitiveType result_type;
  const void *result;
  int get_result_size_in_bytes() {
    int out=0;
    LOG(INFO)<<"Calculating result_buffer size in bytes for node "<<getNodeType()<<",id="<<id<<":";
    if(result_type == PrimitiveType::F32){
        out = 4;
        LOG(INFO)<<"\ttype=F32";
    }else if(result_type == PrimitiveType::F16){
        out = 2;
        LOG(INFO)<<"\ttype=F16";
    }
    else{
      LOG(INFO)<<"\ttype=Unknown";
      LOG(INFO)<<"ERROR: Unsupported type found";
    }
    LOG(INFO)<<"\tdims:";
    for (auto dim : result_dims) {
      LOG(INFO)<<"\t- "<<dim;
      out *= dim;
    }
    return out;
  }

  void setOperand(fNode *operand) {
    operands.push_back(operand);
    operand->children.push_back(this);
  }
  // virtual methods
  virtual void visit(graphVisitor *visitor)=0 ;
  virtual std::string getNodeType()=0;
  void pre_visit(graphVisitor *visitor) ;//called before visit
  void post_visit(graphVisitor *visitor) ;//called after visit
  virtual ~fNode() {}
};

typedef std::vector<fNode *> vec_nodePtr;
class fGraph {
 public:
  virtual void dummy(){
    LOG(INFO)<<"This is a dummy virtual func to allow dyanmic cast";
  }
  fNode* root_node;
  vec_nodePtr nodes;  // root must be node zero
  template <class T>
  T *addNode() {
    T *node = new T;
    node->id = nodes.size();
    nodes.push_back(static_cast<fNode *>(node));
    return node;
  }
  int size() { return nodes.size(); }
  vec_nodePtr getPostOrderTraversal() {
    vec_nodePtr nodesInPostOrder;
    std::vector<bool> visited(nodes.size(), false);
    visitSubgraphInPostOrder(root_node, visited, nodesInPostOrder);
    return nodesInPostOrder;
  }
  ~fGraph() {
    for (auto node : nodes) {
      delete (node);
    }
  }

  void visitSubgraphInPostOrder(fNode *node, std::vector<bool> &visited,
                                vec_nodePtr &nodesInPostOrder) {
    if (visited[node->id]) return;
    visited[node->id] = true;
    for (auto operand : node->operands) {
      visitSubgraphInPostOrder(operand, visited, nodesInPostOrder);
    }
    nodesInPostOrder.push_back(node);
  }
};

class graphVisitor {
 public:
  fGraph &graph;
  /****************Non-Virtual Members**************************/
  graphVisitor(fGraph &_graph) : graph(_graph){};
  int graph_size() { return graph.size(); }
  void visitPostOrder() {  // root visited last
    vec_nodePtr nodesInPostOrder = graph.getPostOrderTraversal();
    LOG(INFO) <<"nodesInPostOrder.size()=="<<nodesInPostOrder.size();
    for (fNode *node : nodesInPostOrder) {
      LOG(INFO)<<"Executing node "<<node->getNodeType()<<" id "<<node->id;
      node->pre_visit(this);
      node->visit(this);
      node->post_visit(this);
      LOG(INFO)<<"Executed node "<<node->getNodeType()<<" id "<<node->id;
      //node->get_result_size_in_bytes();
    }
  }
  void visitReversePostOrder() {  // root visited first
    vec_nodePtr nodesInPostOrder = graph.getPostOrderTraversal();
    for (auto nodeIterator = nodesInPostOrder.rbegin();
         nodeIterator != nodesInPostOrder.rend(); nodeIterator++) {
      (*nodeIterator)->visit(this);
    }
  }
  /*******************Virtual Members**************************/
  virtual void pre_visitFNode(fNode *)            {} ;
  virtual void post_visitFNode(fNode *)            {} ;
  virtual void visitFusedConv(fFusedConv *)            {} ;
  virtual void visitConvolution(fConvolution *)        {} ;
  virtual void visitParameter(fParameter *)            {} ;
  virtual void visitConstant(fConstant *)              {} ;
  virtual void visitAdd(fAdd *)                        {} ;
  virtual void visitMaximum(fMaximum *)                {} ;
  virtual void visitSubtract(fSubtract *)              {} ;
  virtual void visitReshape(fReshape *)                {} ;
  virtual void visitBroadcast(fBroadcast *)            {} ;
  virtual void visitReduceWindow(fReduceWindow *)      {} ;
  virtual void visitTuple(fTuple *)                    {} ;
  virtual void visitGetTupleElement(fGetTupleElement *){} ;
};

//*************************NODE IMPLEMENTATIONS**********************//

class fFusedConv : public fNode {
 public:
  void visit(graphVisitor *visitor) { visitor->visitFusedConv(this); }
   std::string getNodeType(){return std::string("fFusedConv");};
};

struct convParams{
  int batch;
  int Ix;
  int Iy;
  int Iz;
  int Ox;
  int Oy;
  int Oz;
  int Kx;
  int Ky;
  int Sx;
  int Sy;
}; 

class fConvolution : public fNode {
 public:
  struct convParams params;
  struct Padding padding;
  void visit(graphVisitor *visitor) { visitor->visitConvolution(this); }
   std::string getNodeType(){return std::string("fConvolution");};
};

class fParameter : public fNode {
 public:
  // TODO:
  // Parameters are assigned params during execution
  // To do that each parameter has a unique number
  // Check execute_function.cc for details and add reqd members
  int parameter_number;
  void visit(graphVisitor *visitor) { visitor->visitParameter(this); }
   std::string getNodeType(){return std::string("fParameter");};
};

class fConstant : public fNode {
 public:
  void visit(graphVisitor *visitor) { visitor->visitConstant(this); }
   std::string getNodeType(){return std::string("fConstant");};
};

class fAdd : public fNode {
 public:
  void visit(graphVisitor *visitor) { visitor->visitAdd(this); };
   std::string getNodeType(){return std::string("fAdd");};
};

class fMaximum : public fNode {
 public:
  void visit(graphVisitor *visitor) { visitor->visitMaximum(this); };
   std::string getNodeType(){return std::string("fMaximum");};
};

class fSubtract : public fNode {
 public:
  void visit(graphVisitor *visitor) { visitor->visitSubtract(this); };
   std::string getNodeType(){return std::string("fSubtract");};
};

class fReshape : public fNode {
 public:
  // This node is a NOP. It is removed by the graph optimizer
  void visit(graphVisitor *visitor) { visitor->visitReshape(this); };
   std::string getNodeType(){return std::string("fReshape");};
};

class fBroadcast : public fNode {
 public:
  // This node is a NOP. It is removed by the graph optimizer
  struct {
    std::vector<int> broadcast_dims;
  } params;
  void visit(graphVisitor *visitor) { visitor->visitBroadcast(this); };
   std::string getNodeType(){return std::string("fBroadcast");};
};

class fReduceWindow : public fNode {
 public:
  // FIXME: Only handles maxpool style reduce window operations
  struct {
    int Kx;
    int Ky;
    int Sx;
    int Sy;
  } params;

  struct Padding padding;
  void visit(graphVisitor *visitor) { visitor->visitReduceWindow(this); };
   std::string getNodeType(){return std::string("fReduceWindow");};
};

class fTuple : public fNode {
 public:
  void visit(graphVisitor *visitor) { visitor->visitTuple(this); };
   std::string getNodeType(){return std::string("fTuple");};
};

class fGetTupleElement : public fNode {
 public:
  void visit(graphVisitor *visitor) { visitor->visitGetTupleElement(this); };
   std::string getNodeType(){return std::string("fGetTupleElement");};
};
}
}
#endif
