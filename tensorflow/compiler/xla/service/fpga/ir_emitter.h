/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ==============================================================================*/

#ifndef TENSORFLOW_COMPILER_XLA_SERVICE_FPGA_IR_EMITTER_H_
#define TENSORFLOW_COMPILER_XLA_SERVICE_FPGA_IR_EMITTER_H_

#include <stddef.h>
#include <map>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "tensorflow/compiler/xla/service/fpga/fpgraph.h"

#include "absl/container/flat_hash_map.h"
#include "absl/strings/string_view.h"
#include "absl/types/span.h"
#include "tensorflow/compiler/xla/service/buffer_assignment.h"
#include "tensorflow/compiler/xla/service/dfs_hlo_visitor_with_default.h"
#include "tensorflow/compiler/xla/service/hlo_instruction.h"
#include "tensorflow/compiler/xla/service/hlo_instructions.h"
#include "tensorflow/compiler/xla/service/hlo_module_config.h"
#include "tensorflow/compiler/xla/service/hlo_verifier.h"
#include "tensorflow/compiler/xla/service/name_uniquer.h"
#include "tensorflow/compiler/xla/statusor.h"
#include "tensorflow/compiler/xla/types.h"
#include "tensorflow/compiler/xla/xla_data.pb.h"
#include "tensorflow/core/platform/macros.h"
#include "tensorflow/core/platform/types.h"

namespace xla {
namespace fpga {
// This class is the top-level API for the XLA HLO --> LLVM IR compiler.  It
// implements the DfsHloVisitor interface and emits HLO computations as LLVM IR
// functions.

class IrEmitter : public DfsHloVisitorWithDefault,
                  public FPGraph,
                  public ShapeVerifier {
 public:
  IrEmitter(
      const HloModule& hlo_module) /*, const BufferAssignment& assignment)*/;
  ~IrEmitter() override;
  //---------------Added by Kingshuk-----------------
  FILE* fp_log;
  //-------------------------------------------------
  Status CreateGraph(const HloModule&);

  Graph* GetGraph() const { return graph_; }

  Status ElementTypesSameAndSupported(
      const HloInstruction& instruction,
      absl::Span<const HloInstruction* const> operands,
      absl::Span<const PrimitiveType> supported_types);

  // Hash map for HLO instruction to Intermediate Graph Node.
  absl::flat_hash_map<int, Node*> instruction_to_node_map;

  // Hash map for mapping Subcomputation HLO instructions to Intermediate sub
  // graph.
  absl::flat_hash_map<int, Node*> subcomp_reduce_instruction_to_node_map;

  // Hash map for mapping Reduce Window Subcomputation HLO instructions to
  // Intermediate sub-graph.
  absl::flat_hash_map<int, Node*> subcomp_reducewind_instruction_to_node_map;

  // std::unordered_map<const HloInstruction*, void*> emitted_value_;

 protected:
  Status DefaultAction(HloInstruction* hlo) override;
  // Status HandleBitcast(HloInstruction* bitcast) override;
  Status HandleBroadcast(HloInstruction* broadcast) override;
  Status HandleReshape(HloInstruction* reshape) override;
  // Status HandleTranspose(HloInstruction* transpose) override;
  // Status HandleCopy(HloInstruction* Copy) override;
  // Status HandleConvert(HloInstruction* convert) override;
  Status HandleConstant(HloInstruction* constant) override;
  Status HandleGetTupleElement(HloInstruction* get_tuple_element) override;
  Status HandleDot(HloInstruction* dot) override;
  Status HandleConvolution(HloInstruction* convolution) override;
  Status HandleParameter(HloInstruction* parameter) override;
  // Status HandleReduce(HloInstruction* reduce) override;
  Status HandleReduceWindow(HloInstruction* reduce_window) override;
  Status HandleMaximum(HloInstruction* maximum) override;
  Status HandleAdd(HloInstruction* add) override;
  Status HandleSubtract(HloInstruction* subtract) override;
  Status HandleDivide(HloInstruction* divide) override;
  Status HandleTuple(HloInstruction* Tuple) override;
  // Status HandlePad(HloInstruction* pad) override;
  Status HandleElementwiseBinary(HloInstruction* hlo) override;

 private:
  Graph* graph_;
  void create_node_to_xla_op_mapping(const HloComputation*);
  void addEdges_old(const HloComputation*);
  void addEdges_new(const HloComputation*);
  TF_DISALLOW_COPY_AND_ASSIGN(IrEmitter);
};

}  // namespace fpga
}  // namespace xla

#endif  // TENSORFLOW_COMPILER_XLA_SERVICE_CPU_IR_EMITTER_H_
