void conv(fpga_tensor<float> &outpTensor, const convParamsType conv_params,
          fpga_tensor<float> &inpTensor, fpga_tensor<float> &biasTensor,
          fpga_tensor<float> &wtTensor) {
  int Ox = calc_output_dim(conv_params.Ix, conv_params.Kx, conv_params.Sx);
  int Oy = calc_output_dim(conv_params.Iy, conv_params.Ky, conv_params.Sy);
  int Oz = conv_params.Oz;
  int Iz = conv_params.Iz;
  int Kx = conv_params.Kx;
  int Ky = conv_params.Ky;
  int Sx = conv_params.Sx;
  int Sy = conv_params.Sy;

  if (wtTensor.C == 0) {
    printf("wtTensor.C=%d,conv_params.Oz=%d!\n", wtTensor.C, conv_params.Oz);
    exit(1);
  }

  for (int i = 0; i < Oy; i++) {
    for (int j = 0; j < Ox; j++) {
      for (int co = 0; co < Oz; co++) {
        float val = 0;
        for (int ky = 0; ky < Ky; ky++) {
          for (int kx = 0; kx < Kx; kx++) {
            for (int ci = 0; ci < Iz; ci++) {
              float inp = inpTensor(0, i * Sy + ky, j * Sx + kx, ci);
              float wt = wtTensor(ky, kx, ci, co);
              val += inp * wt;
            }
          }
        }
        outpTensor(0, i, j, co) = val + biasTensor(0, 0, 0, co);
      }
    }
  }
}

void cpu_conv(fpga_tensor<float> &outpTensor, const convParamsType conv_params,
              const maxpoolParamsType maxpool_params,
              fpga_tensor<float> &inpTensor, fpga_tensor<float> &biasTensor,
              fpga_tensor<float> &wtTensor) {
  conv(outpTensor, conv_params, inpTensor, biasTensor, wtTensor);
}
