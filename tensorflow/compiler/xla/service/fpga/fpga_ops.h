#ifndef FPGA_OPS_H
#define FPGA_OPS_H
#include <sys/time.h>
#include <chrono>
#include <cstring>
#include <thread>
#include "tiling.h"
namespace xla{
  namespace fpga{

int min(int a, int b);
template <typename T>
class fpga_tensor {
 public:
  T *arr;
  bool forget_mem;

 public:
  unsigned int N;
  unsigned int H;
  unsigned int W;
  unsigned int C;
  bool initialized;

  fpga_tensor(unsigned int N, unsigned int H, unsigned int W, unsigned int C,
              T init_val) ;

  fpga_tensor(unsigned int N, unsigned int H, unsigned int W, unsigned int C) ;

  fpga_tensor(unsigned int N, unsigned int H, unsigned int W, unsigned int C,
              T *buffer,bool _forget_mem=true) ;

  void create_tensor(unsigned int N, unsigned int H, unsigned int W,
                     unsigned int C, T *buffer) ;

  fpga_tensor(fpga_tensor<T> &&other) ;

  fpga_tensor(fpga_tensor<T> &other) ;
  ~fpga_tensor(); 

  T &operator()(unsigned int n, unsigned int h, unsigned int w,
                       unsigned int c) ;
  void print();
};

template<typename T,typename T2>
fpga_tensor<T> make_fpga_tensor(unsigned int N, unsigned int H, unsigned int W, unsigned int C,
            T2 init_val);
void fpga_conv(fpga_tensor<float> &outTensor, const convParamsType conv_params,
               const maxpoolParamsType maxpool_params,
               fpga_tensor<const float> &inpTensor, fpga_tensor<const float> &biasTensor,
               fpga_tensor<const float> &wtTensor,int _NUM_PE_PER_CORE) ;


//IMPEMENTATION OF TEMPLATED FUNCTIONS
//Templates can't be linked later. Must be header only

template <typename T>
fpga_tensor<T>::fpga_tensor(unsigned int N, unsigned int H, unsigned int W,
                            unsigned int C, T init_val) {
  T *buffer = (T *)malloc(sizeof(T) * (N * H * W * C));
  // T *buffer = new T[N * H * W * C];
  forget_mem = false;
  for (int i = 0; i < N * H * W * C; i++) {
    buffer[i] = init_val;
  }
  create_tensor(N, H, W, C, buffer);
}

template <typename T>
fpga_tensor<T>::fpga_tensor(unsigned int N, unsigned int H, unsigned int W,
                            unsigned int C) {
  T *buffer = (T *)malloc(sizeof(T) * (N * H * W * C));
  // T *buffer = new T[N * H * W * C];
  forget_mem = false;
  create_tensor(N, H, W, C, buffer);
}

template <typename T>
fpga_tensor<T>::fpga_tensor(unsigned int N, unsigned int H, unsigned int W,
                            unsigned int C, T *buffer, bool _forget_mem) {
  forget_mem =
      _forget_mem;  // didn't allocate memory so don't delete it in destructor
  create_tensor(N, H, W, C, buffer);
}

template <typename T>
void fpga_tensor<T>::create_tensor(unsigned int N, unsigned int H,
                                   unsigned int W, unsigned int C, T *buffer) {
  myassert(N > 0, "N must be greater than 0\n");
  myassert(H > 0, "H must be greater than 0\n");
  myassert(W > 0, "W must be greater than 0\n");
  myassert(C > 0, "C must be greater than 0\n");

  this->N = N;
  this->H = H;
  this->W = W;
  this->C = C;
  arr = (T *)buffer;
  initialized = false;
  printf("Tensor.N=%d\n", this->N);
  printf("Tensor.H=%d\n", this->H);
  printf("Tensor.W=%d\n", this->W);
  printf("Tensor.C=%d\n", this->C);
}

template <typename T>
fpga_tensor<T>::fpga_tensor(fpga_tensor<T> &&other) {
  N = other.N;
  H = other.H;
  W = other.W;
  C = other.C;
  arr = other.arr;
  initialized = other.initialized;
  other.arr = nullptr;
}

template <typename T>
fpga_tensor<T>::fpga_tensor(fpga_tensor<T> &other) {
  this->N = N;
  this->H = H;
  this->W = W;
  this->C = C;
  arr = (T *)malloc(sizeof(T) * (N * H * W * C));
  // arr = new T[N * H * W * C];
  memcpy((void *)arr, (void *)other.arr, N * H * W * C * sizeof(T));
  initialized = other.initialized;
}

template <typename T>
fpga_tensor<T>::~fpga_tensor() {
  if (arr != nullptr && !forget_mem) {
    free((void *)arr);
    // delete(arr);
  }
}

template <typename T>
T& fpga_tensor<T>::operator()(unsigned int n, unsigned int h,
                                     unsigned int w, unsigned int c) {
  // std::cout << "W=" <<W<<std::endl;
  // std::cout << "w=" <<w<<std::endl;
  if (n >= N) {
    printf("overflow in dim N. n(%d)>=N(%d)\n", n, N);
    myassert(false);
  }
  if (c >= C) {
    printf("overflow in dim C. c(%d)>=C(%d)\n", c, C);
    exit(1);
  }
  myassert(h < H, "overflow in dimension H\n");
  myassert(w < W, "overflow in dimension W\n");
  myassert(arr != nullptr, "Trying to access data from moved fpga_tensor\n");
  initialized =
      true;  // this is not perfect. A read can also set it. For now its ok
  return arr[n * (H * W * C) + h * (W * C) + w * C + c];
}

template <typename T>
void fpga_tensor<T>::print() {
  if (!initialized) {
    std::cout << "\n'Empty Array'\n";
    return;
  }

  for (int n = 0; n < N; n++) {
    for (int c = 0; c < min(3, C); c++) {
      for (int h = 0; h < min(10, H); h++) {
        for (int w = 0; w < min(10, W); w++) {
          std::cout << arr[n * (H * W * C) + h * (W * C) + w * C + c] << "\t, ";
        }
        if (W > 10) {
          std::cout << " ...";
        }
        std::cout << std::endl;
      }
      if (H > 10) {
        std::cout << ".\n.\n.\n";
      }
      if (c == C - 1) continue;
      for (int w = 0; w < min(10, W); w++) {
        std::cout << "-";
      }
      std::cout << std::endl;
    }
    if (n == N - 1) continue;
    for (int w = 0; w < min(10, W); w++) {
      std::cout << "=";
    }
    std::cout << std::endl;
  }
}

template <typename T, typename T2>
fpga_tensor<T> make_fpga_tensor(unsigned int N, unsigned int H, unsigned int W,
                                unsigned int C, T2 init_val) {
  T2 *buffer = (T2 *)malloc(sizeof(T) * (N * H * W * C));
  // T *buffer = new T[N * H * W * C];
  for (int i = 0; i < N * H * W * C; i++) {
    buffer[i] = init_val;
  }
  return fpga_tensor<T>(N, H, W, C, buffer, false);
}


}
}
#endif
