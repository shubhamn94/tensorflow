#include "fGraph.h"
namespace xla {
namespace fpga {
#include <sstream>

class printGraphPass : public graphVisitor {
  FILE *fp;

 public:
  printGraphPass(fGraph& graph) : graphVisitor(graph) {}

  void printDotfile(const char *filename) {
    static int n=0;
    std::ostringstream fullname;
    fullname<<filename<<n<<".dot";
    n++;
    LOG(INFO) <<"printing to "<<fullname.str()<<" file";
    fp = fopen(fullname.str().c_str(), "w");
    if(!fp){
      LOG(INFO)<<"Could not open file";
      exit(1);
    }
    fprintf(fp, "digraph G{\n");
    visitPostOrder();
    fprintf(fp, "}");
    fclose(fp);
    fp=0;
  }
  void pre_visitFNode(fNode *node) {
    if(!fp){
      LOG(INFO)<<"no file open";
      exit(1);
    }

    std::ostringstream label;
    label<<node->getNodeType()<<"(id="<<node->id<<",result_dims=[";
    for(int dim:node->result_dims){
      label<<dim<<",";
    }
    label<<"],"<<"size="<<node->get_result_size_in_bytes()<<")";;
    LOG(INFO)<<"Executing node "<<node->getNodeType();
    fprintf(fp, "node%d[label=\"%s\"];\n", node->id,label.str().c_str());
    for (auto operand : node->operands) {
      fprintf(fp, "node%d -> node%d ;\n", operand->id, node->id);
    }
  }
  //void visitFusedConv(fFusedConv *node) {
  //  fprintf(fp, "node%d[label=\"fusedConv(id=%d)\"];\n", node->id, node->id);
  //}
  //void visitConvolution(fConvolution *node) {
  //  fprintf(fp, "node%d[label=\"convolution(id=%d)\"];\n", node->id, node->id);
  //}
  //void visitParameter(fParameter *node) {
  //  fprintf(fp, "node%d[label=\"parameter(id=%d)\"];\n", node->id, node->id);
  //}
  //void visitConstant(fConstant *node) {
  //  fprintf(fp, "node%d[label=\"constant(id=%d)\"];\n", node->id, node->id);
  //}
  //void visitAdd(fAdd *node) {
  //  fprintf(fp, "node%d[label=\"add(id=%d)\"];\n", node->id, node->id);
  //}
  //void visitMaximum(fMaximum *node) {
  //  fprintf(fp, "node%d[label=\"maximum(id=%d)\"];\n", node->id, node->id);
  //}
  //void visitSubtract(fSubtract *node) {
  //  fprintf(fp, "node%d[label=\"subtract(id=%d)\"];\n", node->id, node->id);
  //}
  //void visitReshape(fReshape *node) {
  //  fprintf(fp, "node%d[label=\"reshape(id=%d)\"];\n", node->id, node->id);
  //}
  //void visitBroadcast(fBroadcast *node) {
  //  fprintf(fp, "node%d[label=\"broadcast(id=%d)\"];\n", node->id, node->id);
  //}
  //void visitReduceWindow(fReduceWindow *node) {
  //  fprintf(fp, "node%d[label=\"reduceWindow(id=%d)\"];\n", node->id, node->id);
  //}
  //void visitTuple(fTuple *node) {
  //  fprintf(fp, "node%d[label=\"tuple(id=%d)\"];\n", node->id, node->id);
  //}
  //void visitGetTupleElement(fGetTupleElement *node) {
  //  fprintf(fp, "node%d[label=\"getTupleElement(id=%d)\"];\n", node->id,
  //          node->id);
  //}
};
}
}
