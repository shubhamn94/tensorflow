/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef TENSORFLOW_COMPILER_XLA_SERVICE_FPGA_FPGA_EXECUTABLE_H_
#define TENSORFLOW_COMPILER_XLA_SERVICE_FPGA_FPGA_EXECUTABLE_H_

#include <memory>
#include <string>
#include <vector>

#include "absl/types/span.h"
#include "tensorflow/compiler/xla/service/buffer_assignment.h"
#include "tensorflow/compiler/xla/service/device_memory_allocator.h"
#include "tensorflow/compiler/xla/service/executable.h"
#include "tensorflow/compiler/xla/service/fpga/execute_function.h"
#include "tensorflow/compiler/xla/service/fpga/fpgraph.h"
#include "tensorflow/compiler/xla/service/hlo_cost_analysis.h"
#include "tensorflow/compiler/xla/service/hlo_execution_profile.h"
#include "tensorflow/compiler/xla/service/hlo_module.h"
#include "tensorflow/compiler/xla/service/hlo_module_config.h"
#include "tensorflow/compiler/xla/service/service_executable_run_options.h"
#include "tensorflow/compiler/xla/service/shaped_buffer.h"
#include "tensorflow/compiler/xla/service/tuple_points_to_analysis.h"
#include "tensorflow/compiler/xla/statusor.h"
#include "tensorflow/compiler/xla/types.h"
#include "tensorflow/compiler/xla/xla_data.pb.h"
#include "tensorflow/core/platform/macros.h"
#include "tensorflow/core/platform/mutex.h"
#include "tensorflow/core/platform/stream_executor_no_cuda.h"
#include "tensorflow/core/platform/types.h"

#include "fGraph.h"
#include "executeGraphPass.h"
#include "printGraphPass.h"
namespace xla {
namespace fpga {

// Responsible for running a HLO graph through the HloEvaluator and output
// buffer allocation. Refer to fpga/README.md for more.
class FpgaExecutable : public Executable {
 public:
  FpgaExecutable(std::unique_ptr<const BufferAssignment> assignment,
                 std::unique_ptr<Graph> graph,
                 fGraph_tf* fgraph_ptr,
                 std::unique_ptr<const HloModule> hlo_module);
  ~FpgaExecutable() override;

  StatusOr<ScopedShapedBuffer> ExecuteOnStream(
      const ServiceExecutableRunOptions* run_options,
      absl::Span<const ShapedBuffer* const> arguments,
      HloExecutionProfile* hlo_execution_profile) override
      LOCKS_EXCLUDED(graph_evaluator_lock_);

  StatusOr<ScopedShapedBuffer> ExecuteAsyncOnStream(
      const ServiceExecutableRunOptions* run_options,
      absl::Span<const ShapedBuffer* const> arguments) override;

  static int64 ShapeSizeBytes(const Shape& shape);

  const BufferAssignment& buffer_assignment() const { return *assignment_; }

  const Graph* Getraph() const { return graph_.get(); }

 protected:
  mutable tensorflow::mutex graph_evaluator_lock_;

 private:
  executeGraphPass graphExecutor;
  StatusOr<std::pair<std::vector<se::DeviceMemoryBase>,
                     std::vector<OwningDeviceMemory>>>
  GetBufferPointer(DeviceMemoryAllocator*, int device_ordinal,
                   absl::Span<const ShapedBuffer* const> arguments);

  // Calls the generated function performing the computation with the given
  // arguments using the supplied buffers.
  Status ExecuteComputeFunction(const ExecutableRunOptions* run_options,
                                absl::Span<const se::DeviceMemoryBase> buffers,
                                HloExecutionProfile* hlo_execution_profile);

  // Creates a ScopedShapedBuffer for holding the result of the computation,
  // moving buffers out of allocated_buffers and into the result as appropriate.
  // The addresses are set according to buffer assignment.
  StatusOr<ScopedShapedBuffer> CreateResultShapedBuffer(
      const ServiceExecutableRunOptions* run_options,
      absl::Span<OwningDeviceMemory> buffers);

  // Execute Function which will execute the Intermediate Graph
  // Execute execute_function_ PT_GUARDED_BY(graph_evaluator_lock_);

  // Returns the points to set of the root instruction of the extry computation.
  // Uses points-to analysis from buffer assignment.
  const PointsToSet& GetRootPointsToSet() const;

  // Buffer assignment for the buffers we need to allocate.
  std::unique_ptr<const BufferAssignment> assignment_;

  // Execute Function which will execute the graph
  Execute execute_function;

  // The Graph containing compiled code.
  const std::unique_ptr<Graph> graph_;
  std::unique_ptr<fGraph_tf> fgraph;//fgraph is not const. results are assigned to it

  TF_DISALLOW_COPY_AND_ASSIGN(FpgaExecutable);
};

}  // namespace fpga
}  // namespace xla

#endif  // TENSORFLOW_COMPILER_XLA_SERVICE_FPGA_FPGA_EXECUTABLE_H_
