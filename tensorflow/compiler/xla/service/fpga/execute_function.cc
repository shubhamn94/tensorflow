/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/
#include "tensorflow/compiler/xla/service/fpga/execute_function.h"

#include <cmath>
#include <iostream>
#include <vector>
#include "absl/types/span.h"
#include "tensorflow/compiler/xla/executable_run_options.h"
#include "tensorflow/compiler/xla/service/fpga/fpgraph.h"
#include "tensorflow/compiler/xla/status.h"
#include "tensorflow/compiler/xla/types.h"
#include "tensorflow/compiler/xla/xla_data.pb.h"
#include "tensorflow/core/platform/logging.h"

#define __NUM_PE_PER_CORE 16
#include "fpga_ops.h"
//#include "cpu_ops.h"
#include "fGraph.h"
using namespace xla::fpga;
fpga_tensor<float> read_inp(int Ix, int Iy, int Iz) {
  FILE* fp = fopen("inpTensor.txt", "r");
  float data;
  fpga_tensor<float> inpTensor(1, Iy, Ix, Iz);

  for (int c = 0; c < Iz; c++) {
    float data = 1.0;
    for (int i = 0; i < Iy; i++) {
      for (int j = 0; j < Ix; j++) {
        fscanf(fp, "%f\n", &inpTensor(0, i, j, c));
      }
    }
  }
  fclose(fp);
  return std::move(inpTensor);
}

fpga_tensor<float> read_weights(int Ky, int Kx, int Iz, int Oz) {
  FILE* fp = fopen("wtTensor.txt", "r");
  fpga_tensor<float> wtTensor(Ky, Kx, Iz, Oz);
  for (int iz = 0; iz < Iz; iz++) {
    for (int kx = 0; kx < Kx; kx++) {
      for (int ky = 0; ky < Ky; ky++) {
        for (int oz = 0; oz < Oz; oz++) {
          fscanf(fp, "%f\n", &wtTensor(ky, kx, iz, oz));
        }
      }
    }
  }
  fclose(fp);
  return std::move(wtTensor);
}

fpga_tensor<float> read_bias(int Oz) {
  fpga_tensor<float> biasTensor(1, 1, 1, Oz);
  for (int oz = 0; oz < Oz; oz++) {
    biasTensor(0, 0, 0, oz) = 0.5;
  }

  return std::move(biasTensor);
}

namespace xla {
namespace fpga {
void zero_pad(float* new_input, float* input, absl::Span<const int64> inp_dim,
              int64 padding_top, int64 padding_bot, int64 padding_left,
              int64 padding_right) {
  int64 x, y, flag = 0;
  std::vector<int64> conv_dim;

  for (auto it = inp_dim.begin(); it != inp_dim.end(); ++it)
    conv_dim.push_back(*it);
  conv_dim[1] += (padding_top + padding_bot);
  conv_dim[2] += (padding_left + padding_right);

  for (int i = 0; i < conv_dim[0]; i++) {
    for (int j = 0, x = 0; j < conv_dim[1]; j++) {
      for (int k = 0, y = 0; k < conv_dim[2]; k++) {
        for (int l = 0; l < conv_dim[3]; l++) {
          if ((j < padding_top) || (j >= (conv_dim[1] - padding_bot))) {
            *(new_input + (i * conv_dim[1] * conv_dim[2] * conv_dim[3]) +
              (j * conv_dim[2] * conv_dim[3]) + (k * conv_dim[3]) + l) = 0;
          } else if ((k < padding_left) ||
                     (k >= (conv_dim[2] - padding_right))) {
            *(new_input + (i * conv_dim[1] * conv_dim[2] * conv_dim[3]) +
              (j * conv_dim[2] * conv_dim[3]) + (k * conv_dim[3]) + l) = 0;
          } else {
            *(new_input + (i * conv_dim[1] * conv_dim[2] * conv_dim[3]) +
              (j * conv_dim[2] * conv_dim[3]) + (k * conv_dim[3]) + l) =
                *(input + (i * inp_dim[1] * inp_dim[2] * inp_dim[3]) +
                  (x * inp_dim[2] * inp_dim[3]) + (y * inp_dim[3]) + l);
            flag = 1;
          }
        }
        if (flag == 1) {
          y++;
          if (y >= inp_dim[2]) {
            y = 0;
            x++;
          }
          if (x >= inp_dim[1]) x = 0;
          flag = 0;
        }
      }
    }
  }
}

void myconv2d(float* result, std::vector<int64> conv_dim,
              std::vector<int64> kernel_dim, float* new_input, float* kernel,
              absl::Span<const int64> inst_dim, int row_str, int col_str) {
  std::cout << "myINFO: myconv2d called\n";
  if (conv_dim[0] != 1) {
    printf("myconv2d: ERROR:: Batch size (%d) != 1\n", conv_dim[0]);
    exit(1);
  }
  if (kernel_dim[0] == 0) {
    printf("ERROR: kernel_dim[0]==0\n");
    exit(1);
  }
  myassert(kernel_dim[2] == conv_dim[3], "kernel_dim[2]!=conv_dim[3]\n");
  convParamsType conv_params;
  maxpoolParamsType maxpool_params;

  conv_params.Kx = kernel_dim[1];
  conv_params.Ky = kernel_dim[0];
  conv_params.Sx = row_str;
  conv_params.Sy = col_str;
  conv_params.Ix = conv_dim[2];
  conv_params.Iy = conv_dim[1];
  conv_params.Iz = conv_dim[3];
  conv_params.Oz = kernel_dim[3];
  // no maxpooling (K=S=1)
  maxpool_params.reset = 0;
  maxpool_params.cflush = 0;
  maxpool_params.Ix =
      calc_output_dim(conv_params.Ix, conv_params.Kx, conv_params.Sx);
  maxpool_params.Iy =
      calc_output_dim(conv_params.Iy, conv_params.Ky, conv_params.Sy);
  maxpool_params.Iz = conv_params.Oz;
  maxpool_params.Kx = 1;
  maxpool_params.Ky = 1;
  maxpool_params.Sx = 1;
  maxpool_params.Sy = 1;
  maxpool_params.Ox =
      calc_output_dim(maxpool_params.Ix, maxpool_params.Kx, maxpool_params.Sx);
  maxpool_params.Oy =
      calc_output_dim(maxpool_params.Iy, maxpool_params.Ky, maxpool_params.Sy);
  maxpool_params.relu = false;
  fpga_tensor<const float> inpTensor(1, conv_params.Iy, conv_params.Ix,
                               conv_params.Iz, new_input);
  fpga_tensor<const float> wtTensor(conv_params.Ky, conv_params.Kx, conv_params.Iz,
                              conv_params.Oz, kernel);
  fpga_tensor<const float> biasTensor=make_fpga_tensor<const float,float>(1, 1, 1, conv_params.Oz, float(0));
  fpga_tensor<float> cpu_outTensor(1, maxpool_params.Oy, maxpool_params.Ox,
                                   maxpool_params.Iz);
  fpga_tensor<float> fpga_outTensor(1, maxpool_params.Oy, maxpool_params.Ox,
                                    maxpool_params.Iz, result);

  fpga_conv(fpga_outTensor, conv_params, maxpool_params, inpTensor, biasTensor,
            wtTensor,__NUM_PE_PER_CORE);
  // cpu_conv (cpu_outTensor
  // ,conv_params,maxpool_params,inpTensor,biasTensor,wtTensor);
  
  // fpga_tensor<float> inpTensor  = read_inp     (
  // conv_params.Ix,conv_params.Iy,conv_params.Iz                ) ;
  // fpga_tensor<float> wtTensor   = read_weights (
  // conv_params.Ky,conv_params.Kx,conv_params.Iz,conv_params.Oz ) ;
  // wtTensor.arr = kernel;
  // fpga_tensor<float> biasTensor2 = read_bias    ( conv_params.Oz ) ;

  // FILE* fptr =
  // fopen("~/tesseract/sources/Examples/riffa_axi/testapp/wtTensor.txt", "w");
  // FILE* wtfptr = fopen("wtTensor.txt", "w");
  // FILE* inpfptr = fopen("inpTensor.txt", "w");

  // if(wtfptr == NULL | inpfptr==NULL)
  //{
  //    printf("file Error!");
  //    exit(1);
  //}

  // for(int n = 0;n<wtTensor.N;n++){
  //    for(int h = 0; h<wtTensor.H;h++){
  //        for(int w = 0; w<wtTensor.W; w++){
  //            for(int c = 0; c<wtTensor.C; c++){
  //                fprintf(wtfptr,"%f\n", wtTensor(n,h,w,c));
  //            }
  //        }
  //    }
  //}

  // for(int n = 0;n<inpTensor.N;n++){
  //    for(int h = 0; h<inpTensor.H;h++){
  //        for(int w = 0; w<inpTensor.W; w++){
  //            for(int c = 0; c<inpTensor.C; c++){
  //                fprintf(inpfptr,"%f\n", inpTensor(n,h,w,c));
  //            }
  //        }
  //    }
  //}

  // fclose(wtfptr);
  // fclose(inpfptr);
  // fpga_tensor<float> inpTensor2  = read_inp     (
  // conv_params.Ix,conv_params.Iy,conv_params.Iz                ) ;
  // fpga_tensor<float> wtTensor2   = read_weights (
  // conv_params.Ky,conv_params.Kx,conv_params.Iz,conv_params.Oz ) ;
  // printf("myconv2d:wtTensor.C=%d\n",wtTensor.C);
  // printf("myconv2d:inpTensor.C=%d\n",inpTensor.C);
  // printf("myconv2d:biasTensor.C=%d\n",biasTensor.C);
  // printf("conv_params.Oz=%d\n",conv_params.Oz);

  // std::cout <<
  // "-------------------------------------------------------------------------------\n";
  // std::cout <<
  // "-----------------------ERROR(FPGA,CPU)-----------------------------------------\n";
  // std::cout <<
  // "-------------------------------------------------------------------------------\n";
  // for(int n = 0;n<fpga_outTensor.N;n++){
  //    for(int h = 0; h<fpga_outTensor.H;h++){
  //        for(int w = 0; w<fpga_outTensor.W; w++){
  //            for(int c = 0; c<fpga_outTensor.C; c++){
  //                float valf = fpga_outTensor(n,h,w,c);
  //                float valc = cpu_outTensor(n,h,w,c);
  //                float err = 100.0*((valf-valc)/valc);
  //                if(err > 0.1 || err < -0.1){
  //                    printf("valf=%f , valc=%f\n",valf,valc);
  //                }
  //            }
  //        }
  //    }
  //}

  printf("conv_dim[2]=%d\n", conv_dim[2]);
  printf("inst_dim[0]=%d\n", inst_dim[0]);
  printf("inst_dim[1]=%d\n", inst_dim[1]);
  printf("inst_dim[2]=%d\n", inst_dim[2]);
  printf("inst_dim[3]=%d\n", inst_dim[3]);

  // printf("fpga_conv over\n");
  // fflush(stdout);

  // check if output sizes match;

  // if(cpu_outTensor.H != inst_dim[1]){printf("myconv2d: ERROR:: H(%d) !=
  // inst_dim[1](%d)",cpu_outTensor.H,inst_dim[1]);}
  // if(cpu_outTensor.W != inst_dim[2]){printf("myconv2d: ERROR:: W(%d) !=
  // inst_dim[2](%d)",cpu_outTensor.W,inst_dim[2]);}
  // if(cpu_outTensor.C != inst_dim[3]){printf("myconv2d: ERROR:: C(%d) !=
  // inst_dim[3](%d)",cpu_outTensor.C,inst_dim[3]);}

  // std::vector<int64> conv_cond(4);
  // conv_cond[0] = conv_dim[0];
  // conv_cond[1] = conv_dim[1] - kernel_dim[0] + 1;
  // conv_cond[2] = conv_dim[2] - kernel_dim[1] + 1;
  // conv_cond[3] = conv_dim[3];
  // for(int i = 0; i < conv_cond[0]; i++) {
  //    for(int j = 0; j < conv_cond[1]; (j = j + row_str)) {
  //        for(int k = 0; k < conv_cond[2]; (k = k + col_str)) {
  //            for(int out_c = 0; out_c < kernel_dim[3]; out_c++) {
  //                float sum = 0;
  //                for(int in_ch = 0; in_ch < kernel_dim[2]; in_ch++) {
  //                    for(int h = 0; h < kernel_dim[0]; h++) {
  //                        for(int c = 0; c < kernel_dim[1]; c++) {
  //                            sum += (*(new_input +
  //                            (i*conv_dim[1]*conv_dim[2]*conv_dim[3]) +
  //                                        ((j+h)*conv_dim[2]*conv_dim[3]) +
  //                                        ((k+c)*conv_dim[3]) + in_ch)) *
  //                                (*(kernel +
  //                                (h*kernel_dim[1]*kernel_dim[2]*kernel_dim[3])
  //                                +
  //                                   (c*kernel_dim[2]*kernel_dim[3]) +
  //                                   (in_ch*kernel_dim[3]) + out_c));
  //                        }
  //                    }
  //                }
  //                *(result + (i*inst_dim[1]*inst_dim[2]*inst_dim[3]) +
  //                (j*inst_dim[2]*inst_dim[3]) +
  //                        (k*inst_dim[3]) + out_c) = sum;
  //            }
  //        }
  //    }
  //}
}

Execute::Execute() {}

Execute::~Execute() {}

void* Execute::get_result_handle() { return result_handle; }

void Execute::Execute_Graph(void* result,
                            const ExecutableRunOptions* run_options,
                            const Graph* graph, 
                            const fGraph* fgraph, 
                            int64* profile_counters,
                            int64 result_size) {
  graph_ = graph;
  result_handle = TraverseAndExecute();
  if (result_handle != nullptr) memcpy(result, result_handle, result_size);
}

void Execute::Execute_Handle(Node* vertex) {
  switch (vertex->tag) {
    case 0:
      if (vertex->child != nullptr) Execute_Handle(vertex->child->vertex);
      vertex->result_ptr = vertex->child->vertex->result_ptr;
      break;
    case 1:
      Execute_Convolution(vertex);
      break;
    case 2:
      LOG(INFO) << "ERROR: Execute_Dot is not implemented\n";
      exit(1);
    case 3:
      Execute_Broadcast(vertex);
      break;
    case 4:
      Execute_Parameter(vertex);
      break;
    case 5:
      Execute_Constant(vertex);
      break;
    case 6:
      Execute_Add(vertex);
      break;
    case 7:
      Execute_Subtract(vertex);
      break;
    case 8:
      LOG(INFO) << "ERROR: Execute_Reduce is not implemented\n";
      exit(1);
    case 9:
      Execute_Divide(vertex);
      break;
    case 10:
      Execute_ReduceWindow(vertex);
      break;
    case 11:
      Execute_Maximum(vertex);
      break;
    case 12:
      Execute_Reshape(vertex);
      break;
    case 13:
      LOG(INFO) << "ERROR: Execute_Convert is not implemented\n";
      exit(1);
    case 14:
      LOG(INFO) << "ERROR: Execute_Exponential is not implemented\n";
      exit(1);
    case 15:
      Execute_Tuple(vertex);
      break;
    case 16:
      Execute_GetTupleElement(vertex);
      break;
    default:
      LOG(INFO) << "ERROR: Found unknown node\n";
      exit(1);
  }
}

void* Execute::TraverseAndExecute() {
  Node* root_ = graph_->root;
  if (root_->IsVisited == graph_->global_epoch) Execute_Handle(root_);

  return root_->result_ptr;
}

void Execute::Execute_Convolution(Node* conv) {
  if (conv->IsVisited != graph_->global_epoch) return;

  while (conv->operand_index < conv->operands.size()) {
    Execute_Handle(conv->operands[conv->operand_index]->vertex);
    conv->operand_index++;
  }

  Node* lhs = conv->operands[0]->vertex;
  Node* rhs = conv->operands[1]->vertex;

  float* input = (float*)lhs->result_ptr;
  float* kernel = (float*)rhs->result_ptr;

  if (lhs->result_ptr == nullptr || rhs->result_ptr == nullptr) {
    LOG(INFO) << "ERROR: Result uncomputed as convolution operands are not "
                 "available!! ";
    exit(1);
  }

  int64 output_rows, output_cols, row_str, col_str, padding_top, padding_bot,
      padding_left, padding_right, lhs_row_dilation, lhs_col_dilation,
      rhs_row_dilation, rhs_col_dilation;

  output_rows = conv->node_type_.c.output_rows;
  output_cols = conv->node_type_.c.output_cols;
  row_str = conv->node_type_.c.row_stride;
  col_str = conv->node_type_.c.col_stride;
  padding_top = conv->node_type_.c.padding_top;
  padding_bot = conv->node_type_.c.padding_bottom;
  padding_left = conv->node_type_.c.padding_left;
  padding_right = conv->node_type_.c.padding_right;
  lhs_row_dilation = conv->node_type_.c.lhs_row_dilation;
  lhs_col_dilation = conv->node_type_.c.lhs_col_dilation;
  rhs_row_dilation = conv->node_type_.c.rhs_row_dilation;
  rhs_col_dilation = conv->node_type_.c.rhs_col_dilation;

  if (row_str == 0 || col_str == 0) {
    LOG(INFO) << "ERROR: Convolution Stride should be non zero";
    exit(1);
  }

  absl::Span<const int64> inp_dim = conv->node_type_.c.lhs_shape.dimensions;
  std::vector<int64> conv_dim;

  for (auto it = inp_dim.begin(); it != inp_dim.end(); ++it)
    conv_dim.push_back(*it);

  // New input dimensions after padding
  conv_dim[1] += (padding_top + padding_bot);     // conv_dim[1] --> H  (NHWC)
  conv_dim[2] += (padding_left + padding_right);  // conv_dim[2] --> W  (NHWC)

  uint32 num_elem = 1;
  for (auto it = conv_dim.begin(); it != conv_dim.end(); ++it)
    num_elem *= (*it);
  float* new_input = new float[num_elem];

  zero_pad(new_input, input, inp_dim, padding_top, padding_bot, padding_left,
           padding_right);

  absl::Span<const int64> inst_dim = conv->node_type_.c.conv_shape.dimensions;
  uint32 num_ele = 1;
  for (int i = 0; i < 4; i++) num_ele *= inst_dim[i];
  float* result = new float[num_ele];

  absl::Span<const int64> rhs_dim = conv->node_type_.c.rhs_shape.dimensions;
  std::vector<int64> kernel_dim;

  for (auto it = rhs_dim.begin(); it != rhs_dim.end(); ++it)
    kernel_dim.push_back(*it);

  float sum = 0;

  myconv2d(result, conv_dim, kernel_dim, new_input, kernel, inst_dim, row_str,
           col_str);

  conv->result_ptr = (void*)result;
  conv->result_size = num_ele * 4;

  if (conv->IsVisited == 0)
    conv->IsVisited = 1;
  else
    conv->IsVisited = 0;

  // Resetting operand_index for next execution
  conv->operand_index = 0;

  return;
}

void Execute::Execute_Parameter(Node* parameter) {
  if (parameter->IsVisited != graph_->global_epoch) return;

  while (parameter->operand_index < parameter->operands.size()) {
    Execute_Handle(parameter->operands[parameter->operand_index]->vertex);
    parameter->operand_index++;
  }

  if (parameter->node_type_.p.get_host_kernel_ptr() == nullptr) {
    LOG(INFO) << "ERROR: Parameter input not available!!";
    exit(1);
  }

  parameter->result_ptr = parameter->node_type_.p.get_host_kernel_ptr();
  parameter->result_size = parameter->node_type_.p.get_param_size();

  if (parameter->IsVisited == 0)
    parameter->IsVisited = 1;
  else
    parameter->IsVisited = 0;

  // Resetting operand_index for next execution
  parameter->operand_index = 0;
  return;
}

void Execute::Execute_Reshape(Node* reshape_) {
  if (reshape_->IsVisited != graph_->global_epoch) return;

  // Visit parents of this node (graph is upside down so child is parent in
  // actual graph)
  if (reshape_->operands.size() > 1) {
    LOG(INFO) << "ERROR:reshape number of operands > 1\n";
    exit(1);
  }

  while (reshape_->operand_index < reshape_->operands.size()) {
    Execute_Handle(reshape_->operands[reshape_->operand_index]->vertex);
    reshape_->operand_index++;
  }

  if (reshape_->operands[0]->vertex->result_ptr == nullptr) {
    LOG(INFO) << "ERROR: Reshape input not available!!";
    exit(1);
  }

  if ((reshape_->node_type_.reshape.reshape_shape.dimensions ==
       reshape_->node_type_.reshape.operand_shape.dimensions) &&
      (reshape_->node_type_.reshape.reshape_shape.layout ==
       reshape_->node_type_.reshape.operand_shape.layout)) {
    reshape_->result_ptr = reshape_->operands[0]->vertex->result_ptr;
    reshape_->result_size = reshape_->operands[0]->vertex->result_size;

    if (reshape_->IsVisited == 0)
      reshape_->IsVisited = 1;
    else
      reshape_->IsVisited = 0;

    // Resetting operand_index for next execution
    reshape_->operand_index = 0;
    return;
  } else {
    LOG(INFO) << "CRITICAL WARNING: We can only handle reshape when input and "
                 "output have same shape and layout (i.e. a NOP)";
    reshape_->result_ptr = reshape_->operands[0]->vertex->result_ptr;
    reshape_->result_size = reshape_->operands[0]->vertex->result_size;

    if (reshape_->IsVisited == 0)
      reshape_->IsVisited = 1;
    else
      reshape_->IsVisited = 0;

    // Resetting operand_index for next execution
    reshape_->operand_index = 0;
    return;
  }
}
bool layout_is_supported(absl::Span<const int64>& layout) {
  int64 i = layout.size();
  for (int64 dim : layout) {
    i--;
    if (dim != i) {
      return false;
    }
  }
  return true;
}
void Execute::Execute_Broadcast(Node* broadcast) {
  if (broadcast->IsVisited != graph_->global_epoch) return;

  while (broadcast->operand_index < broadcast->operands.size()) {
    Execute_Handle(broadcast->operands[broadcast->operand_index]->vertex);
    broadcast->operand_index++;
  }

  if (broadcast->operands[0]->vertex->result_ptr == nullptr) {
    LOG(INFO) << "ERROR: Broadcast input not available!!";
    exit(1);
    return;
  }
  absl::Span<const int64> dim =
      broadcast->node_type_.broadcast.operand_shape.dimensions;
  uint32 num_elem = 1;
  for (auto d : dim) {
    num_elem *= d;
  }

  if ((broadcast->node_type_.broadcast.broadcast_shape.dimensions ==
       broadcast->node_type_.broadcast.operand_shape.dimensions) &&
      (broadcast->node_type_.broadcast.broadcast_shape.layout ==
       broadcast->node_type_.broadcast.operand_shape.layout) &&
      (broadcast->operands[0]->vertex->result_size == (num_elem * 4))) {
    broadcast->result_ptr = broadcast->operands[0]->vertex->result_ptr;
    broadcast->result_size = broadcast->operands[0]->vertex->result_size;

    // result_buffer = static_cast<void*>(result);  //Just check if this is
    // required
    if (broadcast->IsVisited == 0)
      broadcast->IsVisited = 1;
    else
      broadcast->IsVisited = 0;

    // Resetting operand_index for next execution
    broadcast->operand_index = 0;
    return;
  }

  absl::Span<const int64> inst_layout =
      broadcast->node_type_.broadcast.broadcast_shape.layout;
  absl::Span<const int64> inst_dim =
      broadcast->node_type_.broadcast.broadcast_shape.dimensions;
  absl::Span<const int64> op_layout =
      broadcast->node_type_.broadcast.operand_shape.layout;
  absl::Span<const int64> op_dim =
      broadcast->node_type_.broadcast.operand_shape.dimensions;
  absl::Span<const int64> broadcast_dim =
      broadcast->node_type_.broadcast.dimensions;
  if (!layout_is_supported(
          inst_layout)) {  // we only support c-style layouts (ex:3,2,1,0)
    LOG(INFO) << "ERROR:We only support c-style layout";
    exit(1);
  }
  if (!layout_is_supported(
          op_layout)) {  // we only support c-style layouts (ex:3,2,1,0)
    LOG(INFO) << "ERROR:We only support c-style layout";
    exit(1);
  }
  if (inst_dim.size() > 4 || op_dim.size() > 4) {
    LOG(INFO) << "ERROR: We only support max of 4 dimensional tensors";
    exit(1);
  }
  if (broadcast_dim.size() != op_dim.size()) {
    LOG(INFO) << "ERROR: broadcast_dim.size not equal to op_dim.size";
    exit(1);
  }

  int dim_sel[4] = {4};
  int inp_dim[4] = {1};
  for (int i = 0; i < op_dim.size(); i++) {
    int j = i + 4 - op_dim.size();
    dim_sel[j] = broadcast_dim[i];
    if (broadcast_dim[i] > 3) {
      LOG(INFO) << "ERROR: We dont support tensors of dim >4";
      exit(1);
    }
  }
  for (int i = 0; i < op_dim.size(); i++) {
    int j = i + 4 - op_dim.size();
    inp_dim[j] = op_dim[i];
  }

  int N = inst_dim.size() >= 4 ? inst_dim[inst_dim.size() - 4] : 1;
  int H = inst_dim.size() >= 3 ? inst_dim[inst_dim.size() - 3] : 1;
  int W = inst_dim.size() >= 2 ? inst_dim[inst_dim.size() - 2] : 1;
  int C = inst_dim.size() >= 1 ? inst_dim[inst_dim.size() - 1] : 1;

  int outp_size = 1;
  for (int i = 0; i < inst_dim.size(); i++) {
    outp_size *= inst_dim[i];
  }
  float* result = new float[outp_size];
  float* operand =
      static_cast<float*>(broadcast->operands[0]->vertex->result_ptr);
  for (int n = 0; n < N; n++) {
    for (int h = 0; h < H; h++) {
      for (int w = 0; w < W; w++) {
        for (int c = 0; c < C; c++) {
          int out_iters[5] = {n, h, w, c, 0};
          result[n * (H * W * C) + h * (W * C) + w * (C) + c] =
              operand[out_iters[dim_sel[0]] *
                          (inp_dim[1] * inp_dim[2] * inp_dim[3]) +
                      out_iters[dim_sel[1]] * (inp_dim[2] * inp_dim[3]) +
                      out_iters[dim_sel[2]] * (inp_dim[3]) +
                      out_iters[dim_sel[3]]];
        }
      }
    }
  }

  broadcast->result_ptr =
      static_cast<void*>(result);  // Just check if this is required
  broadcast->result_size =
      outp_size * sizeof(float);  // size in bytes (outp_size is in floats)

  if (broadcast->IsVisited == 0)
    broadcast->IsVisited = 1;
  else
    broadcast->IsVisited = 0;

  // Resetting operand_index for next execution
  broadcast->operand_index = 0;
}

void Execute::Execute_Add(Node* Add) {
  if (Add->IsVisited != graph_->global_epoch) return;

  while (Add->operand_index < Add->operands.size()) {
    Execute_Handle(Add->operands[Add->operand_index]->vertex);
    Add->operand_index++;
  }

  if (Add->operands[0]->vertex->result_ptr == nullptr ||
      Add->operands[1]->vertex->result_ptr == nullptr) {
    LOG(INFO) << "ERROR: Result from previous nodes not available";
    exit(1);
  }

  absl::Span<const int64> bin_dim = Add->node_type_.bin.bin_shape.dimensions;
  absl::Span<const int64> bin_layout = Add->node_type_.bin.bin_shape.layout;
  std::vector<int64> inst_cond;

  int i = 0;
  for (auto it = bin_layout.rbegin(); it != bin_layout.rend(); ++it, i++)
    inst_cond.push_back(bin_dim[(*it)]);

  while (i < 4) {
    inst_cond.push_back(1);
    i++;
  }

  int64 num_ele = 1;

  for (i = 0; i < bin_dim.size(); i++) num_ele *= bin_dim[i];

  float* lhs_ptr = static_cast<float*>(Add->operands[0]->vertex->result_ptr);
  float* rhs_ptr = static_cast<float*>(Add->operands[1]->vertex->result_ptr);
  uint32 size = num_ele * 4;
  float* result = (float*)new char[size];

  for (int i = 0; i < inst_cond[0]; i++) {
    for (int j = 0; j < inst_cond[1]; j++) {
      for (int k = 0; k < inst_cond[2]; k++) {
        for (int l = 0; l < inst_cond[3]; l++) {
          *(result + (i * inst_cond[1] * inst_cond[2] * inst_cond[3]) +
            (j * inst_cond[2] * inst_cond[3]) + (k * inst_cond[3]) + l) =
              *(lhs_ptr + (i * inst_cond[1] * inst_cond[2] * inst_cond[3]) +
                (j * inst_cond[2] * inst_cond[3]) + (k * inst_cond[3]) + l) +
              *(rhs_ptr + (i * inst_cond[1] * inst_cond[2] * inst_cond[3]) +
                (j * inst_cond[2] * inst_cond[3]) + (k * inst_cond[3]) + l);
        }
      }
    }
  }

  Add->result_ptr = static_cast<void*>(result);
  Add->result_size = size;

  if (Add->IsVisited == 0)
    Add->IsVisited = 1;
  else
    Add->IsVisited = 0;

  // Resetting operand_index for next execution
  Add->operand_index = 0;
}

void Execute::Execute_Subtract(Node* sub) {
  if (sub->IsVisited != graph_->global_epoch) return;

  while (sub->operand_index < sub->operands.size()) {
    Execute_Handle(sub->operands[sub->operand_index]->vertex);
    sub->operand_index++;
  }

  if (sub->operands[0]->vertex->result_ptr == nullptr ||
      sub->operands[1]->vertex->result_ptr == nullptr) {
    LOG(INFO) << "ERROR: Result from previous nodes not available";
    exit(1);
  }

  absl::Span<const int64> bin_dim = sub->node_type_.bin.bin_shape.dimensions;
  absl::Span<const int64> bin_layout = sub->node_type_.bin.bin_shape.layout;
  std::vector<int64> inst_cond;

  int i = 0;
  for (auto it = bin_layout.rbegin(); it != bin_layout.rend(); ++it, i++)
    inst_cond.push_back(bin_dim[(*it)]);

  while (i < 4) {
    inst_cond.push_back(1);
    i++;
  }

  int64 num_ele = 1;

  for (i = 0; i < bin_dim.size(); i++) num_ele *= bin_dim[i];

  float* lhs_ptr = static_cast<float*>(sub->operands[0]->vertex->result_ptr);
  float* rhs_ptr = static_cast<float*>(sub->operands[1]->vertex->result_ptr);
  uint32 size = (num_ele * 4);
  float* result = (float*)new char[size];

  for (int i = 0; i < inst_cond[0]; i++) {
    for (int j = 0; j < inst_cond[1]; j++) {
      for (int k = 0; k < inst_cond[2]; k++) {
        for (int l = 0; l < inst_cond[3]; l++) {
          *(result + (i * inst_cond[1] * inst_cond[2] * inst_cond[3]) +
            (j * inst_cond[2] * inst_cond[3]) + (k * inst_cond[3]) + l) =
              *(lhs_ptr + (i * inst_cond[1] * inst_cond[2] * inst_cond[3]) +
                (j * inst_cond[2] * inst_cond[3]) + (k * inst_cond[3]) + l) -
              *(rhs_ptr + (i * inst_cond[1] * inst_cond[2] * inst_cond[3]) +
                (j * inst_cond[2] * inst_cond[3]) + (k * inst_cond[3]) + l);
        }
      }
    }
  }

  sub->result_ptr = static_cast<void*>(result);
  sub->result_size = size;

  float* res = static_cast<float*>(sub->result_ptr);

  for (int i = 0; i < 20; i++, res++) {
    std::cout << *res << " ";
    std::cout.flush();
  }

  if (sub->IsVisited == 0)
    sub->IsVisited = 1;
  else
    sub->IsVisited = 0;

  // Resetting operand_index for next execution
  sub->operand_index = 0;
}

void Execute::Execute_Divide(Node* divide) {
  if (divide->IsVisited != graph_->global_epoch) return;

  while (divide->operand_index < divide->operands.size()) {
    Execute_Handle(divide->operands[divide->operand_index]->vertex);
    divide->operand_index++;
  }

  if (divide->IsVisited == 0)
    divide->IsVisited = 1;
  else
    divide->IsVisited = 0;

  // Resetting operand_index for next execution
  divide->operand_index = 0;
}

void Execute::Execute_ReduceWindow(Node* reduce_window) {
  if (reduce_window->IsVisited != graph_->global_epoch) return;

  while (reduce_window->operand_index < reduce_window->operands.size()) {
    Execute_Handle(
        reduce_window->operands[reduce_window->operand_index]->vertex);
    reduce_window->operand_index++;
  }

  if (reduce_window->operands[0]->vertex->result_ptr == nullptr) {
    LOG(INFO) << "ERROR: Result from previous nodes not available";
    exit(1);
  }

  std::vector<int> window_size = reduce_window->node_type_.reducewindow.size;
  std::vector<int> stride = reduce_window->node_type_.reducewindow.stride;
  std::vector<int> padding_low =
      reduce_window->node_type_.reducewindow.padding_low;
  std::vector<int> padding_high =
      reduce_window->node_type_.reducewindow.padding_high;
  absl::Span<const int64> dimensions;

  float* init_value = nullptr;

  float* input = (float*)reduce_window->operands[0]->vertex->result_ptr;

  absl::Span<const int64> inp_dim =
      reduce_window->node_type_.reducewindow.lhs_shape.dimensions;
  absl::Span<const int64> inst_dim =
      reduce_window->node_type_.reducewindow.reducewindow_shape.dimensions;
  std::vector<int64> redwin_dim;

  for (auto it = inp_dim.begin(); it != inp_dim.end(); ++it)
    redwin_dim.push_back(*it);

  uint32 num_ele = 1, result_size;
  for (auto it = inst_dim.begin(); it != inst_dim.end(); ++it) num_ele *= (*it);

  result_size = (num_ele * 4);
  float* result = (float*)new char[result_size];

  // New input dimensions after padding
  for (int i = 0; i < 4; i++) {
    redwin_dim[i] += (padding_high[i] + padding_low[i]);
  }

  uint32 num_elem = 1;
  for (auto it = redwin_dim.begin(); it != redwin_dim.end(); ++it) {
    num_elem *= (*it);
  }

  uint32 size = (num_elem * 4);
  float* new_input = (float*)new char[size];

  int count = 0;
  if (reduce_window->node_type_.reducewindow.subcomputation_opcode ==
          _maximum_) {
    dimensions = reduce_window->node_type_.reducewindow.rhs_shape.dimensions;
    Node* rhs = reduce_window->operands[1]->vertex;
    if (dimensions.empty()) init_value = (float*)rhs->result_ptr;

    int64 i, ii, j, jj, k, kk, l, ll, x, y, w, z, flag = 0;
    // Adding Padding to the input and storing as a new input
    for (i = 0; i < redwin_dim[0]; i++) {
      for (j = 0, x = 0; j < redwin_dim[1]; j++) {
        for (k = 0, y = 0; k < redwin_dim[2]; k++) {
          for (l = 0; l < redwin_dim[3]; l++) {
            if ((j < padding_high[1]) ||
                (j >= (redwin_dim[1] - padding_low[1]))) {
              *(new_input +
                (i * redwin_dim[1] * redwin_dim[2] * redwin_dim[3]) +
                (j * redwin_dim[2] * redwin_dim[3]) + (k * redwin_dim[3]) + l) =
                  *init_value;
            }

            else if ((k < padding_low[2]) ||
                     (k >= (redwin_dim[2] - padding_high[2]))) {
              *(new_input +
                (i * redwin_dim[1] * redwin_dim[2] * redwin_dim[3]) +
                (j * redwin_dim[2] * redwin_dim[3]) + (k * redwin_dim[3]) + l) =
                  *init_value;
            }

            else {
              *(new_input +
                (i * redwin_dim[1] * redwin_dim[2] * redwin_dim[3]) +
                (j * redwin_dim[2] * redwin_dim[3]) + (k * redwin_dim[3]) + l) =
                  *(input + (i * inp_dim[1] * inp_dim[2] * inp_dim[3]) +
                    (x * inp_dim[2] * inp_dim[3]) + (y * inp_dim[3]) + l);
              flag = 1;
            }
          }
          if (flag == 1) {
            y++;
            if (y >= inp_dim[2]) {
              y = 0;
              x++;
            }
            if (x >= inp_dim[1]) x = 0;
            flag = 0;
          }
        }
      }
    }

    float maximum = *init_value;
    for (i = 0, ii = 0; i < redwin_dim[0]; i = (i + stride[0]), ii++) {
      for (l = 0, ll = 0; l < redwin_dim[3]; l = (l + stride[3]), ll++) {
        for (j = 0, jj = 0; j < redwin_dim[1]; j = (j + stride[1]), jj++) {
          for (k = 0, kk = 0; k < redwin_dim[2]; k = (k + stride[2]), kk++) {
            for (x = 0; x < window_size[0]; x++) {
              for (w = 0; w < window_size[3]; w++) {
                for (y = 0; y < window_size[1]; y++) {
                  for (z = 0; z < window_size[2]; z++) {
                    maximum = std::fmax(
                        maximum,
                        (*(new_input + ((i + x) * redwin_dim[1] *
                                        redwin_dim[2] * redwin_dim[3]) +
                           ((j + y) * redwin_dim[2] * redwin_dim[3]) +
                           ((k + z) * redwin_dim[3]) + l + w)));
                  }
                }
              }
            }
            (*(result + (ii * inst_dim[1] * inst_dim[2] * inst_dim[3]) +
               (jj * inst_dim[2] * inst_dim[3]) + (kk * inst_dim[3]) + ll)) =
                maximum;
            maximum = *init_value;
            count++;
          }
        }
      }
    }
  }

  else {
    LOG(INFO) << "ERROR: We dont support any type of reduce-window other than "
                 "subcomputation=max(i.e. maxpooling)";
    exit(1);
  }

  reduce_window->result_ptr = static_cast<void*>(result);
  reduce_window->result_size = result_size;

  if (reduce_window->IsVisited == 0)
    reduce_window->IsVisited = 1;
  else
    reduce_window->IsVisited = 0;

  // Resetting operand_index for next execution
  reduce_window->operand_index = 0;
}

void Execute::Execute_Maximum(Node* maximum) {
  if (maximum->IsVisited != graph_->global_epoch) return;

  while (maximum->operand_index < maximum->operands.size()) {
    Execute_Handle(maximum->operands[maximum->operand_index]->vertex);
    maximum->operand_index++;
  }

  if (maximum->operands[0]->vertex->result_ptr == nullptr ||
      maximum->operands[1]->vertex->result_ptr == nullptr) {
    LOG(INFO) << "ERROR: Result from previous nodes not available";
    exit(1);
  }

  absl::Span<const int64> bin_dim =
      maximum->node_type_.bin.bin_shape.dimensions;
  absl::Span<const int64> bin_layout = maximum->node_type_.bin.bin_shape.layout;
  std::vector<int64> inst_cond;

  int i = 0;
  for (auto it = bin_layout.rbegin(); it != bin_layout.rend(); ++it, i++)
    inst_cond.push_back(bin_dim[(*it)]);

  while (i < 4) {
    inst_cond.push_back(1);
    i++;
  }

  int64 num_ele = 1;
  for (i = 0; i < bin_dim.size(); i++) num_ele *= bin_dim[i];

  float* lhs_ptr =
      static_cast<float*>(maximum->operands[0]->vertex->result_ptr);
  float* rhs_ptr =
      static_cast<float*>(maximum->operands[1]->vertex->result_ptr);
  uint32 size = (num_ele * 4);
  float* result = (float*)new char[size];
  if (!result) {
    LOG(INFO) << "ERROR: Out of memory error";
    exit(1);
  }

  for (int i = 0; i < inst_cond[0]; i++) {
    for (int j = 0; j < inst_cond[1]; j++) {
      for (int k = 0; k < inst_cond[2]; k++) {
        for (int l = 0; l < inst_cond[3]; l++) {
          *(result + (i * inst_cond[1] * inst_cond[2] * inst_cond[3]) +
            (j * inst_cond[2] * inst_cond[3]) + (k * inst_cond[3]) + l) =
              std::fmax((*(lhs_ptr +
                           (i * inst_cond[1] * inst_cond[2] * inst_cond[3]) +
                           (j * inst_cond[2] * inst_cond[3]) +
                           (k * inst_cond[3]) + l)),
                        (*(rhs_ptr +
                           (i * inst_cond[1] * inst_cond[2] * inst_cond[3]) +
                           (j * inst_cond[2] * inst_cond[3]) +
                           (k * inst_cond[3]) + l)));
        }
      }
    }
  }

  maximum->result_ptr = static_cast<void*>(result);
  maximum->result_size = size;

  if (maximum->IsVisited == 0)
    maximum->IsVisited = 1;
  else
    maximum->IsVisited = 0;

  // Resetting operand_index for next execution
  maximum->operand_index = 0;
}

void Execute::Execute_Constant(Node* const_) {
  if (const_->IsVisited != graph_->global_epoch) return;

  while (const_->operand_index < const_->operands.size()) {
    Execute_Handle(const_->operands[const_->operand_index]->vertex);
    const_->operand_index++;
  }

  if (const_->node_type_.cons.ptr == nullptr) {
    LOG(INFO) << "ERROR: Constant points to NULL";
    exit(1);
  }

  const_->result_ptr = (void*)const_->node_type_.cons.ptr;
  const_->result_size = const_->node_type_.cons.size;

  if (const_->IsVisited == 0)
    const_->IsVisited = 1;
  else
    const_->IsVisited = 0;

  // Resetting operand_index for next execution
  const_->operand_index = 0;
}

void Execute::Execute_Tuple(Node* Tuple_) {
  if (Tuple_->IsVisited != graph_->global_epoch) return;

  while (Tuple_->operand_index < Tuple_->operands.size()) {
    Execute_Handle(Tuple_->operands[Tuple_->operand_index]->vertex);
    Tuple_->operand_index++;
  }

  for (unsigned int i = 0; i < Tuple_->operands.size(); i++) {
    if (Tuple_->operands[i]->vertex->result_ptr == nullptr) {
      LOG(INFO) << "ERROR :Result is not available from operand "
                << Tuple_->operands[i]->vertex->tag;
      exit(1);
    }
  }

  if (Tuple_->IsVisited == 0)
    Tuple_->IsVisited = 1;
  else
    Tuple_->IsVisited = 0;

  // Resetting operand_index for next execution
  Tuple_->operand_index = 0;
}

void Execute::Execute_GetTupleElement(Node* getTuple) {
  if (getTuple->IsVisited != graph_->global_epoch) return;

  while (getTuple->operand_index < getTuple->operands.size()) {
    Execute_Handle(getTuple->operands[getTuple->operand_index]->vertex);
    getTuple->operand_index++;
  }

  getTuple->result_ptr =
      getTuple->operands[0]->vertex->operands[0]->vertex->result_ptr;
  getTuple->result_size =
      getTuple->operands[0]->vertex->operands[0]->vertex->result_size;

  if (getTuple->IsVisited == 0)
    getTuple->IsVisited = 1;
  else
    getTuple->IsVisited = 0;

  // Resetting operand_index for next execution
  getTuple->operand_index = 0;
}

}  // fpga namespace
}  // xla namespace
