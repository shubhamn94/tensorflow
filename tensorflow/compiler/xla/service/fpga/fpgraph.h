/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef TENSORFLOW_COMPILER_XLA_SERVICE_FPGA_FPGRAPH_H_
#define TENSORFLOW_COMPILER_XLA_SERVICE_FPGA_FPGRAPH_H_

#include <iostream>
#include <vector>
#include "absl/container/inlined_vector.h"
#include "absl/types/span.h"
#include "tensorflow/compiler/xla/service/hlo_module.h"
#include "tensorflow/compiler/xla/status.h"
#include "tensorflow/compiler/xla/types.h"
#include "tensorflow/compiler/xla/xla_data.pb.h"
#include "tensorflow/core/platform/logging.h"

namespace xla {
namespace fpga {

class Graph {
 public:
  class Node* root;
  Graph() { global_epoch = 0; }
  std::vector<int> queue;
  bool search_queue(int);
  int64 param_count = 0;
  int64 num_parameters();
  absl::flat_hash_map<int64, Node*> constant_to_node_map;
  absl::flat_hash_map<int64, Node*> parameter_to_node_map;
  uint32 global_epoch;
};

class Edge {
 public:
  class Node* vertex;
  Edge* next;
  uint32 priority_tag;
};

class Instr_Shape {
 public:
  absl::Span<const int64> dimensions;
  absl::Span<const int64> layout;
  PrimitiveType element_type;
};

class Operand_Shape {
 public:
  absl::Span<const int64> dimensions;
  absl::Span<const int64> layout;
  PrimitiveType element_type;

  Operand_Shape() {
    /*std::vector<int64> v(2,10);
    element_type = F16;
    dimensions = absl::Span<const int64>(v);
    layout = absl::Span<const int64>(v);*/
  }
};

class Root {
 public:
  Instr_Shape shape;
  Operand_Shape operand_shape;
};

class convolution {
 public:
  // input tensor
  int64 input_batch, input_rows, input_cols, input_channels;

  // kernel tensor
  int64 kernel_rows, kernel_cols, kernel_channels, kernel_filters;

  // output tensor
  int64 output_rows, output_cols;

  // window info
  int64 row_stride, col_stride;

  // padding info
  int64 padding_top, padding_bottom, padding_left, padding_right;

  // LHS-RHS Dilations
  int64 lhs_row_dilation, lhs_col_dilation, rhs_row_dilation, rhs_col_dilation;

  // Shape of convolution inclassion and it's operands
  Instr_Shape conv_shape;
  Operand_Shape lhs_shape;
  Operand_Shape rhs_shape;

  convolution() {}
  ~convolution() {}
};

class Tuple {
 public:
  std::vector<Operand_Shape> operand_shape;
  std::vector<void*> operand_result;
  std::vector<int64> operand_size;
  Tuple() {}
  ~Tuple() {}
};

class get_tuple_element {
 public:
  int64 index;
  Operand_Shape element_shape;

  get_tuple_element() { index = 0; }
};

class Broadcast {
 public:
  std::vector<int64> dimensions;
  Instr_Shape broadcast_shape;
  Operand_Shape operand_shape;
};

class Reshape {
 public:
  Instr_Shape reshape_shape;
  Operand_Shape operand_shape;
};

class constant {
 public:
  const void* ptr;
  PrimitiveType pointer_type;
  absl::Span<const int64> dimensions;
  absl::Span<const int64> minor_to_major;
  int64 element_count;
  int64 size;  // bytes
};

class dot {
 public:
  absl::Span<const int64> lhs_contr_dim;
  absl::Span<const int64> rhs_contr_dim;
  absl::Span<const int64> lhs_batch_dim;
  absl::Span<const int64> rhs_batch_dim;

  // Shape of convolution inclassion and it's operands
  Instr_Shape dot_shape;
  Operand_Shape lhs_shape;
  Operand_Shape rhs_shape;
};

class ElementwiseBinary {
 public:
  Instr_Shape bin_shape;
  Operand_Shape lhs_shape;
  Operand_Shape rhs_shape;
};

enum tags {
  _root_,
  kconv,
  _dot_,
  _broadcast_,
  _param_,
  kcons,
  add,
  subtract,
  reduce,
  divide,
  reduce_window_,
  _maximum_,
  _reshape_,
  convert,
  exponential,
  _tuple_,
  get_tuple_ele,
  _default_
};
typedef enum tags tags;

class Reduce {
 public:
  int64 input_count;
  absl::Span<const int64> dimensions_to_reduce;
  tags subcomputation_opcode;

  std::vector<const void*> init_value_ptr;
  std::vector<PrimitiveType> init_value_type;

  std::vector<Operand_Shape> input_shape;
  std::vector<Operand_Shape> init_shape;
  Instr_Shape reduce_shape;
  Node* sub_root_node;
  Reduce();
};

class ReduceWindow {
 public:
  Instr_Shape reducewindow_shape;
  Operand_Shape lhs_shape, rhs_shape;

  tags subcomputation_opcode;

  Node* sub_root_node;
  std::vector<int> size;
  std::vector<int> stride;
  std::vector<int> padding_low;
  std::vector<int> padding_high;
};

class Parameter {
  uint64 param_size;
  void* host_kernel_ptr;
  void* fpga_kernel_ptr;

 public:
  int64 param_number;
  // Kernel* kernel = new Kernel;
  void* get_host_kernel_ptr();
  void set_host_kernel_ptr(void*);
  void* get_fpga_kernel_ptr();
  uint64 get_param_size();
  void set_param_size(uint64);
  Instr_Shape param_shape;
  // Kernel* get_kernel();

  Parameter() {
    param_number = 0;
    param_size = 0;
    host_kernel_ptr = nullptr;
    fpga_kernel_ptr = nullptr;
  }

  ~Parameter() {
    if (host_kernel_ptr != nullptr)
      delete[] static_cast<char*>(host_kernel_ptr);

    if (fpga_kernel_ptr != nullptr)
      delete[] static_cast<char*>(fpga_kernel_ptr);
  }
};

class Default {
 public:
  Instr_Shape unimplemented_shape;
  std::vector<Operand_Shape> operand_shape;
  int64 operand_count;
  string opcode;

  Default() { operand_count = 0; }
};

class Stack {
 public:
  Edge* edge = nullptr;
  Stack* link = nullptr;
};

union node_types {
  Root root_node;
  convolution c;
  dot d;
  Parameter p;
  constant cons;
  ElementwiseBinary bin;
  Broadcast broadcast;
  Reshape reshape;
  Tuple tuple_t;
  get_tuple_element get_tuple_elem;
  Reduce reduce;
  ReduceWindow reducewindow;
  Default unimplemented;
  node_types() { memset(this, 0, sizeof(*this)); }
  ~node_types() {}
};
typedef union node_types node_types;

class Node {
 public:
  Node() {
    uniqueId = 0;
    operand_index = 0;
    IsVisited = 0;
    result_ptr = nullptr;
    result_size = 0;
  }
  ~Node() {}

  int uniqueId;
  tags tag;
  node_types node_type_;
  Edge* child = nullptr;
  absl::InlinedVector<Edge*, 2> operands;
  uint32 operand_index;
  uint32 IsVisited;
  void* result_ptr;
  uint32 result_size;
};

class FPGraph {
 public:
  FPGraph() { LOG(INFO) << "\nCreated Graph object!!"; }
  ~FPGraph();

  Graph* get_graph() { return graph; }

  Stack* top = nullptr;

  virtual Status CreateGraph(const HloModule&) = 0;

  void AddGraphVertex(int, Node*, Graph*);

  std::vector<int> delete_graph_components(Graph*);

  Node* DFSGraphTraverse(int, Graph*);

  void DFSGraphTraverse(Graph*);

  // Get Graph Vertex from parameter number
  Node* GetGraphVertex(const HloModule&, Graph*, int64 param_number);

  void DisplayGraph();

  Edge* AddGraphEdge(Node*, Node*, uint32);

  void push(class Edge*);
  Edge* pop(void);
  void stack_clear();

 private:
  Graph* graph = new Graph;

  Edge* AddGraphEdge(int, Graph*);
  void AddGraphEdge(int, int, Graph*);
};

}  // namespace fpga
}  // namespace xla

#endif  // TENSORFLOW_COMPILER_XLA_SERVICE_FPGA_FPGRAPH_H_
