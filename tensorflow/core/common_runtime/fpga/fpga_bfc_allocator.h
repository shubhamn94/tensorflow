/* Copyright 2015 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef TENSORFLOW_CORE_COMMON_RUNTIME_GPU_GPU_BFC_ALLOCATOR_H_
#define TENSORFLOW_CORE_COMMON_RUNTIME_GPU_GPU_BFC_ALLOCATOR_H_

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>
#include <stdlib.h>

#include "tensorflow/core/common_runtime/allocator_retry.h"
#include "tensorflow/core/common_runtime/bfc_allocator.h"
#include "tensorflow/core/common_runtime/gpu/gpu_id.h"
#include "tensorflow/core/platform/stream_executor.h"
#include "tensorflow/core/platform/thread_annotations.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/core/protobuf/config.pb.h"

namespace tensorflow {

// Suballocator for GPU memory.
class FPGAMemAllocator : public SubAllocator {
 public:
  // 'platform_gpu_id' refers to the ID of the GPU device within
  // the process and must reference a valid ID in the process.
  // Note: stream_exec cannot be null.
  explicit FPGAMemAllocator(se::StreamExecutor* stream_exec,
                           PlatformFPGAId fpga_id, bool use_unified_memory,
                           const std::vector<Visitor>& alloc_visitors,
                           const std::vector<Visitor>& free_visitors)
      : SubAllocator(alloc_visitors, free_visitors),
        stream_exec_(stream_exec),
        fpga_id_(fpga_id),
        use_unified_memory_(use_unified_memory) {
    CHECK(stream_exec_ != nullptr);
  }
  ~FPGAMemAllocator() override {}

    void* Alloc(size_t alignment, size_t num_bytes) override {
    void* ptr = nullptr;
    if (num_bytes > 0) {
      if (use_unified_memory_) {
        ptr = malloc(sizeof(size_t)*num_bytes);
	//ptr = stream_exec_->UnifiedMemoryAllocate(num_bytes);
      } else {
        ptr = malloc(sizeof(size_t)*num_bytes);
	//ptr = stream_exec_->AllocateArray<char>(num_bytes).opaque();
      }
      VisitAlloc(ptr, fpga_id_.value(), num_bytes);
    }
    return ptr;
  }

  void Free(void* ptr, size_t num_bytes) override {
    if (ptr != nullptr) {
      VisitFree(ptr, fpga_id_.value(), num_bytes);
      if (use_unified_memory_) {
       free (ptr);
	// stream_exec_->UnifiedMemoryDeallocate(ptr);
      } else {
        se::DeviceMemoryBase fpga_ptr(ptr);
        free (ptr);
	//stream_exec_->Deallocate(&fpga_ptr);
      }
    }
  }

 private:
  se::StreamExecutor* stream_exec_;  // not owned, non-null
  const PlatformGpuId fpga_id_;
  const bool use_unified_memory_ = false;

  TF_DISALLOW_COPY_AND_ASSIGN(FPGAMemAllocator);
};

// A GPU memory allocator that implements a 'best-fit with coalescing'
// algorithm.
class FPGABFCAllocator : public BFCAllocator {
 public:
  FPGABFCAllocator(FPGAMemAllocator* sub_allocator, size_t total_memory,
                  const string& name);
  FPGABFCAllocator(FPGAMemAllocator* sub_allocator, size_t total_memory,
                  const GPUOptions& gpu_options, const string& name);
  ~FPGABFCAllocator() override {}

  TF_DISALLOW_COPY_AND_ASSIGN(FPGABFCAllocator);

 private:
  static bool GetAllowGrowthValue(const GPUOptions& gpu_options);
};

}  // namespace tensorflow

#endif  // TENSORFLOW_CORE_COMMON_RUNTIME_FPGA_FPGA_BFC_ALLOCATOR_H_
