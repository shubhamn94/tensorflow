/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

// TODO(opensource): Use a more generic sounding preprocessor name than
// GOOGLE_FPDA
#if GOOGLE_FPDA

#define EIGEN_USE_FPGA

#include "tensorflow/core/common_runtime/fpga/fpga_device.h"

#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <list>
#include <map>
#include <tuple>
#include <vector>

#include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"
#include "tensorflow/core/common_runtime/device_factory.h"
#include "tensorflow/core/common_runtime/fpga/fpga_event_mgr.h"
#include "tensorflow/core/common_runtime/fpga/fpga_id.h"
#include "tensorflow/core/common_runtime/fpga/fpga_id_manager.h"
#include "tensorflow/core/common_runtime/fpga/fpga_id_utils.h"
#include "tensorflow/core/common_runtime/fpga/fpga_init.h"
#include "tensorflow/core/common_runtime/fpga/fpga_process_state.h"
#include "tensorflow/core/common_runtime/fpga/fpga_stream_util.h"
#include "tensorflow/core/common_runtime/fpga/fpga_util.h"
#include "tensorflow/core/common_runtime/fpga_device_context.h"
#include "tensorflow/core/common_runtime/local_device.h"
#include "tensorflow/core/framework/allocator.h"
#include "tensorflow/core/framework/device_base.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/framework/tensor.pb.h"
#include "tensorflow/core/framework/types.h"
#include "tensorflow/core/framework/variant_op_registry.h"
#include "tensorflow/core/graph/types.h"
#include "tensorflow/core/lib/core/errors.h"
#include "tensorflow/core/lib/core/status.h"
#include "tensorflow/core/lib/gtl/stl_util.h"
#include "tensorflow/core/lib/strings/numbers.h"
#include "tensorflow/core/lib/strings/str_util.h"
#include "tensorflow/core/lib/strings/strcat.h"
#include "tensorflow/core/platform/fpda.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/platform/macros.h"
#include "tensorflow/core/platform/stream_executor.h"
#include "tensorflow/core/platform/tracing.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/core/public/session_options.h"
#include "tensorflow/core/util/device_name_utils.h"
#include "tensorflow/core/util/env_var.h"
#include "tensorflow/core/util/stream_executor_util.h"

#if !defined(PLATFORM_GOOGLE)
#include "fpda/fpda_config.h"
#endif

namespace tensorflow {

// Eigen Ops directly allocate memory only for temporary buffers used
// during OpKernel::Compute().  The recommended way of allocating such
// memory is via OpKernelContext::allocate_temp().  However, Eigen Ops
// don't have access to OpKernelContext, instead they get access to
// memory directly through the device allocator.  As an Open Source
// project, Eigen assumes allocator semantics similar to those of the
// FPDA memory allocator, and may not work correctly due to race
// conditions if used with some other allocator.  For safety, we need
// to delay deallocation calls out of Eigen until all events on the
// corresponding stream have completed.  The following two classes
// serve this purpose in two different compilation environments.

class EigenFpdaStreamDevice : public ::Eigen::StreamInterface {
 public:
  EigenFpdaStreamDevice()
      : scratch_(nullptr), semaphore_(nullptr), context_(nullptr) {
    Eigen::initializeDeviceProp();
  }
  ~EigenFpdaStreamDevice() override {}
  void Reinitialize(OpKernelContext* context, const fpdaStream_t* fpda_stream,
                    TfFpgaId tf_fpga_id, ::tensorflow::Allocator* alloc,
                    char* scratch) {
    if (LogMemory::IsEnabled()) {
      operation_ = context->op_kernel().name() + "/EigenAllocator";
      step_id_ = context->step_id();
    }
    context_ = context;
    scratch_ = scratch;
    semaphore_ =
        reinterpret_cast<unsigned int*>(scratch + Eigen::kFpdaScratchSize);
    stream_ = fpda_stream;
    allocator_ = alloc;
    PlatformFpgaId platform_fpga_id;
    TF_CHECK_OK(FpgaIdManager::TfToPlatformFpgaId(tf_fpga_id, &platform_fpga_id));
    device_prop_ = &Eigen::m_deviceProperties[platform_fpga_id.value()];
  }

  const fpdaStream_t& stream() const override { return *stream_; }
  const fpdaDeviceProp& deviceProperties() const override {
    return *device_prop_;
  }

  void* allocate(size_t num_bytes) const override {
    void* ret = allocator_->AllocateRaw(32 /* alignment */, num_bytes);
    if (ret == nullptr) {
      if (context_) {
        context_->SetStatus(errors::ResourceExhausted(
            strings::StrCat("Ran out of FPGA memory when allocating ", num_bytes,
                            " bytes for ", operation_)));
      } else {
        LOG(FATAL)
            << "EigenAllocator for FPGA ran out of memory when allocating "
            << num_bytes << ". See error logs for more detailed info.";
      }
    }
    if (LogMemory::IsEnabled() && ret != nullptr) {
      LogMemory::RecordRawAllocation(operation_, step_id_, num_bytes, ret,
                                     allocator_);
    }
    return ret;
  }
  void deallocate(void* buffer) const override {
    if (LogMemory::IsEnabled() && buffer != nullptr) {
      LogMemory::RecordRawDeallocation(operation_, step_id_, buffer, allocator_,
                                       true);
    }
    AsyncFreeData* afData =
        new AsyncFreeData(allocator_, buffer, operation_, step_id_);
    fpdaError_t err = fpdaStreamAddCallback(*stream_, asyncFree, afData, 0);
    CHECK_EQ(err, fpdaSuccess);
  }

  // Return a pointer to a per stream scratchpad of 1024 bytes residing
  // in global memory.
  void* scratchpad() const override { return scratch_; }

  // Return a semaphore. The semaphore is initially initialized to 0, and
  // each kernel using it is responsible for resetting to 0 upon completion
  // to maintain the invariant that the semaphore is always equal to 0 upon
  // each kernel start.
  unsigned int* semaphore() const override { return semaphore_; }

 private:
  struct AsyncFreeData {
    AsyncFreeData(::tensorflow::Allocator* a, void* p, const string& o,
                  const int64 s)
        : allocator_(a), address_(p), operation_(o), step_id_(s) {}
    ::tensorflow::Allocator* allocator_;
    void* address_;
    const string operation_;
    const int64 step_id_;
  };

  static void FPDART_CB asyncFree(fpdaStream_t stream, fpdaError_t status,
                                  void* userData) {
    AsyncFreeData* data = static_cast<AsyncFreeData*>(userData);
    if (LogMemory::IsEnabled()) {
      LogMemory::RecordRawDeallocation(data->operation_, data->step_id_,
                                       data->address_, data->allocator_, false);
    }
    data->allocator_->DeallocateRaw(data->address_);
    delete data;
  }

  string operation_;
  int64 step_id_;
  const fpdaStream_t* stream_;          // Not owned.
  const fpdaDeviceProp* device_prop_;   // Not owned.
  ::tensorflow::Allocator* allocator_;  // Not owned.
  mutable char* scratch_;
  mutable unsigned int* semaphore_;
  OpKernelContext* context_;

  TF_DISALLOW_COPY_AND_ASSIGN(EigenFpdaStreamDevice);
};

// This factory helps to ensure that different FPGA device objects that refer to
// the same physical device and stream group id use the same stream group
// object (and therefore the same FPDA streams). This is necessary since there
// is a single memory allocator per device (see ProcessState::GetFPGAAllocator)
// and allocators must not be shared across streams.
class BaseFPGADevice::StreamGroupFactory {
 public:
  // Returns the unique stream group for use with the stream defined by
  // {tf_fpga_id, stream_group_within_fpga}, creating it if it does not yet
  // exist.
  // This function is thread safe.
  BaseFPGADevice::StreamGroup* GetOrCreate(TfFpgaId tf_fpga_id,
                                          int stream_group_within_fpga,
                                          se::StreamExecutor* executor,
                                          const FPGAOptions& options) {
    mutex_lock guard(lock_);
    StreamGroup* group =
        &streams_[key_type(tf_fpga_id.value(), stream_group_within_fpga)];
    if (!group->compute) {
      group->compute = new se::Stream(executor);
      group->compute->Init();
      VLOG(2) << "Created stream[" << stream_group_within_fpga
              << "] = " << group->compute;

      group->host_to_device = new se::Stream(executor);
      group->host_to_device->Init();
      VLOG(2) << "Created host_to_device_stream[" << stream_group_within_fpga
              << "] = " << group->host_to_device;

      group->device_to_host = new se::Stream(executor);
      group->device_to_host->Init();
      VLOG(2) << "Created device_to_host_stream[" << stream_group_within_fpga
              << "] = " << group->device_to_host;

      int num_d2d_streams =
          options.experimental().num_dev_to_dev_copy_streams();
      if (num_d2d_streams == 0) num_d2d_streams = 1;
      if (num_d2d_streams < 1 || num_d2d_streams > 4) {
        LOG(ERROR)
            << "Illegal FPGAOptions.experimental.num_dev_to_dev_copy_streams="
            << num_d2d_streams << " set to 1 instead.";
        num_d2d_streams = 1;
      }
      for (int i = 0; i < num_d2d_streams; ++i) {
        se::Stream* stream = new se::Stream(executor);
        stream->Init();
        group->device_to_device.push_back(stream);
        VLOG(2) << "Created device_to_device_stream[" << stream_group_within_fpga
                << "] = " << group->device_to_device.back();
      }
    }
    return group;
  }

  // Returns a reference to the StreamGroupFactory singleton. Note that this is
  // never destroyed, so the objects it owns are never deleted.
  static StreamGroupFactory& Global() {
    static StreamGroupFactory* instance = new StreamGroupFactory();
    return *instance;
  }

 private:
  mutex lock_;
  using key_type = std::tuple<int, int>;
  std::map<key_type, StreamGroup> streams_;

  // StreamGroupFactory cannot be created directly; Call
  // StreamGroupFactory::Global() to get the global instance.
  StreamGroupFactory() = default;
  TF_DISALLOW_COPY_AND_ASSIGN(StreamGroupFactory);
};

BaseFPGADevice::BaseFPGADevice(const SessionOptions& options, const string& name,
                             Bytes memory_limit, const DeviceLocality& locality,
                             TfFpgaId tf_fpga_id,
                             const string& physical_device_desc,
                             Allocator* fpga_allocator, Allocator* cpu_allocator,
                             bool sync_every_op, int32 max_streams)
    : LocalDevice(options, Device::BuildDeviceAttributes(name, DEVICE_FPGA,
                                                         memory_limit, locality,
                                                         physical_device_desc)),
      fpga_allocator_(fpga_allocator),
      cpu_allocator_(cpu_allocator),
      scoped_allocator_mgr_(new ScopedAllocatorMgr(name)),
      tf_fpga_id_(tf_fpga_id),
      sync_every_op_(sync_every_op),
      max_streams_(max_streams) {
  FPGAProcessState::singleton()->EnableFPGADevice();
}

BaseFPGADevice::~BaseFPGADevice() {
  delete fpga_device_info_;
  for (auto sb : scratch_) fpga_allocator_->DeallocateRaw(sb);
  for (auto ctx : device_contexts_) ctx->Unref();
}

// This should be idempotent if already initialized.
Status BaseFPGADevice::InitScratchBuffers() {
  mutex_lock l(scratch_init_mutex_);
  if (scratch_.size() < max_streams_) {
    for (int i = 0; i < max_streams_; i++) {
      DCHECK(streams_[i]);
      if (scratch_.size() > i && scratch_[i]) continue;
      size_t scratch_buffer_size =
          Eigen::kFpdaScratchSize + sizeof(unsigned int);
      void* scratch_buffer = fpga_allocator_->AllocateRaw(
          Allocator::kAllocatorAlignment, scratch_buffer_size);
      if (scratch_buffer == nullptr) {
        return errors::FailedPrecondition(
            "Failed to allocate scratch buffer for device ",
            tf_fpga_id_.value());
      }
      se::DeviceMemory<char> mem(
          se::DeviceMemoryBase(scratch_buffer, scratch_buffer_size));

      bool ok = executor_->SynchronousMemZero(
          &mem, Eigen::kFpdaScratchSize + sizeof(unsigned int));
      if (!ok) {
        return errors::FailedPrecondition(
            "Failed to memcopy into scratch buffer for device ",
            tf_fpga_id_.value());
      }
      scratch_.push_back(static_cast<char*>(scratch_buffer));
    }
  }
  return Status::OK();
}

Status BaseFPGADevice::Init(const SessionOptions& options) {
  auto executor_status = FpgaIdUtil::ExecutorForTfFpgaId(tf_fpga_id_);
  if (!executor_status.status().ok()) {
    return errors::Internal("Failed to get StreamExecutor for device ",
                            tf_fpga_id_.value());
  }

  executor_ = executor_status.ValueOrDie();
  em_.reset(new EventMgr(executor_, options.config.fpga_options()));

  if (max_streams_ < 1) {
    return errors::InvalidArgument("Invalid value for max_streams.");
  }

  // Create the specified number of FPGA streams
  for (int i = 0; i < max_streams_; i++) {
    streams_.push_back(StreamGroupFactory::Global().GetOrCreate(
        tf_fpga_id_, i, executor_, options.config.fpga_options()));
    device_contexts_.push_back(new FPGADeviceContext(
        i, streams_.back()->compute, streams_.back()->host_to_device,
        streams_.back()->device_to_host, streams_.back()->device_to_device));
  }
  fpga_device_info_ = new FpgaDeviceInfo;
  fpga_device_info_->stream = streams_[0]->compute;
  fpga_device_info_->default_context = device_contexts_[0];
  fpga_device_info_->event_mgr = em_.get();
  PlatformFpgaId platform_fpga_id;
  TF_RETURN_IF_ERROR(
      FpgaIdManager::TfToPlatformFpgaId(tf_fpga_id_, &platform_fpga_id));
  fpga_device_info_->fpga_id = platform_fpga_id.value();
  set_tensorflow_fpga_device_info(fpga_device_info_);

  // Whether and how the FPGA device uses its own threadpool.
  // This option is experimental. Once we confirm the best setting, we
  // may change the default behavior and completely remove this flag.
  // Default values might change in future releases.
  // Possible values:
  //   * global: FPGA uses threads shared with CPU in the main compute
  //          thread-pool. This is currently the default.
  //   * fpga_private: FPGA uses threads dedicated to this device.
  //   * fpga_shared: All FPGAs share a dedicated thread pool.
  string fpga_thread_mode;
  TF_RETURN_IF_ERROR(
      ReadStringFromEnvVar("TF_FPGA_THREAD_MODE", "global", &fpga_thread_mode));
  fpga_thread_mode = str_util::Lowercase(fpga_thread_mode);
  if (fpga_thread_mode != "global") {
    int64 fpga_thread_count = -1;
    // Default to two threads. One for device compute and another for memory
    // copies.
    TF_RETURN_IF_ERROR(
        ReadInt64FromEnvVar("TF_FPGA_THREAD_COUNT", 2, &fpga_thread_count));
    if (fpga_thread_mode == "fpga_private") {
      // TODO(zhengxq): since these threads only serve a single FPGA device,
      //   we should set the device context once for each thread, and avoid
      //   setting them for each kernel.
      // TODO(zhengxq): pin the thread to the same socket of the target FPGA.
      thread_pool_.reset(new thread::ThreadPool(
          options.env, strings::StrCat("fpga_private_", tf_fpga_id_.value()),
          static_cast<int32>(fpga_thread_count)));
      set_tensorflow_device_thread_pool(thread_pool_.get());
    } else if (fpga_thread_mode == "fpga_shared") {
      static thread::ThreadPool* thread_pool = new thread::ThreadPool(
          options.env, "fpga_shared", static_cast<int32>(fpga_thread_count));
      set_tensorflow_device_thread_pool(thread_pool);
    } else {
      string error_message =
          strings::StrCat("Invalid fpga_thread_mode: ", fpga_thread_mode);
      LOG(WARNING) << error_message;
      return errors::InvalidArgument(error_message);
    }
  }

  return Status::OK();
}

bool BaseFPGADevice::RequiresRecordingAccessedTensors() const {
  // When there is no more than one stream, we release the tensor reference
  // at the end of the kernel launch, instead of at the end of the kernel
  // execution.
  return streams_.size() > 1;
}

Status BaseFPGADevice::FillContextMap(const Graph* graph,
                                     DeviceContextMap* device_context_map) {
  VLOG(2) << "FillContextMap";

  const size_t num_streams = streams_.size();
  // Special case for single stream.
  if (num_streams == 1) {
    return Status::OK();
  }
  const int64 before = Env::Default()->NowMicros();
  fpga_stream_util::AssignStreamsOpts opts;
  opts.max_streams = static_cast<int32>(num_streams);
  std::unordered_map<int, int> node_to_stream_id;
  TF_RETURN_IF_ERROR(
      fpga_stream_util::AssignStreams(graph, opts, &node_to_stream_id));
  int64 elapsed = Env::Default()->NowMicros() - before;
  VLOG(3) << "AssignStreams took " << elapsed << "us";

  // Fill in the context map.  It is OK for this map to contain
  // duplicate DeviceContexts so long as we increment the refcount.
  device_context_map->resize(graph->num_node_ids());
  for (Node* n : graph->nodes()) {
    auto mapped_stream = node_to_stream_id[n->id()];
    CHECK_LE(mapped_stream, num_streams);
    auto ctx = device_contexts_[mapped_stream];
    VLOG(3) << "Assigned stream " << node_to_stream_id[n->id()]
            << " ==> stream[" << ctx->stream_id() << "] for node id " << n->id()
            << " " << n->type_string() << " " << n->name();
    ctx->Ref();
    (*device_context_map)[n->id()] = ctx;
  }

  return Status::OK();
}

void BaseFPGADevice::Compute(OpKernel* op_kernel, OpKernelContext* context) {
  // NOTE(tucker): We need to discriminate between Eigen FPGA
  // operations and all others.  If an operation is Eigen
  // implemented (or otherwise tries to launch a fpda kernel
  // directly), we need to establish a stacked-scoped environment
  // that directs it to execute on the proper device.  Otherwise we
  // expect the Op to use StreamExecutor directly and correctly.  The
  // way we make this discrimination is quite hacky: At the moment
  // the only non-Eigen FPGA Op is the recv-op, which is known to be
  // asynchronous.
  if (op_kernel->is_internal() && op_kernel->type_string() == "_Recv") {
    context->SetStatus(errors::Internal(
        "Invalid synchronous 'Compute' on FPGA for '_Recv' op"));
  } else {
    ComputeHelper(op_kernel, context);
  }
}

string BaseFPGADevice::ComputeOpKernelDebugString(const OpKernel& op_kernel,
                                                 const int& stream_id) {
  return strings::StrCat(op_kernel.name(), " op ", op_kernel.type_string(),
                         " on FPGA ", tf_fpga_id_.value(), " stream[", stream_id,
                         "]");
}

void BaseFPGADevice::ComputeHelper(OpKernel* op_kernel,
                                  OpKernelContext* context) {
  FPGADeviceContext* fpga_device_context = device_contexts_[0];
  if (context->op_device_context() != nullptr) {
    fpga_device_context =
        static_cast<FPGADeviceContext*>(context->op_device_context());
  }
  se::Stream* stream = fpga_device_context->stream();
  const auto stream_id = fpga_device_context->stream_id();

  const bool vlog_1 = VLOG_IS_ON(1);
  const bool vlog_2 = vlog_1 && VLOG_IS_ON(2);

  if (vlog_1) {
    VLOG(1) << "FpgaDevice::ComputeHelper "
            << ComputeOpKernelDebugString(*op_kernel, stream_id);
  }

  const auto num_streams = streams_.size();
  if (num_streams > 1) {
    // If this op's device context is different from the other contexts,
    // we must wait on the stream.
    for (int i = 0; i < context->num_inputs(); ++i) {
      const FPGADeviceContext* idc =
          static_cast<FPGADeviceContext*>(context->input_device_context(i));
      OP_REQUIRES(context, idc != nullptr,
                  errors::Internal("Input device context ", i,
                                   " was not set properly."));
      if (vlog_2) {
        const void* base;
        size_t len;
        if (context->has_input(i)) {
          if (IsRefType(context->input_dtype(i))) {
            Tensor tensor = context->mutable_input(i, false);
            base = DMAHelper::base(&tensor);
            len = tensor.TotalBytes();
          } else {
            const Tensor& tensor = context->input(i);
            base = DMAHelper::base(&tensor);
            len = tensor.TotalBytes();
          }
          LOG(INFO) << "Input " << i << " " << base << "  " << len;
          LOG(INFO) << "  stream[" << stream_id << "].ThenWaitFor(stream["
                    << idc->stream_id() << "])"
                    << ((idc->stream() == stream) ? " not needed" : "");
        }
      }
      if (idc->stream() != stream) stream->ThenWaitFor(idc->stream());
    }
  }
  se::fpda::ScopedActivateExecutorContext scoped_activation{stream->parent()};
  op_kernel->Compute(context);
  if (context->status().ok()) {
    if (sync_every_op_) {
      // Note: FPGAUtil::Sync() only syncs the default stream.
      // We need to either sync the stream used by this op, or
      // all streams.  Given that this flag is typically used for
      // debugging it makes more sense to sync all FPGA activity.
      context->SetStatus(FPGAUtil::SyncAll(this));
      if (vlog_1) {
        VLOG(1) << "FpgaDevice::ComputeHelper finished "
                << ComputeOpKernelDebugString(*op_kernel, stream_id);
      }
    } else if (vlog_1) {
      VLOG(1) << "FpgaDevice::ComputeHelper scheduled "
              << ComputeOpKernelDebugString(*op_kernel, stream_id);
    }
  } else {
    if (vlog_1) {
      VLOG(1) << "FpgaDevice::ComputeHelper failed to schedule "
              << ComputeOpKernelDebugString(*op_kernel, stream_id);
    }
  }
}

void BaseFPGADevice::ConsumeListOfAccessedTensors(
    DeviceContext* device_context, const TensorReferenceVector& tensor_refs) {
  FPGADeviceContext* fpga_device_context = device_contexts_[0];
  if (device_context != nullptr) {
    fpga_device_context = static_cast<FPGADeviceContext*>(device_context);
  }
  se::Stream* stream = fpga_device_context->stream();
  em_->ThenDeleteTensors(stream, tensor_refs);
}

// Based on the semantics of Device::Sync this call should wait for
// all streams not just the current one.
Status BaseFPGADevice::Sync() { return FPGAUtil::SyncAll(this); }

void BaseFPGADevice::ComputeAsync(AsyncOpKernel* op_kernel,
                                 OpKernelContext* context,
                                 AsyncOpKernel::DoneCallback done) {
  FPGADeviceContext* fpga_device_context = device_contexts_[0];
  if (context->op_device_context() != nullptr) {
    fpga_device_context =
        static_cast<FPGADeviceContext*>(context->op_device_context());
  }
  se::Stream* stream = fpga_device_context->stream();
  const auto stream_id = fpga_device_context->stream_id();

  VLOG(1) << "FpgaDevice::ComputeAsync " << op_kernel->name() << " op "
          << op_kernel->type_string() << " on FPGA" << tf_fpga_id_ << " stream["
          << stream_id << "]";

  // When Xprof profiling is off (which is the default), constructing the
  // activity is simple enough that its overhead is negligible.
  tracing::ScopedActivity activity(op_kernel->name(), op_kernel->type_string(),
                                   op_kernel->IsExpensive());
  se::fpda::ScopedActivateExecutorContext scoped_activation{stream->parent()};
  op_kernel->ComputeAsync(context, done);
}

Status BaseFPGADevice::MaybeCopyTensorToFPGA(
    const AllocatorAttributes& alloc_attrs, const Tensor& from, Tensor* to,
    StatusCallback done) {
  if (alloc_attrs.on_host()) {
    *to = from;
    done(Status::OK());
    return Status::OK();
  } else {
    if (!DMAHelper::CanUseDMA(&from)) {
      Status err = errors::Internal("FPGA copy from non-DMA ",
                                    DataTypeString(from.dtype()), " tensor");
      done(err);
      return err;
    }
    auto* copy =
        new Tensor(GetAllocator(alloc_attrs), from.dtype(), from.shape());

    // If the tensor is not initialized, we likely ran out of memory.
    if (!copy->IsInitialized()) {
      delete copy;
      Status err = errors::ResourceExhausted(
          "OOM when allocating tensor of shape ", from.shape().DebugString(),
          " and type ", DataTypeString(from.dtype()));
      done(err);
      return err;
    }

    StatusCallback wrapped_done = std::bind(
        [to, copy](StatusCallback done_,
                   // Begin unbound arguments.
                   const Status& s) {
          *to = std::move(*copy);
          delete copy;
          done_(s);
        },
        std::move(done), std::placeholders::_1);

    tracing::ScopedAnnotation annotation("MakeTensorFromProto");
    device_contexts_[0]->CopyCPUTensorToDevice(&from, this, copy,
                                               std::move(wrapped_done));
    return Status::OK();
  }
}

Status BaseFPGADevice::MakeTensorFromProto(const TensorProto& tensor_proto,
                                          const AllocatorAttributes alloc_attrs,
                                          Tensor* tensor) {
  AllocatorAttributes attr;
  attr.set_on_host(true);
  attr.set_fpga_compatible(true);
  Allocator* host_alloc = GetAllocator(attr);
  Tensor parsed(tensor_proto.dtype());
  if (!parsed.FromProto(host_alloc, tensor_proto)) {
    return errors::InvalidArgument("Cannot parse tensor from proto: ",
                                   tensor_proto.DebugString());
  }

  if (parsed.dtype() == DT_VARIANT) {
    const Variant* from = parsed.flat<Variant>().data();
    Tensor copy(cpu_allocator(), DT_VARIANT, parsed.shape());
    Variant* copy_variant = copy.flat<Variant>().data();

    std::list<Notification> notifications;
    Status copy_status;
    auto copier = [this, &alloc_attrs, &notifications, &copy_status](
                      const Tensor& from, Tensor* to) {
      // Copier isn't run in a multithreaded environment, so we don't
      // have to worry about the notifications list being modified in parallel.
      notifications.emplace_back();
      Notification& n = *notifications.rbegin();
      return MaybeCopyTensorToFPGA(alloc_attrs, from, to,
                                  [&n, &copy_status](const Status& s) {
                                    if (copy_status.ok()) {
                                      copy_status.Update(s);
                                    }
                                    n.Notify();
                                  });
    };
    Status s;
    for (int64 ix = 0; ix < parsed.NumElements(); ++ix) {
      s = VariantDeviceCopy(VariantDeviceCopyDirection::HOST_TO_DEVICE,
                            from[ix], &copy_variant[ix], copier);
      if (!s.ok()) {
        break;
      }
    }
    for (auto& n : notifications) {
      n.WaitForNotification();
    }
    if (!s.ok()) {
      return s;
    }
    *tensor = std::move(copy);
    return copy_status;
  } else {
    Notification n;
    Status status;
    TF_RETURN_IF_ERROR(MaybeCopyTensorToFPGA(alloc_attrs, parsed, tensor,
                                            [&n, &status](const Status& s) {
                                              status = s;
                                              n.Notify();
                                            }));
    n.WaitForNotification();
    return status;
  }
}

namespace {
class ConcretePerOpFpgaDevice : public PerOpFpgaDevice {
 public:
  ConcretePerOpFpgaDevice() : device_(&stream_device_) {}

  void Reinitialize(OpKernelContext* context, const fpdaStream_t* fpda_stream,
                    TfFpgaId tf_fpga_id, Allocator* base_allocator,
                    char* scratch) {
    stream_device_.Reinitialize(context, fpda_stream, tf_fpga_id, base_allocator,
                                scratch);
  }

  const Eigen::FpgaDevice& device() const override { return device_; }

 private:
  EigenFpdaStreamDevice stream_device_;
  Eigen::FpgaDevice device_;
};

// Parse 'visible_device_list' into a list of platform FPGA ids.
Status ParseVisibleDeviceList(const string& visible_device_list,
                              std::vector<PlatformFpgaId>* visible_fpga_order) {
  visible_fpga_order->clear();
  se::Platform* fpga_manager = FPGAMachineManager();

  // If the user wants to remap the visible to virtual FPGA mapping,
  // check for that here.
  if (visible_device_list.empty()) {
    visible_fpga_order->resize(fpga_manager->VisibleDeviceCount());
    // By default, visible to virtual mapping is unchanged.
    int deviceNo = 0;
    std::generate(visible_fpga_order->begin(), visible_fpga_order->end(),
                  [&deviceNo] { return deviceNo++; });
  } else {
    const std::vector<string> order_str =
        str_util::Split(visible_device_list, ',');
    for (const string& platform_fpga_id_str : order_str) {
      int32 platform_fpga_id;
      if (!strings::safe_strto32(platform_fpga_id_str, &platform_fpga_id)) {
        return errors::InvalidArgument(
            "Could not parse entry in 'visible_device_list': '",
            platform_fpga_id_str, "'. visible_device_list = ",
            visible_device_list);
      }
      if (platform_fpga_id < 0 ||
          platform_fpga_id >= fpga_manager->VisibleDeviceCount()) {
        return errors::InvalidArgument(
            "'visible_device_list' listed an invalid FPGA id '", platform_fpga_id,
            "' but visible device count is ",
            fpga_manager->VisibleDeviceCount());
      }
      visible_fpga_order->push_back(PlatformFpgaId(platform_fpga_id));
    }
  }

  // Validate no repeats.
  std::set<PlatformFpgaId> visible_device_set(visible_fpga_order->begin(),
                                             visible_fpga_order->end());
  if (visible_device_set.size() != visible_fpga_order->size()) {
    return errors::InvalidArgument(
        "visible_device_list contained a duplicate entry: ",
        visible_device_list);
  }
  return Status::OK();
}

Status VerifyVirtualDeviceSettings(
    const size_t num_fpgas_to_use, const FPGAOptions& fpga_options,
    const std::vector<PlatformFpgaId>& visible_fpga_order,
    const std::vector<PlatformFpgaId>& valid_platform_fpga_ids) {
  const auto& virtual_devices = fpga_options.experimental().virtual_devices();
  CHECK(!virtual_devices.empty());
  if (fpga_options.per_process_fpga_memory_fraction() > 0) {
    return errors::InvalidArgument(
        "It's invalid to set per_process_fpga_memory_fraction when "
        "virtual_devices is set.");
  }
  if (num_fpgas_to_use < virtual_devices.size()) {
    return errors::Unknown(
        "Not enough FPGAs to create virtual devices."
        " num_fpgas_to_use: ",
        num_fpgas_to_use, " #virtual_devices: ", virtual_devices.size());
  }
  if (!fpga_options.visible_device_list().empty() &&
      visible_fpga_order.size() != virtual_devices.size()) {
    return errors::InvalidArgument(
        "The number of FPGAs in visible_device_list doesn't match the number "
        "of elements in the virtual_devices list.",
        " #FPGAs in visible_device_list: ", visible_fpga_order.size(),
        " virtual_devices.size(): ", virtual_devices.size());
  }
  if (valid_platform_fpga_ids.size() != virtual_devices.size()) {
    return errors::Unknown(
        "The number of valid FPGAs doesn't match the number of elements in "
        "the virtual_devices list.",
        " #valid FPGAs: ", valid_platform_fpga_ids.size(),
        " virtual_devices.size(): ", virtual_devices.size());
  }
  return Status::OK();
}

int64 MinSystemMemory(int64 available_memory) {
  // We use the following heuristic for now:
  //
  // If the available_memory is < 2GiB, we allocate 225MiB to system memory.
  // Otherwise, allocate max(300MiB, 0.05 * available_memory) to system memory.
  //
  // In the future we could be more sophisticated by using a table of devices.
  int64 min_system_memory;
  if (available_memory < (1LL << 31)) {
    // 225MiB
    min_system_memory = 225 * 1024 * 1024;
  } else {
    // max(300 MiB, 0.05 * available_memory)
    min_system_memory =
        std::max(int64{314572800}, static_cast<int64>(available_memory * 0.05));
  }
#if defined(__GNUC__) && defined(__OPTIMIZE__)
// Do nothing
#elif !defined(__GNUC__) && defined(NDEBUG)
// Do nothing
#else
  // Double the amount of available FPGA memory in non-opt builds (debug
  // builds in windows); because in non-opt builds more system memory
  // is necessary.
  min_system_memory *= 2;
#endif

#if defined(ANDROID_TEGRA)
  // 1GB system mem for NVIDIA Tegra devices since they use the same mem for RAM
  // and Video RAM
  min_system_memory = 1 << 30;
#endif
  return min_system_memory;
}

// Get the memory limit for the virtual device being created on FPGA with
// 'platform_fpga_id', when that virtual device is the only virtual device being
// created on that FPGA.
Status SingleVirtualDeviceMemoryLimit(const FPGAOptions& fpga_options,
                                      PlatformFpgaId platform_fpga_id,
                                      int64* memory_limit) {
  int64 total_memory = 0;
  int64 available_memory = 0;
  se::StreamExecutor* se =
      FpgaIdUtil::ExecutorForPlatformFpgaId(platform_fpga_id).ValueOrDie();
  if (!se->DeviceMemoryUsage(&available_memory, &total_memory)) {
    return errors::Unknown("Failed to query available memory for FPGA ",
                           platform_fpga_id.value());
  }

  int64 allocated_memory = 0;
  const double per_process_fpga_memory_fraction =
      fpga_options.per_process_fpga_memory_fraction();
  if (per_process_fpga_memory_fraction > 1.0 ||
      fpga_options.experimental().use_unified_memory()) {
    int cc_major = 0, cc_minor = 0;
    if (!se->GetDeviceDescription().fpda_compute_capability(&cc_major,
                                                            &cc_minor)) {
      return errors::Internal("Failed to get compute capability for device.");
    }
    if (cc_major < 6) {
      return errors::Internal(
          "Unified memory on FPGAs with compute capability lower than 6.0 "
          "(pre-Pascal class FPGAs) does not support oversubscription.");
    }
  }

  if (per_process_fpga_memory_fraction == 0) {
    allocated_memory = available_memory;
    const int64 min_system_memory = MinSystemMemory(available_memory);
    if (min_system_memory < allocated_memory) {
      allocated_memory -= min_system_memory;
    }
  } else {
    allocated_memory = total_memory * per_process_fpga_memory_fraction;
  }
  *memory_limit = allocated_memory;
  return Status::OK();
}
}  // namespace

void BaseFPGADevice::ReinitializeDevice(OpKernelContext* context,
                                       PerOpFpgaDevice* device, int stream_id,
                                       Allocator* allocator) {
  ConcretePerOpFpgaDevice* concrete_device =
      static_cast<ConcretePerOpFpgaDevice*>(device);
  DCHECK(concrete_device);
  const fpdaStream_t* fpda_stream = reinterpret_cast<const fpdaStream_t*>(
      streams_[stream_id]->compute->implementation()->FpgaStreamMemberHack());
  concrete_device->Reinitialize(context, fpda_stream, tf_fpga_id_, allocator,
                                scratch_[stream_id]);
}

PerOpFpgaDevice* BaseFPGADevice::MakeFpgaDevice() {
  return new ConcretePerOpFpgaDevice();
}

Status BaseFPGADevice::ReinitializeFpgaDevice(OpKernelContext* context,
                                            PerOpFpgaDevice* device,
                                            DeviceContext* dc,
                                            Allocator* allocator) {
  TF_RETURN_IF_ERROR(InitScratchBuffers());
  if (dc) {
    const FPGADeviceContext* fpga_dc = static_cast<FPGADeviceContext*>(dc);
    const int stream_id = fpga_dc->stream_id();
    VLOG(1) << "  eigen_fpga_device(" << dc << ") => stream[" << stream_id
            << "]";
    CHECK_LT(stream_id, streams_.size());
    ReinitializeDevice(context, device, stream_id, allocator);
  } else {
    ReinitializeDevice(context, device, 0, allocator);
  }
  return Status::OK();
}

Allocator* BaseFPGADevice::GetScopedAllocator(AllocatorAttributes attr,
                                             int64 step_id) {
  if (attr.scope_id > 0) {
    return scoped_allocator_mgr_->GetContainer(step_id)->GetInstance(
        attr.scope_id);
  }
  LOG(FATAL) << "Unexpected call to BaseFPGADevice::GetScopedAllocator "
             << "attr.scope_id = " << attr.scope_id;
  return fpga_allocator_;
}

const int BaseFPGADeviceFactory::InterconnectMap::kSameDeviceStrength = 1000;
const int BaseFPGADeviceFactory::InterconnectMap::kStreamExecutorStrength = 1;

Status BaseFPGADeviceFactory::CreateDevices(const SessionOptions& options,
                                           const string& name_prefix,
                                           std::vector<Device*>* devices) {
  TF_RETURN_IF_ERROR(ValidateFPGAMachineManager());
  se::Platform* fpga_manager = FPGAMachineManager();
  if (fpga_manager == nullptr) {
    return Status::OK();
  }
  // If there are no FPGAs visible, do nothing.
  if (fpga_manager->VisibleDeviceCount() <= 0) {
    return Status::OK();
  }

  size_t num_fpgas_to_use = INT_MAX;
  auto iter = options.config.device_count().find("FPGA");
  if (iter != options.config.device_count().end()) {
    num_fpgas_to_use = iter->second;
  }
  const auto& fpga_options = options.config.fpga_options();
  std::vector<PlatformFpgaId> visible_fpga_order;
  std::vector<PlatformFpgaId> valid_platform_fpga_ids;
  // If we aren't going to use any FPGAs, don't initialize them.
  // We don't want to call ParseVisibleDeviceList if num_fpgas_to_use is 0,
  // because it treats an empty fpga_options.visible_device_list as 'all FPGAs are
  // visible'.
  if (num_fpgas_to_use > 0) {
    TF_RETURN_IF_ERROR(ParseVisibleDeviceList(fpga_options.visible_device_list(),
                                              &visible_fpga_order));
    TF_RETURN_IF_ERROR(
        GetValidDeviceIds(visible_fpga_order, &valid_platform_fpga_ids));
  }
  if (num_fpgas_to_use > valid_platform_fpga_ids.size()) {
    num_fpgas_to_use = valid_platform_fpga_ids.size();
  }
  if (!valid_platform_fpga_ids.empty()) {
    // Save the original device.
    int original_device = 0;
    fpdaError_t err = fpdaGetDevice(&original_device);
    if (err != fpdaSuccess) {
      return errors::Internal("fpdaGetDevice() failed. Status: ",
                              fpdaGetErrorString(err));
    }
    // Force to implicitly initialize FPDA runtime on each valid FPGA before
    // CreateFPGADevice().
    for (PlatformFpgaId platform_fpga_id : valid_platform_fpga_ids) {
      err = fpdaSetDevice(platform_fpga_id.value());
      if (err != fpdaSuccess) {
        return errors::Internal("fpdaSetDevice() on FPGA:",
                                platform_fpga_id.value(), " failed. Status: ",
                                fpdaGetErrorString(err));
      }
      err = fpdaFree(nullptr);
      if (err != fpdaSuccess) {
        return errors::Internal("FPDA runtime implicit initialization on FPGA:",
                                platform_fpga_id.value(), " failed. Status: ",
                                fpdaGetErrorString(err));
      }
    }
    // Reset to the original device.
    err = fpdaSetDevice(original_device);
    if (err != fpdaSuccess) {
      return errors::Internal("fpdaSetDevice() on FPGA:", original_device,
                              " failed. Status: ", fpdaGetErrorString(err));
    }
  }

  std::vector<InterconnectMap> interconnect_maps;
  TF_RETURN_IF_ERROR(
      GetInterconnectMaps(visible_fpga_order, fpga_manager, &interconnect_maps));

  // Print each interconnect map to the log.
  for (const InterconnectMap& im : interconnect_maps) {
    LOG(INFO) << "Device interconnect " << im.name << " with strength "
              << im.strength << " edge matrix:";
    string line_buf = "     ";
    for (int i = 0; i < visible_fpga_order.size(); ++i) {
      strings::StrAppend(&line_buf, visible_fpga_order[i].value(), " ");
    }
    LOG(INFO) << line_buf;
    for (int i = 0; i < visible_fpga_order.size(); ++i) {
      line_buf = strings::StrCat(visible_fpga_order[i].value(), ":   ");
      PlatformFpgaId fpga_id_i = visible_fpga_order[i];
      for (int j = 0; j < visible_fpga_order.size(); ++j) {
        PlatformFpgaId fpga_id_j = visible_fpga_order[j];
        if (im.directed_links.find({fpga_id_i, fpga_id_j}) !=
            im.directed_links.end()) {
          line_buf.append("Y ");
        } else {
          line_buf.append("N ");
        }
      }
      LOG(INFO) << line_buf;
    }
  }

  const auto& virtual_devices = fpga_options.experimental().virtual_devices();
  if (!virtual_devices.empty()) {
    TF_RETURN_IF_ERROR(VerifyVirtualDeviceSettings(num_fpgas_to_use, fpga_options,
                                                   visible_fpga_order,
                                                   valid_platform_fpga_ids));
    // We've verified that num_fpgas_to_use >= virtual_devices.size().
    num_fpgas_to_use = virtual_devices.size();
    CHECK(fpga_options.visible_device_list().empty() ||
          valid_platform_fpga_ids == visible_fpga_order);
  }
  int next_tf_fpga_id = 0;
  std::vector<int64> memory_limit_bytes;
  for (int i = 0; i < num_fpgas_to_use; ++i) {
    const PlatformFpgaId platform_fpga_id = valid_platform_fpga_ids[i];
    if (virtual_devices.empty() ||
        virtual_devices.Get(i).memory_limit_mb_size() == 0) {
      int64 single_virtual_device_memory_limit = 0;
      TF_RETURN_IF_ERROR(SingleVirtualDeviceMemoryLimit(
          fpga_options, platform_fpga_id, &single_virtual_device_memory_limit));
      memory_limit_bytes.push_back(single_virtual_device_memory_limit);
    } else {
      const auto& memory_limit_mb = virtual_devices.Get(i).memory_limit_mb();
      std::transform(memory_limit_mb.begin(), memory_limit_mb.end(),
                     std::back_inserter(memory_limit_bytes), [](float mb) {
                       return static_cast<int64>(mb) * (1ll << 20);
                     });
    }
    while (next_tf_fpga_id < memory_limit_bytes.size()) {
      TfFpgaId tf_fpga_id(next_tf_fpga_id);
      ++next_tf_fpga_id;
      TF_RETURN_IF_ERROR(
          FpgaIdManager::InsertTfPlatformFpgaIdPair(tf_fpga_id, platform_fpga_id));
    }
  }
  const int num_tf_fpgas = next_tf_fpga_id;

  LocalityMap device_localities;
  TF_RETURN_IF_ERROR(
      GetDeviceLocalities(num_tf_fpgas, interconnect_maps, &device_localities));

  // Build the FPGADevices
  CHECK_EQ(next_tf_fpga_id, memory_limit_bytes.size());
  for (int di = 0; di < num_tf_fpgas; ++di) {
    TfFpgaId tf_fpga_id(di);
    int64 bytes = memory_limit_bytes[di];
    auto it = device_localities.find(tf_fpga_id);
    if (it == device_localities.end()) {
      return errors::Internal("Failed to find DeviceLocality for FPGA device ",
                              tf_fpga_id.value());
    }
    TF_RETURN_IF_ERROR(CreateFPGADevice(options, name_prefix, tf_fpga_id, bytes,
                                       it->second, devices));
  }
  return Status::OK();
}

static string GetShortDeviceDescription(PlatformFpgaId platform_fpga_id,
                                        const se::DeviceDescription& desc) {
  int cc_major;
  int cc_minor;
  if (!desc.fpda_compute_capability(&cc_major, &cc_minor)) {
    cc_major = 0;
    cc_minor = 0;
  }
  // LINT.IfChange
  return strings::StrCat("device: ", platform_fpga_id.value(), ", name: ",
                         desc.name(), ", pci bus id: ", desc.pci_bus_id(),
                         ", compute capability: ", cc_major, ".", cc_minor);
  // LINT.ThenChange(//tensorflow/python/platform/test.py)
}

Status BaseFPGADeviceFactory::CreateFPGADevice(const SessionOptions& options,
                                             const string& name_prefix,
                                             TfFpgaId tf_fpga_id,
                                             int64 memory_limit,
                                             const DeviceLocality& dev_locality,
                                             std::vector<Device*>* devices) {
  CHECK_GE(tf_fpga_id.value(), 0);
  const string device_name =
      strings::StrCat(name_prefix, "/device:FPGA:", tf_fpga_id.value());
  FpgaIdUtil::CheckValidTfFpgaId(tf_fpga_id);
  PlatformFpgaId platform_fpga_id;
  TF_RETURN_IF_ERROR(
      FpgaIdManager::TfToPlatformFpgaId(tf_fpga_id, &platform_fpga_id));
  int numa_node = dev_locality.numa_node();

  se::StreamExecutor* se =
      FpgaIdUtil::ExecutorForPlatformFpgaId(platform_fpga_id).ValueOrDie();
  const se::DeviceDescription& desc = se->GetDeviceDescription();
  FPGAProcessState* process_state = FPGAProcessState::singleton();
  Allocator* fpga_allocator = process_state->GetFPGAAllocator(
      options.config.fpga_options(), tf_fpga_id, memory_limit);
  if (fpga_allocator == nullptr) {
    return errors::Internal("Failed to get memory allocator for TF FPGA ",
                            tf_fpga_id.value(), " with ", memory_limit,
                            " bytes of memory.");
  }
  AllocatorStats stats;
  fpga_allocator->GetStats(&stats);
  // 'memory_limit' is the required memory size, but if the allocator with given
  // tf_fpga_id was created before, we'll use it instead of creating a new one
  // (as TF fpga device is a shared resource), in which case the actual memory
  // limit represented by 'stats.bytes_limit' used by that allocator may be
  // different (which should be an error).
  //
  // TODO(laigd): report error if memory_limit doesn't match stats.bytes_limit.
  BaseFPGADevice* fpga_device = CreateFPGADevice(
      options, device_name, static_cast<Bytes>(stats.bytes_limit), dev_locality,
      tf_fpga_id, GetShortDeviceDescription(platform_fpga_id, desc),
      fpga_allocator, ProcessState::singleton()->GetCPUAllocator(numa_node));
  LOG(INFO) << "Created TensorFlow device (" << device_name << " with "
            << (stats.bytes_limit >> 20) << " MB memory) -> physical FPGA ("
            << GetShortDeviceDescription(platform_fpga_id, desc) << ")";
  TF_RETURN_IF_ERROR(fpga_device->Init(options));
  devices->push_back(fpga_device);

  return Status::OK();
}

namespace {
std::unique_ptr<std::map<std::pair<PlatformFpgaId, PlatformFpgaId>, bool>>
GetPeerAccessMap(se::Platform* platform,
                 const std::vector<PlatformFpgaId>& visible_fpga_order) {
  std::unique_ptr<std::map<std::pair<PlatformFpgaId, PlatformFpgaId>, bool>> map(
      new std::map<std::pair<PlatformFpgaId, PlatformFpgaId>, bool>);
  for (PlatformFpgaId platform_fpga_i : visible_fpga_order) {
    for (PlatformFpgaId platform_fpga_j : visible_fpga_order) {
      se::StreamExecutor* from =
          FpgaIdUtil::ExecutorForPlatformFpgaId(platform, platform_fpga_i)
              .ValueOrDie();
      se::StreamExecutor* to =
          FpgaIdUtil::ExecutorForPlatformFpgaId(platform, platform_fpga_j)
              .ValueOrDie();
      (*map)[{platform_fpga_i, platform_fpga_j}] =
          from->CanEnablePeerAccessTo(to);
    }
  }

  return map;
}

}  // namespace

Status BaseFPGADeviceFactory::GetInterconnectMaps(
    const std::vector<PlatformFpgaId>& visible_fpga_order,
    se::Platform* fpga_manager, std::vector<InterconnectMap>* maps) {
  // The default interconnect map is obtained from the StreamExecutor.
  auto access_map = GetPeerAccessMap(fpga_manager, visible_fpga_order);
  maps->resize(1);
  InterconnectMap& imap = maps->at(0);
  imap.name = "StreamExecutor";
  imap.strength = InterconnectMap::kStreamExecutorStrength;
  for (PlatformFpgaId fpga_id_i : visible_fpga_order) {
    for (PlatformFpgaId fpga_id_j : visible_fpga_order) {
      if (fpga_id_i == fpga_id_j) continue;
      if ((*access_map)[{fpga_id_i, fpga_id_j}]) {
        imap.directed_links.insert({fpga_id_i, fpga_id_j});
      }
    }
  }
  return Status::OK();
}

Status BaseFPGADeviceFactory::GetDeviceLocalities(
    int num_tf_fpgas, const std::vector<InterconnectMap>& interconnects,
    LocalityMap* localities) {
  std::vector<TfFpgaId> all_tf_fpga_ids;
  for (int i = 0; i < num_tf_fpgas; ++i) {
    all_tf_fpga_ids.push_back(TfFpgaId(i));
  }
  for (TfFpgaId tf_fpga_id : all_tf_fpga_ids) {
    PlatformFpgaId platform_fpga_id;
    TF_RETURN_IF_ERROR(
        FpgaIdManager::TfToPlatformFpgaId(tf_fpga_id, &platform_fpga_id));
    // Get FPGA bus_id from its reported NUMA affinity.  Because FPGAs are
    // virtualized in some environments, we can't just use the FPGA id.
    // NUMA locales are indexed from 0, buses are indexed from 1.
    se::StreamExecutor* se =
        FpgaIdUtil::ExecutorForPlatformFpgaId(platform_fpga_id).ValueOrDie();
    const se::DeviceDescription& desc = se->GetDeviceDescription();
    int numa_node = desc.numa_node();
    if (numa_node < 0) {
      // For some reason the StreamExecutor couldn't get the NUMA
      // affinity of the FPGA.  If this is not a multi-socket mobo with
      // FPGAs local to different buses, it doesn't matter.  If it is, we
      // may run into trouble later with data transfer operations.  The
      // trouble may manifest as slower than expected performance, or
      // outright failures.
      LOG(INFO) << "Could not identify NUMA node of platform FPGA id "
                << platform_fpga_id
                << ", defaulting to 0.  Your kernel may not have been built "
                << "with NUMA support.";
      numa_node = 0;
    }
    DeviceLocality dev_locality;
    dev_locality.set_numa_node(numa_node);
    dev_locality.set_bus_id(numa_node + 1);

    // Set LocalLinks from InterconnectMaps.
    LocalLinks* links = dev_locality.mutable_links();
    for (const InterconnectMap& imap : interconnects) {
      for (TfFpgaId tf_fpga_dst : all_tf_fpga_ids) {
        PlatformFpgaId platform_fpga_dst;
        TF_RETURN_IF_ERROR(
            FpgaIdManager::TfToPlatformFpgaId(tf_fpga_dst, &platform_fpga_dst));
        if (imap.directed_links.find({platform_fpga_id, platform_fpga_dst}) !=
            imap.directed_links.end()) {
          InterconnectLink* ilink = links->add_link();
          ilink->set_device_id(tf_fpga_dst.value());
          ilink->set_type(imap.name);
          ilink->set_strength(imap.strength);
        }
      }
    }

    // If this is one of multiple virtual FPGAs on the same physical FPGA
    // add high strength links to the others.
    for (TfFpgaId tf_fpga_dst : all_tf_fpga_ids) {
      if (tf_fpga_id == tf_fpga_dst) continue;
      PlatformFpgaId platform_fpga_dst;
      TF_RETURN_IF_ERROR(
          FpgaIdManager::TfToPlatformFpgaId(tf_fpga_dst, &platform_fpga_dst));
      if (platform_fpga_id == platform_fpga_dst) {
        InterconnectLink* ilink = links->add_link();
        ilink->set_device_id(tf_fpga_dst.value());
        ilink->set_type("SAME_DEVICE");
        ilink->set_strength(InterconnectMap::kSameDeviceStrength);
      }
    }

    (*localities)[tf_fpga_id] = dev_locality;
    VLOG(1) << "FPGADevice PlatformFpgaId " << platform_fpga_id << " TfFpgaId "
            << tf_fpga_id << " on bus " << dev_locality.bus_id()
            << " numa: " << numa_node << " pci: " << desc.pci_bus_id()
            << " DeviceLocality: " << dev_locality.DebugString();
  }
  return Status::OK();
}

static int GetDefaultMinFPGAMultiprocessorCount(
    se::Platform* fpga_manager,
    const std::vector<PlatformFpgaId>& visible_fpga_order) {
  static const int kDefaultMinFPGAMultiprocessorCount = 8;

  // Find the highest multi-processor count across all visible FPGAs.
  int max_count = -1;
  for (int i = 0; i < visible_fpga_order.size(); ++i) {
    auto exec_status =
        FpgaIdUtil::ExecutorForPlatformFpgaId(fpga_manager, visible_fpga_order[i]);
    if (!exec_status.ok()) {
      continue;
    }

    se::StreamExecutor* se = exec_status.ValueOrDie();
    const se::DeviceDescription& desc = se->GetDeviceDescription();
    max_count = std::max(max_count, desc.core_count());
  }

  if (max_count < 0 || kDefaultMinFPGAMultiprocessorCount < max_count) {
    return kDefaultMinFPGAMultiprocessorCount;
  } else {
    return max_count;
  }
}

static int GetMinFPGAMultiprocessorCount(
    se::Platform* fpga_manager,
    const std::vector<PlatformFpgaId>& visible_fpga_order) {
  const char* tf_min_fpga_core_count = getenv("TF_MIN_FPGA_MULTIPROCESSOR_COUNT");

  if (tf_min_fpga_core_count == nullptr ||
      strcmp(tf_min_fpga_core_count, "") == 0) {
    return GetDefaultMinFPGAMultiprocessorCount(fpga_manager, visible_fpga_order);
  }

  int min_fpga_core_count = -1;
  if (strings::safe_strto32(tf_min_fpga_core_count, &min_fpga_core_count)) {
    if (min_fpga_core_count >= 0) {
      return min_fpga_core_count;
    }
  }

  int count =
      GetDefaultMinFPGAMultiprocessorCount(fpga_manager, visible_fpga_order);
  LOG(ERROR) << "Invalid minimum FPGA multiprocessor count: ["
             << tf_min_fpga_core_count << "]. "
             << "Using the default value: " << count;
  return count;
}

namespace {

struct FpdaVersion {
  // Initialize from version_name in the form of "3.5"
  explicit FpdaVersion(const std::string& version_name) {
    size_t dot_pos = version_name.find('.');
    CHECK(dot_pos != string::npos)
        << "Illegal version name: [" << version_name << "]";
    string major_str = version_name.substr(0, dot_pos);
    CHECK(strings::safe_strto32(major_str, &major_part))
        << "Illegal version name: [" << version_name << "]";
    string minor_str = version_name.substr(dot_pos + 1);
    CHECK(strings::safe_strto32(minor_str, &minor_part))
        << "Illegal version name: [" << version_name << "]";
  }
  FpdaVersion() {}
  bool operator<(const FpdaVersion& other) const {
    if (this->major_part != other.major_part) {
      return this->major_part < other.major_part;
    }
    return this->minor_part < other.minor_part;
  }
  friend std::ostream& operator<<(std::ostream& os,
                                  const FpdaVersion& version) {
    os << version.major_part << "." << version.minor_part;
    return os;
  }
  int major_part = -1;
  int minor_part = -1;
};

std::vector<FpdaVersion> supported_fpda_compute_capabilities = {
    TF_FPDA_CAPABILITIES,};

std::vector<FpdaVersion> GetSupportedFpdaComputeCapabilities() {
  auto fpda_caps = supported_fpda_compute_capabilities;
#ifdef TF_EXTRA_FPDA_CAPABILITIES
// TF_EXTRA_FPDA_CAPABILITIES should be defined a sequence separated by commas,
// for example:
//   TF_EXTRA_FPDA_CAPABILITIES=3.0,4.0,5.0
// Use two-level macro expansion for stringification.
#define TF_XSTRING(...) #__VA_ARGS__
#define TF_STRING(s) TF_XSTRING(s)
  string extra_fpda_caps = TF_STRING(TF_EXTRA_FPDA_CAPABILITIES);
#undef TF_STRING
#undef TF_XSTRING
  auto extra_capabilities = str_util::Split(extra_fpda_caps, ',');
  for (const auto& capability : extra_capabilities) {
    fpda_caps.push_back(FpdaVersion(capability));
  }
#endif
  return fpda_caps;
}

Status EnablePeerAccess(se::Platform* platform,
                        const std::vector<PlatformFpgaId>& visible_fpga_order) {
  int possible_peer_count = 0;
  int enabled_peer_count = 0;
  for (int i = 0; i < visible_fpga_order.size(); ++i) {
    const PlatformFpgaId platform_fpga_i = visible_fpga_order[i];
    for (int j = 0; j < visible_fpga_order.size(); ++j) {
      const PlatformFpgaId platform_fpga_j = visible_fpga_order[j];
      // We have already validated that ExecutorForDevice() calls return OK.
      se::StreamExecutor* from =
          FpgaIdUtil::ExecutorForPlatformFpgaId(platform, platform_fpga_i)
              .ValueOrDie();
      se::StreamExecutor* to =
          FpgaIdUtil::ExecutorForPlatformFpgaId(platform, platform_fpga_j)
              .ValueOrDie();

      if (from->CanEnablePeerAccessTo(to)) {
        ++possible_peer_count;
        auto status = from->EnablePeerAccessTo(to);
        if (!status.ok()) {
          LOG(WARNING)
              << "Unable to enable peer access between device ordinals "
              << platform_fpga_i << " and " << platform_fpga_j
              << ", status: " << status;
        } else {
          ++enabled_peer_count;
        }
      }
    }
  }

  // Return an error in the extreme failure case where the driver
  // reported that peering was possible but not a single peering was
  // successful.  This is to catch possible system misconfigurations
  // or more fundamental issues.
  if (possible_peer_count > 0 && enabled_peer_count == 0) {
    return errors::Internal(possible_peer_count,
                            " potential peer access pairs were reported by the "
                            "driver, but no peering could be enabled.");
  }
  return Status::OK();
}

}  // namespace

Status BaseFPGADeviceFactory::GetValidDeviceIds(
    const std::vector<PlatformFpgaId>& visible_fpga_order,
    std::vector<PlatformFpgaId>* ids) {
  se::Platform* fpga_manager = FPGAMachineManager();
  bool new_fpga_found = false;
  for (int i = 0; i < visible_fpga_order.size(); ++i) {
    const PlatformFpgaId visible_fpga_id = visible_fpga_order[i];

    // Only perform this once per visible platform fpga id.
    if (visible_fpga_initialized_[visible_fpga_id.value()]) {
      continue;
    }

    visible_fpga_initialized_[visible_fpga_id.value()] = true;
    new_fpga_found = true;

    auto executor =
        FpgaIdUtil::ExecutorForPlatformFpgaId(fpga_manager, visible_fpga_id);
    if (!executor.ok()) {
      return executor.status();
    }

    auto stream_exec = executor.ValueOrDie();
    int64 free_bytes;
    int64 total_bytes;
    if (!stream_exec->DeviceMemoryUsage(&free_bytes, &total_bytes)) {
      // Logs internally on failure.
      free_bytes = 0;
      total_bytes = 0;
    }
    const auto& description = stream_exec->GetDeviceDescription();
    int cc_major;
    int cc_minor;
    if (!description.fpda_compute_capability(&cc_major, &cc_minor)) {
      // Logs internally on failure.
      cc_major = 0;
      cc_minor = 0;
    }
    LOG(INFO) << "Found device " << i << " with properties: "
              << "\nname: " << description.name() << " major: " << cc_major
              << " minor: " << cc_minor
              << " memoryClockRate(GHz): " << description.clock_rate_ghz()
              << "\npciBusID: " << description.pci_bus_id() << "\ntotalMemory: "
              << strings::HumanReadableNumBytes(total_bytes)
              << " freeMemory: " << strings::HumanReadableNumBytes(free_bytes);
  }
  // Checking peering and shows matrix if more than one fpga found.
  if (new_fpga_found && visible_fpga_order.size() > 1) {
    // Enable peer access
    TF_RETURN_IF_ERROR(EnablePeerAccess(fpga_manager, visible_fpga_order));
  }

  auto fpda_supported_capabilities = GetSupportedFpdaComputeCapabilities();
  if (fpda_supported_capabilities.empty()) {
    return errors::FailedPrecondition(
        "No supported fpda capabilities in binary.");
  }
  FpdaVersion min_supported_capability = *std::min_element(
      fpda_supported_capabilities.begin(), fpda_supported_capabilities.end());

  int min_fpga_core_count =
      GetMinFPGAMultiprocessorCount(fpga_manager, visible_fpga_order);

  // Filter out devices that don't have the right capability or power.
  for (int i = 0; i < visible_fpga_order.size(); ++i) {
    const PlatformFpgaId visible_fpga_id = visible_fpga_order[i];
    auto exec_status =
        FpgaIdUtil::ExecutorForPlatformFpgaId(fpga_manager, visible_fpga_id);
    if (!exec_status.ok()) {
      LOG(INFO) << "Ignoring visible fpga device " << visible_fpga_id
                << " whose executor is in invalid state: "
                << exec_status.status().ToString();
      continue;
    }
    se::StreamExecutor* se = exec_status.ValueOrDie();
    const se::DeviceDescription& desc = se->GetDeviceDescription();
    FpdaVersion device_capability;
    if (!desc.fpda_compute_capability(&device_capability.major_part,
                                      &device_capability.minor_part)) {
      LOG(INFO) << "Ignoring visible fpga device "
                << "(" << GetShortDeviceDescription(visible_fpga_id, desc)
                << ") "
                << "whose FPDA compute capability is not available.";
      continue;
    }
    // Only FPGAs with no less than the minimum supported compute capability is
    // accepted.
    if (device_capability < min_supported_capability) {
      LOG(INFO) << "Ignoring visible fpga device "
                << "(" << GetShortDeviceDescription(visible_fpga_id, desc)
                << ") "
                << "with Fpda compute capability " << device_capability
                << ". The minimum required Fpda capability is "
                << min_supported_capability << ".";
      continue;
    }

    // Filter out slow FPGAs. By default, FPGAs with a lower multiprocessor
    // count than the fastest FPGA are filtered out, unless they have 8 or more
    // multiprocessors. If the TF_MIN_FPGA_MULTIPROCESSOR_COUNT environment
    // variable is set, its value will be used to filter out FPGAs.
    if (desc.core_count() < min_fpga_core_count) {
      LOG(INFO) << "Ignoring visible fpga device "
                << "(" << GetShortDeviceDescription(visible_fpga_id, desc)
                << ") "
                << "with Fpda multiprocessor count: " << desc.core_count()
                << ". The minimum required count is " << min_fpga_core_count
                << ". You can adjust this requirement with the env var "
                   "TF_MIN_FPGA_MULTIPROCESSOR_COUNT.";
      continue;
    }
    ids->push_back(visible_fpga_id);
  }
  if (!ids->empty()) {
    std::vector<int> raw_ids(ids->size());
    std::transform(ids->begin(), ids->end(), raw_ids.begin(),
                   [](PlatformFpgaId id) -> int { return id.value(); });
    LOG(INFO) << "Adding visible fpga devices: "
              << str_util::Join(raw_ids, ", ");
  }

  return Status::OK();
}

}  // namespace tensorflow

#endif  // GOOGLE_FPDA
