/* Copyright 2015 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#ifndef TENSORFLOW_CORE_COMMON_RUNTIME_FPGA_FPGA_ID_UTILS_H_
#define TENSORFLOW_CORE_COMMON_RUNTIME_FPGA_FPGA_ID_UTILS_H_

#include "tensorflow/core/common_runtime/fpga/fpga_id.h"
#include "tensorflow/core/common_runtime/fpga/fpga_id_manager.h"
#include "tensorflow/core/common_runtime/fpga/fpga_init.h"
#include "tensorflow/core/lib/gtl/int_type.h"
#include "tensorflow/core/platform/stream_executor.h"

namespace tensorflow {

// Utility methods for translation between Tensorflow FPGA ids and platform FPGA
// ids.
class FpgaIdUtil {
 public:
  // Convenient methods for getting the associated executor given a TfFpgaId or
  // PlatformFpgaId.
 /* static se::port::StatusOr<se::StreamExecutor*> ExecutorForPlatformFpgaId(
      se::Platform* fpga_manager, PlatformFpgaId platform_fpga_id) {
    return fpga_manager->ExecutorForDevice(platform_fpga_id.value());
  }
  static se::port::StatusOr<se::StreamExecutor*> ExecutorForPlatformFpgaId(
      PlatformFpgaId platform_fpga_id) {
    return ExecutorForPlatformFpgaId(FPGAMachineManager(), platform_fpga_id);
  }
  static se::port::StatusOr<se::StreamExecutor*> ExecutorForTfFpgaId(
      TfFpgaId tf_fpga_id) {
    PlatformFpgaId platform_fpga_id;
    TF_RETURN_IF_ERROR(
        FpgaIdManager::TfToPlatformFpgaId(tf_fpga_id, &platform_fpga_id));
    return ExecutorForPlatformFpgaId(platform_fpga_id);
  }  */

  // Verify that the platform_fpga_id associated with a TfFpgaId is legitimate.
  static void CheckValidTfFpgaId(TfFpgaId tf_fpga_id) {
    PlatformFpgaId platform_fpga_id;
    TF_CHECK_OK(FpgaIdManager::TfToPlatformFpgaId(tf_fpga_id, &platform_fpga_id));
    const int visible_device_count = FPGAMachineManager()->VisibleDeviceCount();
    CHECK_LT(platform_fpga_id.value(), visible_device_count)
        << "platform_fpga_id is outside discovered device range."
        << " TF FPGA id: " << tf_fpga_id
        << " platform FPGA id: " << platform_fpga_id
        << " visible device count: " << visible_device_count;
  }
};

}  // namespace tensorflow

#endif  // TENSORFLOW_CORE_COMMON_RUNTIME_FPGA_FPGA_ID_UTILS_H_
