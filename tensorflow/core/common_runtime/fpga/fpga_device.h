/* Copyright 2015 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#if !GOOGLE_FPDA
#error This file must only be included when building with Fpda support
#endif

#ifndef TENSORFLOW_CORE_COMMON_RUNTIME_FPGA_FPGA_DEVICE_H_
#define TENSORFLOW_CORE_COMMON_RUNTIME_FPGA_FPGA_DEVICE_H_

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"
#include "tensorflow/core/common_runtime/device_factory.h"
#include "tensorflow/core/common_runtime/fpga/fpga_event_mgr.h"
#include "tensorflow/core/common_runtime/fpga/fpga_id.h"
#include "tensorflow/core/common_runtime/fpga/fpga_id_manager.h"
#include "tensorflow/core/common_runtime/fpga/fpga_id_utils.h"
#include "tensorflow/core/common_runtime/fpga_device_context.h"
#include "tensorflow/core/common_runtime/local_device.h"
#include "tensorflow/core/common_runtime/scoped_allocator_mgr.h"
#include "tensorflow/core/framework/allocator.h"
#include "tensorflow/core/framework/device_base.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/lib/core/status.h"
#include "tensorflow/core/lib/gtl/inlined_vector.h"
#include "tensorflow/core/platform/mutex.h"
#include "tensorflow/core/platform/stream_executor.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/core/public/session_options.h"

namespace tensorflow {

class BaseFPGADevice : public LocalDevice {
 public:
  BaseFPGADevice(const SessionOptions& options, const string& name,
                Bytes memory_limit, const DeviceLocality& locality,
                TfFpgaId tf_fpga_id, const string& physical_device_desc,
                Allocator* fpga_allocator, Allocator* cpu_allocator,
                bool sync_every_op, int32 max_streams);

  ~BaseFPGADevice() override;

  // Initialize the device and return the status of initialization.
  Status Init(const SessionOptions& options);

  // FPGA devices require the Op Compute method to save a reference to
  // any temporary tensors that are allocated until the Op execution
  // completes.
  bool RequiresRecordingAccessedTensors() const override;

  // FPGA kernel execution requires us to use `tracing::ScopedAnnotation()`
  // rather than `tracing::ScopedActivity()`, in order to relate asynchronously
  // launched FPGA kernels to the OpKernel.
  bool TraceUsingAnnotations() const { return true; }

  void ConsumeListOfAccessedTensors(
      DeviceContext* device_context,
      const TensorReferenceVector& tensor_refs) override;

  Status FillContextMap(const Graph* graph,
                        DeviceContextMap* device_context_map) override;

  void Compute(OpKernel* op_kernel, OpKernelContext* context) override;

  Status Sync() override;

  void ComputeAsync(AsyncOpKernel* op_kernel, OpKernelContext* context,
                    AsyncOpKernel::DoneCallback done) override;

  Status MakeTensorFromProto(const TensorProto& tensor_proto,
                             const AllocatorAttributes alloc_attrs,
                             Tensor* tensor) override;

  // The caller owns the returned device.
  PerOpFpgaDevice* MakeFpgaDevice() override;

  Status ReinitializeFpgaDevice(OpKernelContext* context, PerOpFpgaDevice* device,
                               DeviceContext* dc,
                               Allocator* allocator) override;

  // Returns the platform FPGA id of this device within the native driver system;
  // e.g., for FPDA this is the ordinal of the FPGA within the system.
  int fpga_id() const {
    PlatformFpgaId platform_fpga_id;
    TF_CHECK_OK(FpgaIdManager::TfToPlatformFpgaId(tf_fpga_id_, &platform_fpga_id));
    return platform_fpga_id.value();
  }

  // The executor that provides control for the device; e.g., for FPDA this
  // corresponds to the fpga context.
  se::StreamExecutor* executor() const { return executor_; }

  Allocator* GetScopedAllocator(AllocatorAttributes attr,
                                int64 step_id) override;

  ScopedAllocatorMgr* GetScopedAllocatorMgr() const override {
    return scoped_allocator_mgr_.get();
  }

 protected:
  Allocator* fpga_allocator_;  // not owned
  Allocator* cpu_allocator_;  // not owned

  se::StreamExecutor* executor_;  // not owned
  std::unique_ptr<ScopedAllocatorMgr> scoped_allocator_mgr_;

 private:
  struct StreamGroup {
    se::Stream* compute = nullptr;
    se::Stream* host_to_device = nullptr;
    se::Stream* device_to_host = nullptr;
    gtl::InlinedVector<se::Stream*, 4> device_to_device;
  };
  class StreamGroupFactory;

  gtl::InlinedVector<StreamGroup*, 4> streams_;
  mutex scratch_init_mutex_;
  gtl::InlinedVector<char*, 4> scratch_;
  std::vector<FPGADeviceContext*> device_contexts_;
  FpgaDeviceInfo* fpga_device_info_ = nullptr;
  mutex trace_mu_;
  TfFpgaId tf_fpga_id_;
  const bool sync_every_op_ = false;
  const int32 max_streams_;
  std::unique_ptr<EventMgr> em_;
  std::unique_ptr<thread::ThreadPool> thread_pool_;

  // Initialize scractch buffers used by Eigen.
  Status InitScratchBuffers();

  void ReinitializeDevice(OpKernelContext* context, PerOpFpgaDevice* device,
                          int stream_id, Allocator* allocator);

  void ComputeHelper(OpKernel* op_kernel, OpKernelContext* context);

  string ComputeOpKernelDebugString(const OpKernel& op_kernel,
                                    const int& stream_id);

  // This method returns an initialization status, in addition to
  // calling the "done" StatusCallback, if there is a failure to
  // allocate memory or if the tensor "from" is not DMA-copyable.
  // If there is no error prior to enqueueing the copy, an OK status
  // is returned.
  Status MaybeCopyTensorToFPGA(const AllocatorAttributes& alloc_attrs,
                              const Tensor& from, Tensor* to,
                              StatusCallback done);
};

class BaseFPGADeviceFactory : public DeviceFactory {
 public:
  Status CreateDevices(const SessionOptions& options, const string& name_prefix,
                       std::vector<Device*>* devices) override;

  struct InterconnectMap {
    // Name of interconnect technology, if known.
    string name;
    // If possible, strength should approximate Gb/sec bandwidth rate.
    // Where architecture-specific subclassing is not done that won't
    // always be possible.  The minimum expectation is that
    // faster links should have a higher value than slower links.
    int32 strength;
    static const int kSameDeviceStrength;
    static const int kStreamExecutorStrength;
    std::set<std::pair<PlatformFpgaId, PlatformFpgaId>> directed_links;
  };

 protected:
  // Populates *maps with interconnect maps for all local direct access
  // pathways between FPGAs.
  virtual Status GetInterconnectMaps(
      const std::vector<PlatformFpgaId>& visible_fpga_order,
      se::Platform* fpga_manager, std::vector<InterconnectMap>* maps);

  struct TfFpgaIdHash {
    std::size_t operator()(const TfFpgaId& id) const noexcept {
      return std::hash<int>{}(id.value());
    }
  };
  typedef std::unordered_map<TfFpgaId, DeviceLocality, TfFpgaIdHash> LocalityMap;
  // Populates *localities with the DeviceLocality descriptor for
  // every TfFpgaId.
  virtual Status GetDeviceLocalities(
      int num_tf_fpgas, const std::vector<InterconnectMap>& interconnects,
      LocalityMap* localities);

 private:
  // Creates a BaseFPGADevice associated with 'tf_fpga_id', allocates (strictly)
  // 'memory_limit' bytes of FPGA memory to it, and adds it to the 'devices'
  // vector.
  Status CreateFPGADevice(const SessionOptions& options,
                         const string& name_prefix, TfFpgaId tf_fpga_id,
                         int64 memory_limit, const DeviceLocality& dev_locality,
                         std::vector<Device*>* devices);

  virtual BaseFPGADevice* CreateFPGADevice(const SessionOptions& options,
                                         const string& name, Bytes memory_limit,
                                         const DeviceLocality& dev_locality,
                                         TfFpgaId tf_fpga_id,
                                         const string& physical_device_desc,
                                         Allocator* fpga_allocator,
                                         Allocator* cpu_allocator) = 0;

  // Returns into 'ids' the list of valid platform FPGA ids, in the order that
  // they should map to TF FPGA ids "/device:FPGA:0", "/device:FPGA:1", etc,
  // based upon 'visible_fpga_order' which was generated by parsing
  // FPGAOptions::visible_device_list which is a comma-separated list of FPDA FPGA
  // ids.
  Status GetValidDeviceIds(const std::vector<PlatformFpgaId>& visible_fpga_order,
                           std::vector<PlatformFpgaId>* ids);

  // visible_fpga_initialized_[platform_fpga_id] is true if visible FPGA
  // platform_fpga_id has been initialized by the process.
  std::unordered_map<int, bool> visible_fpga_initialized_;
};

}  // namespace tensorflow

#endif  // TENSORFLOW_CORE_COMMON_RUNTIME_FPGA_FPGA_DEVICE_H_
