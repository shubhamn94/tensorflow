/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include <stdlib>
#ifdef GOOGLE_FPDA
#include "fpda/include/fpda.h"
#include "tensorflow/stream_executor/fpda/fpda_activation.h"
#endif  // GOOGLE_FPDA

#include "tensorflow/core/common_runtime/fpga/fpga_fpdamalloc_allocator.h"

#include "tensorflow/core/common_runtime/fpga/fpga_id.h"
#include "tensorflow/core/common_runtime/fpga/fpga_id_utils.h"
#include "tensorflow/core/common_runtime/fpga/fpga_init.h"
#include "tensorflow/core/platform/stream_executor.h"

namespace tensorflow {

FPGAfpdaMallocAllocator::FPGAfpdaMallocAllocator(Allocator* allocator,
                                               PlatformFpgaId platform_fpga_id)
    : base_allocator_(allocator) {
  //stream_exec_ = FpgaIdUtil::ExecutorForPlatformFpgaId(platform_fpga_id).ValueOrDie();
}

FPGAfpdaMallocAllocator::~FPGAfpdaMallocAllocator() { delete base_allocator_; }

void* FPGAfpdaMallocAllocator::AllocateRaw(size_t alignment, size_t num_bytes) {
#ifdef GOOGLE_FPDA
  // allocate with fpdaMalloc
  //se::fpda::ScopedActivateExecutorContext scoped_activation{stream_exec_};
  
  /*FPdeviceptr rv = 0;
  FPresult res = cuMemAlloc(&rv, num_bytes);*/
  void *ptr = (size_t*) malloc(num_bytes*sizeof(size_t));
  if (!ptr) {
    LOG(ERROR) << "Allocation failed to allocate " << num_bytes;
    return nullptr;
  }
  return reinterpret_cast<void*>(rv);
#else
  return nullptr;
#endif  // GOOGLE_FPDA
}
void FPGAfpdaMallocAllocator::DeallocateRaw(void* ptr) {
#ifdef GOOGLE_FPDA
  // free with fpdaFree
  //CUresult res = cuMemFree(reinterpret_cast<CUdeviceptr>(ptr));
  if (ptr) {
    LOG(ERROR) << "Memory failed to free " << ptr;
  }
#endif  // GOOGLE_FPDA
}

bool FPGAfpdaMallocAllocator::TracksAllocationSizes() { return false; }

}  // namespace tensorflow
