/* Copyright 2015 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include "tensorflow/core/common_runtime/fpga/fpga_id_manager.h"

#include <unordered_map>

#include "tensorflow/core/common_runtime/fpga/fpga_id.h"
#include "tensorflow/core/lib/core/errors.h"
#include "tensorflow/core/lib/core/status.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/platform/macros.h"
#include "tensorflow/core/platform/mutex.h"

namespace tensorflow {
namespace {
// Manages the map between TfFpgaId and platform FPGA id.
class TfToPlatformFpgaIdMap {
 public:
  static TfToPlatformFpgaIdMap* singleton() {
    static auto* id_map = new TfToPlatformFpgaIdMap;
    return id_map;
  }

  Status Insert(TfFpgaId tf_fpga_id, PlatformFpgaId platform_fpga_id)
      LOCKS_EXCLUDED(mu_) {
    std::pair<IdMapType::iterator, bool> result;
    {
      mutex_lock lock(mu_);
      result = id_map_.insert({tf_fpga_id.value(), platform_fpga_id.value()});
    }
    if (!result.second && platform_fpga_id.value() != result.first->second) {
      return errors::AlreadyExists(
          "TensorFlow device (FPGA:", tf_fpga_id.value(),
          ") is being mapped to "
          "multiple FPDA devices (",
          platform_fpga_id.value(), " now, and ", result.first->second,
          " previously), which is not supported. "
          "This may be the result of providing different FPGA configurations "
          "(ConfigProto.fpga_options, for example different visible_device_list)"
          " when creating multiple Sessions in the same process. This is not "
          " currently supported, see "
          "https://github.com/tensorflow/tensorflow/issues/19083");
    }
    return Status::OK();
  }

  bool Find(TfFpgaId tf_fpga_id, PlatformFpgaId* platform_fpga_id) const
      LOCKS_EXCLUDED(mu_) {
    mutex_lock lock(mu_);
    auto result = id_map_.find(tf_fpga_id.value());
    if (result == id_map_.end()) return false;
    *platform_fpga_id = result->second;
    return true;
  }

 private:
  TfToPlatformFpgaIdMap() = default;

  void TestOnlyReset() LOCKS_EXCLUDED(mu_) {
    mutex_lock lock(mu_);
    id_map_.clear();
  }

  using IdMapType = std::unordered_map<int32, int32>;
  mutable mutex mu_;
  IdMapType id_map_ GUARDED_BY(mu_);

  friend class ::tensorflow::FpgaIdManager;
  TF_DISALLOW_COPY_AND_ASSIGN(TfToPlatformFpgaIdMap);
};
}  // namespace

Status FpgaIdManager::InsertTfPlatformFpgaIdPair(TfFpgaId tf_fpga_id,
                                               PlatformFpgaId platform_fpga_id) {
  return TfToPlatformFpgaIdMap::singleton()->Insert(tf_fpga_id, platform_fpga_id);
}

Status FpgaIdManager::TfToPlatformFpgaId(TfFpgaId tf_fpga_id,
                                       PlatformFpgaId* platform_fpga_id) {
  if (TfToPlatformFpgaIdMap::singleton()->Find(tf_fpga_id, platform_fpga_id)) {
    return Status::OK();
  }
  return errors::NotFound("TensorFlow device FPGA:", tf_fpga_id.value(),
                          " was not registered");
}

void FpgaIdManager::TestOnlyReset() {
  TfToPlatformFpgaIdMap::singleton()->TestOnlyReset();
}

}  // namespace tensorflow
