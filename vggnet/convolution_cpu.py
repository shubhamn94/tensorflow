import tensorflow as tf
import numpy as np
from scipy.misc import imread, imresize
import sys
import time

# This creates a random 32-bit floating point input for your variables
#def rand(dim):
 #   return np.random.random(dim).astype(np.float32)

X = tf.placeholder(tf.float32, [1,224,224,3])

conv1_w = tf.Variable(tf.ones([3,3,3,64], dtype=tf.float32), dtype=tf.float32)
conv1_b = tf.constant([1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1], dtype=tf.float32, shape=[64])
conv2_w = tf.Variable(tf.ones([3,3,64,64], dtype=tf.float32), dtype=tf.float32)
conv2_b = tf.constant([1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1], dtype=tf.float32, shape=[64])

#dense1_w = tf.Variable(rand([784, 20])) 
#dense1_b = tf.Variable(rand([20]))

#dense2_w = tf.Variable(rand([20, 10])) 
#dense2_b = tf.Variable(rand([10]))

    # Generating circuit
#with tf.device("device:XLA_CPU:0"):
    
    #time.sleep(10);
img1 = imread('images/laska.png', mode='RGB')
img1 = imresize(img1, (224, 224)) 
conv1 = tf.nn.relu(tf.add(tf.nn.conv2d(X, conv1_w, strides=[1, 1, 1, 1], padding='SAME', data_format='NHWC'),conv1_b))
#maxp1 = tf.math.add(X,conv1)
maxp1=tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)
#maxp1 = tf.nn.relu(
#print(maxp1.shape)
conv2 = tf.add(tf.nn.conv2d(maxp1, conv2_w, strides=[1, 1, 1, 1], padding='SAME'),conv2_b)
sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
sess.run(tf.global_variables_initializer())
#result = sess.run(conv1,{X:test_image})
result = sess.run(conv2, feed_dict={X: [img1]})
p = result.shape
f = open("cpu_conv_result.log", "w+")
for i in range(0, p[0]):
    for j in range(0, p[1]):
        for k in range(0, p[2]):
            for l in range(0, p[3]):
                f.write("%f\n" % result[i,j,k,l])
f.close()


# Print expected result
print("Result Calculated: "+str(result))
