with open('./vgg16_convonly_golden.log') as golden, open('./vgg16_fpga_result.log') as fpga:
    line_num = 0
    for cpu_line, fpga_line in zip(golden.readlines(), fpga.readlines()):
        line_num += 1
        x = float(cpu_line)
        y = float(fpga_line)
        if(x == y):
            continue
        if(x == 0):
            print("gpu and gpu) result is different at line num: %d" % line_num)
            continue
        error = ((y - x)/x) * 100
        if (error > 0.1 or error < (-0.1)):
            print("%d:(%f,%f) - (%f%%)" % (line_num,y,x,error))
