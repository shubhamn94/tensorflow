rm -rf /tmp/tensorflow_pkg
bazel build -c dbg -c opt --strip=never //tensorflow/tools/pip_package:build_pip_package |& tee build.log
pip uninstall tensorflow
cd ../
./bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp/tensorflow_pkg
pip install /tmp/tensorflow_pkg/tensorflow-*
cd -
#python vgg16_fpga.py >txt_fpga_conv1_1.log 2>&1
